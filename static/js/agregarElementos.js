$(document).ready(function() {
	$(".listado").each(function() {
		var $this = $(this);
		var url = $this.attr("data-url");
		if (url) {
			$.get(url, function(data) {
				$this.autocomplete({source: data});
			});
		}
	});
});