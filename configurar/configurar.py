# coding=utf-8
import sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")))
os.environ['DJANGO_SETTINGS_MODULE'] = 'KunaAty.settings'
from django.conf import settings

from django.core.management import call_command
call_command('syncdb')
call_command('migrate')

from Usuarios.models import KunaAtyUsuario
import getpass

def crear_usuario(nombre, contrasenha, correo, admin = False):
    usuario = KunaAtyUsuario(username = nombre, email = correo, is_staff = admin)
    usuario.set_password(contrasenha)
    usuario.save()

while 1:
    crear = raw_input("Quiere crear un usuario? (S/N): ").upper()
    if crear == 'S':
        nombre = raw_input("Nombre: ")
        contra0 = "0"
        contra1 = "1"
        while contra0 != contra1:
            contra0 = getpass.getpass("Contraseña: ")
            contra1 = getpass.getpass("Contraseña (otra vez): ")
            if contra0 != contra1:
                print "Contraseñas no coinciden"
        
        correo = raw_input("Correo electrónico")
        admin = raw_input("Este usuario es administrador? (S/N)").upper() == 'S'
        try:
            crear_usuario(nombre, contra0, correo, admin)
        except:
            print "Error creando usuario"
    elif crear == 'N':
        break