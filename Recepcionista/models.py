# coding=utf-8
from django.db import models
from Usuarios.models import KunaAtyUsuario
from django.core.urlresolvers import reverse



class Consultorio(models.Model):
	sala_nro=models.CharField(max_length=1)
	especialidad= models.CharField(max_length=40)
	especialista_asignado=models.ForeignKey(KunaAtyUsuario)
	FechaDesde =  models.DateField(verbose_name = "Fecha Desde", null = True, blank = True)
	FechaHasta = models.DateField(verbose_name = "Fecha Hasta", null = True, blank = True)
	
	def get_absolute_url(self):
		return reverse("ver_consultorio", kwargs = {"pk": self.pk})
		
	def _unicode_(self):
		return self.especialidad