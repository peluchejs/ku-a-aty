# coding=utf-8
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from forms import FormularioConsultorio
from django.views.generic import UpdateView
from models import Consultorio
from django.core.urlresolvers import reverse


###
import json

from django.http import HttpResponse, Http404
###
	
@login_required
@user_passes_test(lambda u:u.tipo == 0 or u.is_staff)
def RegistrarConsultorio(solicitud):
	Consultorio_formulario = FormularioConsultorio(solicitud.POST or None)
	if Consultorio_formulario.is_valid():
		Consultorio_formulario.save()
		messages.success(solicitud, "Datos Guardados")
		return HttpResponseRedirect(Consultorio_formulario.instance.get_absolute_url())
	return render(solicitud, "Consultorio_formulario.html",{"form": Consultorio_formulario})

@login_required
def Modificar(solicitud, pk):
	consultorio = get_object_or_404(Consultorio, pk = pk)
	modificar_con=FormularioConsultorio(solicitud.POST or None, instance = consultorio)
	if modificar_con.is_valid():
		modificar_con.save()
		messages.success(solicitud, "Datos Modificados Correctamente")
		return HttpResponseRedirect(modificar_con.instance.get_absolute_url())
	return render(solicitud, "modificar_consultorio.html",{"form": modificar_con})

def Borrar(solicitud, pk):
	consultorio=Consultorio.objects.get(pk = pk)
	consultorio.delete()
	messages.success(solicitud, "Datos Eliminados")
	return HttpResponseRedirect(reverse("lista_consultorio"))
	return render(solicitud, "Consultorio_lista.html",{"form": Consultorio_formulario})
