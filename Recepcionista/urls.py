# coding=utf-8
from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView, CreateView, UpdateView
from django.contrib.auth.decorators import login_required
from models import Consultorio

urlpatterns = patterns('',
	url(r'^RegistrarConsultorio/$', 'Recepcionista.views.RegistrarConsultorio', name = 'RegistrarConsultorio'),
	url(r'^Consultorio/Modificar/(?P<pk>\d+)/$', 'Recepcionista.views.Modificar', name='modificar_consultorio'),
	url(r'^Consultorio/Borrar/(?P<pk>\d+)/$', 'Recepcionista.views.Borrar', name='eliminar_consultorio'),
	url(r'^Consultorio/$', login_required(ListView.as_view(model = Consultorio, template_name = "consultorio_lista.html")), name="lista_consultorio"),
	url(r'^Consultorio/(?P<pk>\d+)/$', login_required(DetailView.as_view(model = Consultorio, template_name = "consultorio_detalle.html")), name="ver_consultorio")
)