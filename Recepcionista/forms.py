# coding=utf-8
from django import forms
from models import Consultorio


class FormularioConsultorio(forms.ModelForm):
	FechaDesde = forms.DateField(label = "Fecha Desde", widget=forms.TextInput(attrs={"class":"datepicker"}), required=False)
	FechaHasta = forms.DateField(label = "Fecha Hasta", widget=forms.TextInput(attrs={"class":"datepicker"}), required=False)
	class Meta:
		model=Consultorio
		