# coding=utf-8
# Create your views here.
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

@login_required
def inicio(solicitud):
    return render(solicitud, "inicio.html")
