# coding=utf-8
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', "KunaAty.views.inicio"),
    url(r'^Informes/', include('Informes.urls')),
    url(r'^Administracion/', include('Administracion.urls')),
    url(r'^Especialista/', include('Especialista.urls')),
    url(r'^Usuarios/', include('Usuarios.urls')),
    url(r'^Recepcionista/', include('Recepcionista.urls')),
    url(r'^Personas/', include('Personas.urls')),
)
