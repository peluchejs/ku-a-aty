# coding=utf-8
"""
Pruebas de ingresar, salir, cambiar contraseña
"""

from django.test import TestCase
from django.test import Client, LiveServerTestCase
from Usuarios.models import KunaAtyUsuario
from django.core.urlresolvers import reverse
import selenium.webdriver
from selenium.webdriver import ActionChains

class PruebaDeIngresar(TestCase):
    def setUp(self):
        for i in xrange(3):
            u = KunaAtyUsuario(tipo = i)
            u.username = "usuario" + str(i)
            u.set_password("contrasena" + str(i))
            u.save()
        u = KunaAtyUsuario(tipo = 0)
        u.username = "admin"
        u.set_password("admin")
        u.is_staff = True
        u.save()
    def test_login(self):
        """
        Probar que usuarios pueden ingresar al sistema
        """
        c = Client()
        for i in xrange(3):
            response = c.post("/Usuarios/Ingresar/", {"username": "usuario" + str(i), "password": "contrasena" + str(i)})
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, "/")
        response = c.post("/Usuarios/Ingresar/", {"username": "admin", "password": "admin"})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/")
    def test_redirect(self):
        """
        Probar que los usuarios tienen que ingresar
        """
        c = Client()
        response = c.get("/")
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, 'Usuarios/Ingresar/?next=/')
        response = c.get("/", follow = True)
        self.assertEqual(response.status_code, 200)
    def test_logout(self):
        """
        Probar que usuarios pueden salir del sistema
        """
        c = Client()
        for i in xrange(3):
            response = c.post("/Usuarios/Ingresar/", {"username": "usuario" + str(i), "password": "contrasena" + str(i)})
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, "/")
            response = c.get("/Usuarios/Salir/")
            self.assertEqual(response.status_code, 200)
        response = c.post("/Usuarios/Ingresar/", {"username": "admin", "password": "admin"})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/")
        response = c.get("/Usuarios/Salir/")
        self.assertEqual(response.status_code, 200)

class LiveUsuarioTest(LiveServerTestCase):
    fixtures = ['test_data.json']
    
    @classmethod
    def setUpClass(cls):
        cls.driver = selenium.webdriver.Chrome()
        super(LiveUsuarioTest, cls).setUpClass()
        cls.driver.maximize_window()
        cls.driver.implicitly_wait(20)
    
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super(LiveUsuarioTest, cls).tearDownClass()
    
    def clickMenuItem(self, menu_header, menu_item):
        menu_heading = self.driver.find_element_by_xpath("//li[contains(text(), '%s')]" % (menu_header,))
        ActionChains(self.driver).move_to_element(menu_heading).perform()
        logout_link = self.driver.find_element_by_partial_link_text(menu_item)
        ActionChains(self.driver).move_to_element(menu_heading).move_to_element(logout_link).click().perform()

    def login(self, username, password):
        self.driver.get('%s%s' % (self.live_server_url, reverse('ingresar')))
        username_input = self.driver.find_element_by_name("username")
        username_input.send_keys(username)
        password_input = self.driver.find_element_by_name("password")
        password_input.send_keys(password)
        login_button = self.driver.find_element_by_xpath("//input[@type='submit']")
        login_button.click()
    
    def logout(self):
        self.clickMenuItem("Cuenta", "Salir")
        
    def testLoginLogout(self):
        self.login("testuser", "testpassword")
        self.assertEqual(self.driver.current_url, "%s%s" % (self.live_server_url, reverse("KunaAty.views.inicio")))
        self.logout()
        self.assertEqual(self.driver.current_url, "%s%s" % (self.live_server_url, reverse("salir")))
        