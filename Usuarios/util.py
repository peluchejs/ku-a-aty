class LoggedInMixin(object):
    """ Requiere un usuario autenticado """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise http.Http404
        return super(LoggedInMixin, self).dispatch(request, *args, **kwargs)