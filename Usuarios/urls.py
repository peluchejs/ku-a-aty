# coding=utf-8
from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login

urlpatterns = patterns('',
     url(r'^Notificacion/(?P<paciente>\d+)/$','Usuarios.views.crear_notificacion', name='crear_notificacion'),
     url(r'^Ingresar/$', login, name='ingresar'),
     url(r'^CambiarContrasenha/$','Usuarios.views.CambiarContrasenha',name='cambiar_contrasenha'),
     url(r'^Configuracion/$', 'Usuarios.views.Configuracion', name="configurar_usuario"),
     url(r'^VerNotificaciones', 'Usuarios.views.VerNotificaciones', name="verNotificaciones"),
     url(r'^Salir/$', 'Usuarios.views.salir', name='salir'),

)
