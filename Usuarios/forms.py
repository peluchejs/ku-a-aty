# coding=utf-8
from django import forms
from models import KunaAtyUsuario
from models import Mensaje

class DatosPersonalesFormulario(forms.ModelForm):
    class Meta:
        model = KunaAtyUsuario
        fields = ('first_name', 'last_name', 'email', 'Telefono')
    
    def __init__(self, *args, **kwargs):
        super(DatosPersonalesFormulario, self).__init__(*args, **kwargs)
        
class Notificacion_formulario(forms.ModelForm):
    class Meta:
        model=Mensaje
        exclude=("Nuevo", "Paciente","notificador",)