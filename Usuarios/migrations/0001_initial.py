# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'KunaAtyUsuario'
        db.create_table(u'Usuarios_kunaatyusuario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('tipo', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('Especialidad', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('Direccion', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='+', unique=True, null=True, to=orm['Personas.Direccion'])),
            ('Telefono', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'Usuarios', ['KunaAtyUsuario'])

        # Adding M2M table for field groups on 'KunaAtyUsuario'
        m2m_table_name = db.shorten_name(u'Usuarios_kunaatyusuario_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('kunaatyusuario', models.ForeignKey(orm[u'Usuarios.kunaatyusuario'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['kunaatyusuario_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'KunaAtyUsuario'
        m2m_table_name = db.shorten_name(u'Usuarios_kunaatyusuario_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('kunaatyusuario', models.ForeignKey(orm[u'Usuarios.kunaatyusuario'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['kunaatyusuario_id', 'permission_id'])

        # Adding model 'Mensaje'
        db.create_table(u'Usuarios_mensaje', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Para', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Usuarios.KunaAtyUsuario'])),
            ('notificador', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+not', to=orm['Usuarios.KunaAtyUsuario'])),
            ('Texto', self.gf('django.db.models.fields.TextField')()),
            ('Nuevo', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('Paciente', self.gf('django.db.models.fields.related.ForeignKey')(blank='True', related_name='+', null=True, to=orm['Personas.DatosPersonalesBasicos'])),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'Usuarios', ['Mensaje'])


    def backwards(self, orm):
        # Deleting model 'KunaAtyUsuario'
        db.delete_table(u'Usuarios_kunaatyusuario')

        # Removing M2M table for field groups on 'KunaAtyUsuario'
        db.delete_table(db.shorten_name(u'Usuarios_kunaatyusuario_groups'))

        # Removing M2M table for field user_permissions on 'KunaAtyUsuario'
        db.delete_table(db.shorten_name(u'Usuarios_kunaatyusuario_user_permissions'))

        # Deleting model 'Mensaje'
        db.delete_table(u'Usuarios_mensaje')


    models = {
        u'Personas.barrio': {
            'Meta': {'object_name': 'Barrio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'Personas.datospersonalesbasicos': {
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Cedula': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'Correo': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'Direccion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.Direccion']", 'null': 'True', 'blank': 'True'}),
            'FechaCreada': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'FechaNacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'LugarDeNacimiento': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'Meta': {'object_name': 'DatosPersonalesBasicos'},
            'Nacionalidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Pais']"}),
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'Sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.departamento': {
            'Meta': {'object_name': 'Departamento'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.direccion': {
            'Barrio': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Barrio']"}),
            'Ciudad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'Departamento': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Departamento']"}),
            'Direccion': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Direccion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.pais': {
            'Meta': {'object_name': 'Pais'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Usuarios.kunaatyusuario': {
            'Direccion': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'+'", 'unique': 'True', 'null': 'True', 'to': u"orm['Personas.Direccion']"}),
            'Especialidad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'Meta': {'object_name': 'KunaAtyUsuario'},
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'tipo': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'Usuarios.mensaje': {
            'Meta': {'object_name': 'Mensaje'},
            'Nuevo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Paciente': ('django.db.models.fields.related.ForeignKey', [], {'blank': "'True'", 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.DatosPersonalesBasicos']"}),
            'Para': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Usuarios.KunaAtyUsuario']"}),
            'Texto': ('django.db.models.fields.TextField', [], {}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notificador': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+not'", 'to': u"orm['Usuarios.KunaAtyUsuario']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Usuarios']