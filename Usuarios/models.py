﻿# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser
from Personas.models import Direccion, DatosPersonalesBasicos

class Especialidades: 
    Opciones = ((0, "Ninguno"), (1, "Asistencia Social"), (2,"Asistencia Juridica"), (3, "Asistencia Psicologica"), (4, "Asistencia Ginecologica"))    

class USUARIO_TIPOS(object):
    opciones = [
        (0, "Recepcionista"),
        (1, "Especialista"),
        (2, "Administrador"),
    ]
    predeterminado = 1

class KunaAtyUsuario(AbstractUser):
    tipo = models.IntegerField(default = USUARIO_TIPOS.predeterminado,
                               choices = USUARIO_TIPOS.opciones)
    Especialidad= models.IntegerField(choices = Especialidades.Opciones, default = 0)
    Direccion = models.OneToOneField(Direccion, null = True, blank = True, related_name = "+")
    Telefono = models.CharField(max_length = 20)
    
    def __unicode__(self):
        nombre = u""
        if self.first_name and self.last_name:
            nombre = u"%s %s" % (self.first_name, self.last_name)
        else:
            nombre = super(KunaAtyUsuario, self).__unicode__()
        return "%s (%s)" % (nombre, self.get_Especialidad_display())
        
    def mensajes_nuevos(self):
        mensajes = self.mensaje_set.all().filter(Nuevo = True)
        return mensajes

    def tiene_mensajes_nuevos(self):
        return self.mensaje_set.all().filter(Nuevo = True).exists()

class Mensaje(models.Model):
    Para= models.ForeignKey(KunaAtyUsuario)
    notificador=models.ForeignKey(KunaAtyUsuario, related_name="+not")
    Texto=  models.TextField()
    Nuevo = models.BooleanField(default = True)
    Paciente = models.ForeignKey(DatosPersonalesBasicos, related_name = "+", null = True, blank = "True")
    fecha = models.DateTimeField(auto_now_add = True)
    
    
