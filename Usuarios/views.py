﻿# coding=utf-8
from django.shortcuts import render
from django.contrib.auth.views import password_change
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.contrib.auth import logout

from forms import DatosPersonalesFormulario, Notificacion_formulario
from Personas.forms import FormularioDireccion
from Personas.models import DatosPersonalesBasicos
from Usuarios.models import Mensaje ######
@login_required
def CambiarContrasenha(solicitud):
    respuesta = password_change(solicitud, post_change_redirect=reverse(CambiarContrasenha), template_name='configurar_cuenta.html')
    if type(respuesta) == HttpResponseRedirect:
        messages.success(solicitud, "La contraseña ha sido actualizada")
    return respuesta


@login_required
def Configuracion(solicitud):
    datos = DatosPersonalesFormulario(solicitud.POST or None, instance = solicitud.user, prefix = "user")
    direccion = FormularioDireccion(solicitud.POST or None, instance = solicitud.user.Direccion, prefix = "direccion")
    if datos.is_valid() and direccion.is_valid():
        da = datos.save(commit = False)
        da.Direccion = direccion.save()
        da.save()
        datos.save_m2m()
        messages.success(solicitud, "Datos guardados")
        return HttpResponseRedirect(reverse(Configuracion))
    return render(solicitud, "configurar_cuenta.html", {'form': datos, 'extras': [direccion]})

@login_required
@user_passes_test(lambda u:u.tipo >= 0 or u.is_staff)
def crear_notificacion(solicitud, paciente):
    paciente_obj = get_object_or_404(DatosPersonalesBasicos, pk=int(paciente))
    Notificacionformulario = Notificacion_formulario(solicitud.POST or None)
    if Notificacionformulario.is_valid():
        notificacion = Notificacionformulario.save(commit = False)
        notificacion.Paciente = paciente_obj
        notificacion.notificador=solicitud.user
        notificacion.save()
        return HttpResponseRedirect(paciente_obj.get_absolute_url())
    return render(solicitud, "Notificaciones.html",{"form": Notificacionformulario})

@login_required
def VerNotificaciones(solicitud):
    if "mensaje" in solicitud.GET and "paciente" in solicitud.GET:
        mensaje = get_object_or_404(Mensaje, pk = int(solicitud.GET["mensaje"]))
        paciente = get_object_or_404(DatosPersonalesBasicos, pk = int(solicitud.GET["paciente"]))
        mensaje.Nuevo = False
        mensaje.save()
        return HttpResponseRedirect(paciente.get_absolute_url())
    return render(solicitud, "verNotificaciones.html", {"Mensajes": Mensaje.objects.filter(Para = solicitud.user).order_by("-fecha")})
                                                      
@login_required
def salir(solicitud):
    logout(solicitud)
    return HttpResponseRedirect(reverse('ingresar'))

