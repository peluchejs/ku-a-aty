rmdir /s /q htmlcov
del .coverage
coverage run --omit=*migration* --source='.' manage.py test
coverage html
htmlcov\index.html