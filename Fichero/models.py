﻿from django.db import models
from Usuarios.models import KunaAtyUsuario

class Actividad(models.Model):
    usuario = models.ForeignKey(KunaAtyUsuario, related_name = "+")
    url = models.URLField()
    tiempo = models.DateTimeField(auto_now = True)