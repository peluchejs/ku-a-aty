from models import Actividad

class GuardarActividadesMiddleware:
    def process_response(self, solicitud, respuesta):
        try:
            if not solicitud.user.is_anonymous():
                act = Actividad(usuario = solicitud.user, url = solicitud.path)
                act.save()
            return respuesta
        except AttributeError:
            return respuesta