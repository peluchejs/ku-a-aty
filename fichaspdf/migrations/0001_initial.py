# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Informes'
        db.create_table(u'Informes_informes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_inicial', self.gf('django.db.models.fields.DateField')()),
            ('fecha_final', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'Informes', ['Informes'])


    def backwards(self, orm):
        # Deleting model 'Informes'
        db.delete_table(u'Informes_informes')


    models = {
        u'Informes.informes': {
            'Meta': {'object_name': 'Informes'},
            'fecha_final': ('django.db.models.fields.DateField', [], {}),
            'fecha_inicial': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['Informes']