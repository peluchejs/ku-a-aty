# coding=utf-8
# Create your views here.
import os

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from forms import CrearInformeFormulario

from django.db.models import Count

from django.views.generic import ListView, DetailView, UpdateView

from Especialista.models import FichaBase
from Usuarios.models import KunaAtyUsuario, Mensaje
from KunaAty.settings import relativoALaRaiz

import reportlab.platypus
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, Image
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch

PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]

def datos_entre_fechas(fecha_inicio, fecha_fin):
	fichas = FichaBase.objects.filter(FechaActualizada__gte = fecha_inicio, FechaActualizada__lte = fecha_fin)
	return fichas

def sociales_adultos(fichas):
	return fichas.filter(fichasocial__isnull = False)

def sociales_adolescentes(fichas):
	return fichas.filter(fichasocialadolescente__isnull = False)

def sociales_infantes(fichas):
	return fichas.filter(fichainfantil__isnull = False)

def nuevos(fichas, fecha_inicio, fecha_fin):
	return fichas.filter(Paciente__FechaCreada__gte = fecha_inicio, Paciente__FechaCreada__lte = fecha_fin)
	
def infantes_datos(fichas):
	return fichas.filter(Paciente__datosinfantiles__isnull = False)

def adolescentes_datos(fichas):
	return fichas.filter(Paciente__datosdepacienteadolescente__isnull = False)

def adultos_datos(fichas):
	return fichas.filter(Paciente__datosdepacienteadulto__isnull = False)

def por_departamento(datos):
	return datos.values("Paciente__Direccion__Departamento__nombre").annotate(dep_count = Count("Paciente__Direccion__Departamento__nombre"))

def por_tipo_violencia_mujer(datos):
	d = datos.values("fichasocial__TipoDeViolencia__nombre").annotate(v_count = Count("fichasocial__TipoDeViolencia__nombre"))
	return [(x["fichasocial__TipoDeViolencia__nombre"], x["v_count"]) for x in d if x["v_count"] != 0]

def por_tipo_violencia_adolescente(datos):
	d = datos.values("fichasocialadolescente__TipoDeViolencia__nombre").annotate(v_count = Count("fichasocialadolescente__TipoDeViolencia__nombre"))
	return [(x["fichasocialadolescente__TipoDeViolencia__nombre"], x["v_count"]) for x in d if x["v_count"] != 0]

def por_tipo_violencia_infante(datos):
	d = datos.values("fichainfantil__TipoDeViolencia__nombre").annotate(v_count = Count("fichainfantil__TipoDeViolencia__nombre"))
	return [(x["fichainfantil__TipoDeViolencia__nombre"], x["v_count"]) for x in d if x["v_count"] != 0]

def dibujarPdf(empezar, terminar, respuesta):


	def primeraPagina(canvas, doc):
		canvas.saveState()
		canvas.drawImage(relativoALaRaiz("..\\static\\img\\image.jpg"),120, 765, 289, 59)
		canvas.setFont('Times-Bold',14)
		canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-127, "Informe Estadístico")
		canvas.setFont('Times-Bold',12)
		canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-140, "Desde %s - Hasta %s" % (empezar.strftime("%d/%m/%y"), terminar.strftime("%d/%m/%y")))
		canvas.setFont('Times-Roman',9)
		canvas.drawString(inch, 0.75 * inch,"Página 1")
		canvas.restoreState()
	
	def siguientesPaginas(canvas, doc):
		canvas.saveState()
		canvas.drawImage(relativoALaRaiz("..\\static\\img\\image.jpg"),120, 765, 289, 59)
		canvas.setFont('Times-Roman', 9)
		canvas.drawString(inch, 0.75 * inch,"Página %d" % (doc.page,))
		canvas.restoreState()
	
	def tabla(cabecera, filas, destacarFilas = []):
		filas.insert(0, [cabecera, ""])
		t = Table(filas, style=[('GRID',(0,0),(-1,-1),0.5,colors.grey),
								('SPAN', (0,0),(1,0))
								], colWidths=200, rowHeights=20)
		return t
	
	def departamento_tabla(titulo, datos):
		return tabla(titulo, [[x["Paciente__Direccion__Departamento__nombre"], x["dep_count"]] for x in datos if x["dep_count"] != 0])
	
	def violencia_tabla(titulo, datos):
		return tabla(titulo, datos)
	
	doc = SimpleDocTemplate(respuesta)
	contenido = [Spacer(1,1*inch)]
	datos = datos_entre_fechas(empezar, terminar)
	infantes = infantes_datos(datos)
	adolescentes = adolescentes_datos(datos)
	adultos = adultos_datos(datos)
	infantes_nuevos = nuevos(infantes, empezar, terminar)
	adolescentes_nuevos = nuevos(adolescentes, empezar, terminar)
	adultos_nuevos = nuevos(adultos, empezar, terminar)
	contenido.append(tabla("Resumen de atenciones realizadas", [["Infantes, primera vez", infantes_nuevos.count()],
									   ["Infantes, seguimiento", infantes.count() - infantes_nuevos.count()],
									   ["Infantes, total", infantes.count()],
									   ["Adolescentes, primera vez", adolescentes_nuevos.count()],
									   ["Adolescentes, seguimiento", adolescentes.count() - adolescentes_nuevos.count()],
									   ["Adolescentes, total", adolescentes.count()],
									   ["Mujeres, primera vez", adultos_nuevos.count()],
									   ["Mujeres, seguimiento", adultos.count() - adultos_nuevos.count()],
									   ["Mujeres, total", adultos.count()],
									   ["Total", infantes.count() + adolescentes.count() + adultos.count()]], [2, 5, 8, 9]))
	inf_por_departamento = por_departamento(infantes)
	ado_por_departamento = por_departamento(adolescentes)
	adul_por_departamento = por_departamento(adultos)
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(departamento_tabla("Infantes por departamento", inf_por_departamento))
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(departamento_tabla("Adolescentes por departamento", ado_por_departamento))
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(departamento_tabla("Mujeres por departamento", adul_por_departamento))
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(departamento_tabla("Totales por departamento", por_departamento(datos)))
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(violencia_tabla("Tipos de Violencia, Infantes", por_tipo_violencia_infante(sociales_infantes(datos))))
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(violencia_tabla("Tipos de Violencia, Adolescentes", por_tipo_violencia_adolescente(sociales_adolescentes(datos))))
	contenido.append(Spacer(1,0.5*inch))
	contenido.append(violencia_tabla("Tipos de Violencia, Mujeres", por_tipo_violencia_mujer(sociales_adultos(datos))))
	doc.build(contenido, onFirstPage = primeraPagina, onLaterPages = siguientesPaginas)

	

@login_required
def generar(solicitud):
	f = CrearInformeFormulario(solicitud.POST or None)
	if f.is_valid():
		respuesta = HttpResponse(content_type='application/pdf')
		dibujarPdf(f.cleaned_data['FechaInicio'], f.cleaned_data['FechaFinal'], respuesta)
		return respuesta
	return render(solicitud, 'elegir_fecha.html', {'form': f})


