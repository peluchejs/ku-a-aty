# coding=utf-8
from __future__ import unicode_literals

class DictionaryAttr(object):
    def __init__(self, **kwargs):
        for k in kwargs:
            setattr(self, k, kwargs[k])