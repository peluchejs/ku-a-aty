# coding=utf-8
from django.test import TestCase
from django.test.client import Client
from django.db import models

from widgets import CheckboxSelectOther
from autoview import AutoVista, LinkField

class WidgetsTest(TestCase):
    def testCheckbox(self):
        cb = CheckboxSelectOther()
        html = cb.render("cb", ("a", "c",), attrs={"id": "prueba"}, choices = (("a", "a"),("b","b"),("c","c"),))
        expected_html = '''<ul>\n<li><label for="prueba_0"><input checked="checked" id="prueba_0" name="cb" type="checkbox" value="a" /> a</label></li>\n<li><label for="prueba_1"><input id="prueba_1" name="cb" type="checkbox" value="b" /> b</label></li>\n<li><label for="prueba_2"><input checked="checked" id="prueba_2" name="cb" type="checkbox" value="c" /> c</label></li>\n<li><label for="prueba_2"><input id="prueba_2" name="cb" type="checkbox" value="-1" /> <input name="cb-otro" placeholder="Otro" type="text" /></label></li>\n</ul>'''
        self.assertEqual(html, expected_html)
        html = cb.render("cb", ("a", "c",), attrs={}, choices = (("a", "a"),("b","b"),("c","c"),))
        expected_html = '''<ul>\n<li><label><input checked="checked" name="cb" type="checkbox" value="a" /> a</label></li>\n<li><label><input name="cb" type="checkbox" value="b" /> b</label></li>\n<li><label><input checked="checked" name="cb" type="checkbox" value="c" /> c</label></li>\n<li><label><input name="cb" type="checkbox" value="-1" /> <input name="cb-otro" placeholder="Otro" type="text" /></label></li>\n</ul>'''
        self.assertEqual(html, expected_html)
        html = cb.render("cb", [])
        expected_html = '''<ul>\n<li><label><input name="cb" type="checkbox" value="-1" /> <input name="cb-otro" placeholder="Otro" type="text" /></label></li>\n</ul>'''
        self.assertEqual(html, expected_html)
        values = cb.value_from_datadict({"prueba": ["-1"], "prueba-otro": "Nuevo"}, [], "prueba")
        self.assertEqual(values, ['Nuevo'])

class TestRelatedModel(models.Model):
    nombre = models.CharField(max_length = 10)
    
    def get_absolute_url(self):
        return "http://www.google.com/"

class MyModel(models.Model):
    related = models.ForeignKey(TestRelatedModel)
    strField = models.CharField(max_length = 10)
    boolField = models.BooleanField()
    ignored = models.CharField(max_length = 3)

class TestVista(AutoVista):
    class Meta:
        fields = {
            "related": LinkField,
        }
        
        exclude = ("ignored",)

class AutoViewTests(TestCase):
    def testAutoVista(self):
        trm = TestRelatedModel(nombre = "Hi")
        m = MyModel(related = trm, strField = "Stuff", boolField = True, ignored = "HEY")
        tv = TestVista(m)
        self.assertEqual(tv.instance, m)
        self.assertEqual(len(tv.fields), 3) 