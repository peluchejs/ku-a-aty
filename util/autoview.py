# coding=utf-8
from __future__ import unicode_literals
from django.db.models import ForeignKey, OneToOneField, ManyToManyField, AutoField, BooleanField
from django.db.models.fields.related import RelatedField
from django.utils.html import mark_safe, escape
from collections import OrderedDict

class AVFieldBase(object):
    def __init__(self, field):
        self._field = field
    
    def get_value(self, instance):
        if isinstance(self._field, RelatedField):
            return getattr(instance, self._field.name)
        if hasattr(instance, "get_%s_display" % (self._field.name,)):
            return getattr(instance, "get_%s_display" % (self._field.name,))()
        return self._field.value_from_object(instance)
        
    def get_name(self, instance):
        return escape(unicode(self._field.verbose_name.decode("utf-8")))
    
    def _convert(self, instance):
        val = self.get_value(instance)
        if val is None:
            val = ""
        return escape(unicode(val))
        
    def render(self, instance, start, seperator, end):
        return mark_safe("%(empezar)s%(campo)s%(intermedio)s%(valor)s%(terminar)s" % {"empezar": start,
                                                                            "campo": self.get_name(instance),
                                                                            "intermedio": seperator,
                                                                            "valor": self._convert(instance),
                                                                            "terminar": end})

class AutoVistaField(AVFieldBase):
    def __init__(self, field, vista = None):
        super(AutoVistaField, self).__init__(field)
        self._vistaType = vista or AutoVista

    def _convert(self, instance):
        if isinstance(self._field, ForeignKey) or isinstance(self._field, OneToOneField):
            if not hasattr(self, "_vista"):
                value = self.get_value(instance)
                if value is not None:
                    self._vista = self._vistaType(value)
                else:
                    self._vista = None
            if self._vista is not None:
                return "<table>%s</table>" % self._vista.as_table()
        return super(AutoVistaField, self)._convert(instance)

def OverloadedAVField(vista):
    def createField(f):
        return AutoVistaField(f, vista = vista)
    return createField
        
class LinkField(AVFieldBase):
    def __init__(self, field):
        super(LinkField, self).__init__(field)
        if isinstance(field, ForeignKey) or isinstance(field, OneToOneField):
            self._old_convert = self._convert
            self._convert = self._make_link
    
    def _make_link(self, instance):
        value = self.get_value(instance)
        if not hasattr(value, "get_absolute_url"):
            return self._old_convert(instance)
        return "<a href='%(href)s'>%(value)s</a>" % {"href": value.get_absolute_url(), "value": self._old_convert(instance)}

class ListField(AVFieldBase):
    def _convert(self, instance):
        val = self.get_value(instance)
        if val is None:
            val = ""
        else:
            val = val.all()
            return "<ul>%s</ul>" %  "".join(map(lambda x: "<li>%s</li>" % x, map(escape, map(unicode, val))))
        return escape(unicode(val))

class StringField(AVFieldBase):
    def get_value(self, instance):
        if isinstance(self._field, RelatedField):
            return getattr(instance, self._field.name)
        if hasattr(instance, "get_%s_display" % self._field.name):
            return getattr(instance, "get_%s_display" % self._field.name)()
        return self._field.value_from_object(instance)

class ExtraDataFieldInner(StringField):
    def __init__(self, field, **kwargs):
        super(ExtraDataFieldInner, self).__init__(field)
        self._kwargs = kwargs
    
    def _resolve_key(self, instance, k):
        if callable(self._kwargs[k]):
            return unicode(self._kwargs[k](instance))
        return unicode(self._kwargs[k])
        
    def render(self, instance, start, seperator, end):
        basic = super(ExtraDataFieldInner, self).render(instance, start, seperator, end)
        extras = [mark_safe("%(empezar)s%(campo)s%(intermedio)s%(valor)s%(terminar)s" % {"empezar": start,
                                                                                         "campo": unicode(k),
                                                                                         "intermedio": seperator,
                                                                                         "valor": self._resolve_key(instance, k),
                                                                                         "terminar": end}) for k in self._kwargs]
        extra_joined = u"".join(extras)
        return basic + extra_joined

def ExtraDataField(**kwargs_outer):
    def create(f):
        return ExtraDataFieldInner(f, **kwargs_outer)
    return create

class SiNoField(AVFieldBase):
    def _convert(self, instance):
        if isinstance(self._field, BooleanField):
            value = self.get_value(instance)
            return "Sí" if value else "No"
        return super(SiNoField, self)._convert(instance)

class SinglePropertyField(AVFieldBase):
    def __init__(self, field, property_name = None):
        super(self, SinglePropertyField).__init__(field)
        self._prop_name = property_name
        
    def get_value(self, instance):
        if isinstance(self._field, RelatedField):
            return getattr(instance, self._field.name)
        if hasattr(instance, "get_%s_display" % self._field.name):
            return getattr(instance, "get_%s_display" % self._field.name)()
        return self._field.value_from_object(instance)

def SinglePropertyLoader(prop_name):
    def createField(f):
        return SinglePropertyField(f, property_name = prop_name)
    return createField

class AutoVista(object):
    def __init__(self, instance):
        self._instance = instance
        field_dict = {}
        self._fields = OrderedDict()
        self._exclude = []
        if hasattr(self.__class__, "Meta"):
            if hasattr(self.__class__.Meta, "exclude"):
                self._exclude = list(self.__class__.Meta.exclude)
            if hasattr(self.__class__.Meta, "fields"):
                field_dict = dict(self.__class__.Meta.fields.items())
        self._load_appropriate_fields(instance._meta.fields, field_dict)
        self._load_appropriate_fields(instance._meta.many_to_many, field_dict)

    def _load_appropriate_fields(self, possibles, field_dict):
        for f in possibles:
            if f.name in self._exclude:
                continue
            if f.name in field_dict:
                self._fields[f.name] = field_dict[f.name]
            else:
                self._fields[f.name] = self._make_field(f)
            if self._fields[f.name] is not None:
                self._fields[f.name] = self._fields[f.name](f)
            else:
                del self._fields[f.name]

    def _make_field(self, field):
        if isinstance(field, AutoField) or field.auto_created:
            return None
        if isinstance(field, ManyToManyField):
            return ListField
        if isinstance(field, RelatedField):
            return AutoVistaField
        if isinstance(field, BooleanField):
            return SiNoField
        return StringField

    @property
    def fields(self):
        return self._fields
    
    def _generar_texto(self, empezar, intermedio, terminar):
        return u"".join([self._fields[f].render(self._instance, empezar, intermedio, terminar) for f in self._fields])

    def as_table(self):
        return mark_safe(u"<tbody>%s</tbody>" % self._generar_texto(u"<tr><td>", u"</td><td>", u"</td></tr>"))
    
    def as_p(self):
        return mark_safe(self._generar_texto(u"<p>", u": ", u"</p>"))
    
    @property
    def instance(self):
        return self._instance