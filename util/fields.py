# coding=utf-8

from __future__ import unicode_literals
from django.forms.models import ModelMultipleChoiceField
from widgets import CheckboxSelectOther
from django.core.exceptions import ValidationError
from django.utils.encoding import force_text

class AgregarOpcionCampo(ModelMultipleChoiceField):
    widget = CheckboxSelectOther
    def clean(self, value):
        if self.required and not value:
            raise ValidationError(self.error_messages['required'])
        elif not self.required and not value:
            return self.queryset.none()
        if not isinstance(value, (list, tuple)):
            raise ValidationError(self.error_messages['list'])
        key = self.to_field_name or 'pk'
        for i, pk in enumerate(value):
            try:
                self.queryset.filter(**{key: pk})
            except ValueError:
                value[i] = self.queryset.get_or_create(nombre = pk)[0].pk
        qs = self.queryset.filter(**{'%s__in' % key: value})
        pks = set([force_text(getattr(o, key)) for o in qs])
        for val in value:
            if force_text(val) not in pks:
                raise ValidationError(self.error_messages['invalid_choice'] % val)
        # Since this overrides the inherited ModelChoiceField.clean
        # we run custom validators here
        self.run_validators(value)
        return qs