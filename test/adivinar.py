from faker import Factory
import random
from datetime import datetime

faker = Factory.create("pt_BR")

def randomOption(opts):
    def f():
        return opts[random.randint(0, len(opts) - 1)]
    return f

guesses = {
        "nombre": faker.first_name,
        "apellido": faker.last_name,
        "first_name": faker.first_name,
        "last_name": faker.last_name,
        "cedula": lambda:random.randint(0, 9999999),
        "sexo": randomOption(["F", "M"]),
        "nacionalidad": faker.country,
        "fecha": lambda: "%s/%s/%s" % (faker.day_of_month(), faker.month(), faker.year()),
        "lugardenacimiento": faker.city,
        "telefono": faker.phone_number,
        "correo": faker.email,
        "academico": randomOption(range(1,7)),
        "escolar": randomOption(["Primario", "Secundario", "Colegio", "Universitario"]),
        "edad": lambda: random.randint(2,100),
        "parentezco": randomOption(["Papa", "Mama", "Tio", "Tia", "Abuelo", "Abuela"]),
        "estadocivil": randomOption(range(6)),
        "vivienda": randomOption(["Propia", "Alquilada", "Cedida", "Casa de familiares", "Casa de amigos", "Casa De Familiares", "Carpa"]),
        "direccion-direccion": faker.street_address,
        "barrio": randomOption(["Santa Elena", "Ybaroty", "Centro"]),
        "ciudad": faker.city,
        "departamento": faker.estado_nome,
        "trabajo": faker.word,
        "cargo": faker.word,
        "ingreso": lambda: random.randint(0,9999999999),
        "vivecon": faker.word,
        "relacionpadre": randomOption(["S", "N", "F"]),
        "username": faker.word,
        "email": faker.email,
        "password1": lambda: "nalena",
        "password2": lambda: "nalena",
        "nro": lambda: random.randint(1,10),
        "ficha": lambda: random.randint(15,55),
        "texto": faker.paragraph
}

def guessValue(id, cv, prob = 0.95):
    if random.random() < prob:
        id = id.lower()
        for k,v in guesses.items():
            if k in id:
                return unicode(v())
    return cv

def pickSelectValue(s):
    opts = s.find_all("option")
    index = random.randint(0, len(opts) - 1)
    opt = opts[index]
    return opt["value"] if "value" in opt.attrs else "" 