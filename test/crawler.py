import urllib2
from urllib import urlencode
import urlparse
import bs4
import cookielib
import re
from adivindar import guessValue, pickSelectValue

settings = {
    "BASE_URL": "http://localhost:8000",
    "LOGIN_PAGE": "/Usuarios/Ingresar",
    "USERNAME": "jon",
    "PASSWORD": "nalena",
    "FILL_FORMS": True,
    "BLACKLIST": [re.compile("Bloquear"), re.compile("Salir")],
    "VALIDATE_W3C": False,
    "MAX_CRAWLS": 2
}

class Url(object):
    def __init__(self, url, source):
        self.url = url
        self.source = source
    
    def display(self):
        print "From %s\n\t%s" % (self.source, self.url,)
    
    def __hash__(self):
        return hash(self.url)

    def __eq__(self, other):
        return self.url == other.url
        
class Context(object):
    def __init__(self):
        self._toCrawl = set()
        self._crawled = set()
        self._crawlCount = {}
    def pushUrl(self, url):
        if url not in self._crawled:
            if all(map(lambda x: x.search(url.url) is None, settings["BLACKLIST"])):
                self._toCrawl.add(url)
    def links(self):
        while len(self._toCrawl):
            url = self._toCrawl.pop()
            self._crawlCount[url] = self._crawlCount.get(url, 0) + 1
            print "Crawl count %i" % (self._crawlCount[url],)
            if self._crawlCount[url] >= settings["MAX_CRAWLS"]:
                self._crawled.add(url)
                if len(self._crawled) % 10 == 0:
                    print "**************  Crawled %i urls  ******************" % (len(self._crawled),)
            yield url

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
urllib2.install_opener(opener)

class MissingParamException(Exception):
    pass

def combine(*args):
    for a in args:
        for b in a:
            yield b

def submit_form(form, values, url=None, base_url=settings['BASE_URL']):
    for (n,v) in values:
        ele = form.find("input", {"name": n})
        if ele:
            ele["value"] = v
        else:
            ele = form.find("select", {"name": n})
            if ele:
                ele["value"] = v
    for s in form.find_all("select"):
        if "value" not in s.attrs:
            sel_opt = s.find("option", {"selected": "selected"})
            s["value"] = sel_opt["value"] if "value" in sel_opt.attrs else ""
    action = form["action"]
    method = form["method"]
    if action == "." or action == "":
        action = url
    if not action:
        raise Exception("Missing parameter %s" % url)
    action = urlparse.urljoin(base_url, action)
    action_parts = list(urlparse.urlparse(action))
    action_parts[4] = action_parts[5] = ""
    action = urlparse.urlunparse(action_parts)
    data = urlencode(list(combine([(x["name"], (x["value"].encode('utf-8') if "value" in x.attrs else "")) for x in form.find_all("input") if 'name' in x.attrs],
                             [(x["name"], (x["value"].encode('utf-8') if "value" in x.attrs else "")) for x in form.find_all("select") if 'name' in x.attrs]
                            )))
    if method.lower() == "get":
        return urllib2.urlopen("%s?%s" % (action, data))
    else:
        return urllib2.urlopen(action, data)

def fill_form(form):
    ins = form.find_all("input")
    sels = form.find_all("select")
    vals = list(combine([(i["name"], guessValue(i["name"], i["value"] if "value" in i.attrs else ""),) for i in ins if "name" in i.attrs],
                        [(s["name"], pickSelectValue(s),) for s in sels if "name" in s.attrs]))
    return vals

def login():
    pg = urllib2.urlopen(settings["BASE_URL"] + settings["LOGIN_PAGE"])
    soup = bs4.BeautifulSoup(pg)
    login_form = soup.form
    return submit_form(login_form, (("username", settings["USERNAME"]), ("password", settings["PASSWORD"])), pg.geturl())

def logout():
    pass

def handleW3C(page, c, base):
    w3cResults = submit_form(c.cachedW3C, (("fragment", page),), base_url = base)
    soup = bs4.BeautifulSoup(w3cResults)

def handle_page(page, c):
    pageData = page.read()
    soup = bs4.BeautifulSoup(pageData)
    ## First, load links
    for a in soup.find_all('a'):
        if 'href' in a.attrs:
            c.pushUrl(Url(urlparse.urljoin(page.geturl(), a['href']), page.geturl()))
    ## Next, submit all forms without values
    for f in soup.find_all('form'):
        pg = submit_form(f, (), urlparse.urljoin(settings["BASE_URL"], page.geturl()))
        c.pushUrl(Url(urlparse.urljoin(settings['BASE_URL'], pg.geturl()), page.geturl()))
    if settings["FILL_FORMS"]:
        for f in soup.find_all('form'):
            pg = submit_form(f, fill_form(f), urlparse.urljoin(settings["BASE_URL"], page.geturl()))
            c.pushUrl(Url(urlparse.urljoin(settings['BASE_URL'], pg.geturl()), page.geturl()))
    if settings["VALIDATE_W3C"]:
        if not hasattr(c, "cachedW3C"):
            print "Opening W3C validator."
            w3csoup = bs4.BeautifulSoup(urllib2.urlopen("http://validator.w3.org/"))
            fieldset = w3csoup.find("fieldset", id="validate-by-input")
            c.cachedW3C = fieldset.find("form")
        handleW3C(pageData,c, "http://validator.w3.org/")
                
def crawl(c):
    for url in c.links():
        url.display()
        pg = urllib2.urlopen(url.url)
        handle_page(pg, c)

def empieza():
    start_url = login().geturl()
    c = Context()
    c.pushUrl(Url(urlparse.urljoin(settings["BASE_URL"], start_url), "**START**"))
    crawl(c)
    logout()
    return c

if __name__ == '__main__':
    start = datetime.now()
    c = empieza()
    end = datetime.now()
    print "Crawled %i links in %i seconds" % (len(c._crawled), (end - start).seconds)