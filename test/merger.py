import os

def listFiles(start, fileType):
    for dirpath, dirnames, filenames in os.walk(start):
        for f in filenames:
            if f.endswith(fileType):
                yield os.path.join(dirpath, f)

def dupLines(f):
    with open(f) as fin:
        lines = list(fin)
        for i in xrange(len(lines)):
            dupFound = False
            for j in xrange(i+1, len(lines)):
                if lines[i] == lines[j] and lines[i].strip() != '':
                    dupFound = True
                    break
            if dupFound:
                k = j
                while i < k and j < len(lines):
                    if lines[i] != lines[j]:
                        dupFound = False
                        break
                    i += 1
                    j += 1
            if dupFound:
                print "Possible duplicate in file %s at line %i" % (f, k+1,)

def empieza():
    for f in listFiles("..", ".py"):
        dupLines(f)

if __name__ == '__main__':
    empieza()