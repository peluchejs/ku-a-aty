# coding=utf-8
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from Usuarios.models import KunaAtyUsuario
from views import generar

class ViewsTests(TestCase):
    def setUp(self):
        self.client = Client()
        user = KunaAtyUsuario(username = "admin", is_staff = True)
        user.set_password("testpassword")
        user.save()
    def testGenerarBasico(self):
        response = self.client.get(reverse(generar))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(self.client.login(username = "admin", password = "testpassword"))
        response = self.client.get(reverse(generar))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse(generar), data = {})
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse(generar), data = {"TipoDeInforme": 0, "FechaInicio": "01/01/1980", "FechaFinal": "29/05/2014"})
        self.assertEqual(response.status_code, 200)
        self.assertIn('pdf', response['Content-Type'])
        self.client.logout()
