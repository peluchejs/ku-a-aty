# coding=utf-8
from django import forms


class CrearInformeFormulario(forms.Form):
	FechaInicio = forms.DateField(label = "Fecha de Inicio", widget=forms.TextInput(attrs={"class":"datepicker"}))
	FechaFinal = forms.DateField(label = "Fecha Final", widget=forms.TextInput(attrs={"class":"datepicker"}))
   
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

	def clean(self):
		cleaned_data = super(CrearInformeFormulario, self).clean()
		if 'FechaInicio' not in cleaned_data or 'FechaFinal' not in cleaned_data:
			raise forms.ValidationError('Se requieren las fechas')
		if cleaned_data['FechaInicio'] >= cleaned_data['FechaFinal']:
			raise forms.ValidationError("Final tiene que ser después de inicio.")
		return cleaned_data
