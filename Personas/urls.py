# coding=utf-8
from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, CreateView
from django.contrib.auth.decorators import login_required
from models import DatosDePacienteAdulto, DatosDePacienteAdolescente

urlpatterns = patterns('',
	url(r'^Agregar/', "Personas.views.Agregar", name="agregar_persona"),
	url(r'^Adulto/Agregar/$', "Personas.views.AdultoFormulario", name="agregar_adulto"),
	url(r'^Adulto/(?P<pk>\d+)/Modificar/$', "Personas.views.AdultoFormulario", name="modificar_adulto"),
	url(r'^Buscar/Modificar/(?P<pk>\d+)/$', "Personas.views.ModificarAdulto", name="modificar_datos"),
	url(r'^Adulto/(?P<pk>\d+)/$', "Personas.views.ver_adulto", name="ver_adulto"),
	url(r'^Adolescente/Agregar/$', "Personas.views.VistaFormulario", name="agregar_adolescente"),
	url(r'^Adolescente/(?P<pk>\d+)/Modificar/$', "Personas.views.ModificarAdolescente", name="modificar_adolescente"),
	url(r'^Adolescente/(?P<pk>\d+)/$', "Personas.views.ver_adolescente", name="ver_adolescente"),
	url(r'^Infante/Agregar/$', "Personas.views.VistaFormularioInfante", name="agregar_infante"),
	url(r'^Infante/(?P<pk>\d+)/Modificar/$', "Personas.views.ModificarInfante", name="modificar_infante"),
	url(r'^Infante/(?P<pk>\d+)/$', "Personas.views.ver_infante", name="ver_infante"),
	url(r'^Buscar/$', 'Personas.views.BuscarPersona', name="buscar_persona"),
	url(r'^DatosListado/(?P<tipo>\w+)$', 'Personas.views.DatosListado', name = "listado_datos"),
	url(r'^Imprimir/(?P<paciente>\d+)/$','Personas.views.imprimir_adulto', name="imprimir_adulto"),
	)