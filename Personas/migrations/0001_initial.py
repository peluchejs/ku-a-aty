# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pais'
        db.create_table(u'Personas_pais', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'Personas', ['Pais'])

        # Adding model 'Departamento'
        db.create_table(u'Personas_departamento', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'Personas', ['Departamento'])

        # Adding model 'Ciudad'
        db.create_table(u'Personas_ciudad', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=45)),
        ))
        db.send_create_signal(u'Personas', ['Ciudad'])

        # Adding model 'Barrio'
        db.create_table(u'Personas_barrio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'Personas', ['Barrio'])

        # Adding model 'TipoVivienda'
        db.create_table(u'Personas_tipovivienda', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal(u'Personas', ['TipoVivienda'])

        # Adding model 'Direccion'
        db.create_table(u'Personas_direccion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Direccion', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('Barrio', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.Barrio'])),
            ('Ciudad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.Ciudad'])),
            ('Departamento', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.Departamento'])),
        ))
        db.send_create_signal(u'Personas', ['Direccion'])

        # Adding model 'DatosPersonalesBasicos'
        db.create_table(u'Personas_datospersonalesbasicos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('Apellido', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Cedula', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('Sexo', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Nacionalidad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.Pais'])),
            ('FechaNacimiento', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('LugarDeNacimiento', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.Ciudad'])),
            ('Direccion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Personas.Direccion'], null=True, blank=True)),
            ('Telefono', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('Correo', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('FechaCreada', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
        ))
        db.send_create_signal(u'Personas', ['DatosPersonalesBasicos'])

        # Adding model 'DatosLaborales'
        db.create_table(u'Personas_datoslaborales', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('LugarDeTrabajo', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('Cargo', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('Ingreso', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosLaborales'])

        # Adding model 'DatosLaboralesAdolescentes'
        db.create_table(u'Personas_datoslaboralesadolescentes', (
            (u'datoslaborales_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosLaborales'], unique=True, primary_key=True)),
            ('EdadCuandoEmpezo', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('PorqueTrabaja', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('AQuienBeneficia', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosLaboralesAdolescentes'])

        # Adding model 'CasaUtilidades'
        db.create_table(u'Personas_casautilidades', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal(u'Personas', ['CasaUtilidades'])

        # Adding model 'EscolaridadAdolescente'
        db.create_table(u'Personas_escolaridadadolescente', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('InstitucionEscolar', self.gf('django.db.models.fields.CharField')(max_length=35, null=True, blank=True)),
            ('Ciudad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.Ciudad'])),
            ('Transporte', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('NivelEscolar', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('HabilidadEscolar', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['EscolaridadAdolescente'])

        # Adding model 'DatosDeViviendaAdolescente'
        db.create_table(u'Personas_datosdeviviendaadolescente', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('TipoVivienda', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.TipoVivienda'])),
            ('Construccion', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('CantidadHabitantes', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('CantidadHabitaciones', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosDeViviendaAdolescente'])

        # Adding M2M table for field Utilidades on 'DatosDeViviendaAdolescente'
        m2m_table_name = db.shorten_name(u'Personas_datosdeviviendaadolescente_Utilidades')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('datosdeviviendaadolescente', models.ForeignKey(orm[u'Personas.datosdeviviendaadolescente'], null=False)),
            ('casautilidades', models.ForeignKey(orm[u'Personas.casautilidades'], null=False))
        ))
        db.create_unique(m2m_table_name, ['datosdeviviendaadolescente_id', 'casautilidades_id'])

        # Adding model 'DatosDeIngresoFamiliar'
        db.create_table(u'Personas_datosdeingresofamiliar', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Observacion', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosDeIngresoFamiliar'])

        # Adding model 'ContribuidorAlIngresoFamiliar'
        db.create_table(u'Personas_contribuidoralingresofamiliar', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Parentezco', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('LugarDeTrabajo', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('Ingreso', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('Horario', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('IngresoFamiliarDatos', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Personas.DatosDeIngresoFamiliar'])),
        ))
        db.send_create_signal(u'Personas', ['ContribuidorAlIngresoFamiliar'])

        # Adding model 'HabitosAdolescentes'
        db.create_table(u'Personas_habitosadolescentes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Tabaco', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('CantidadYFrecuenciaTabaco', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('AlguienEnLaCasaConsumeTabaco', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Alcohol', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('CantidadYFrecuenciaAlcohol', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('AlguienEnLaCasaConsumeAlcohol', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('QuienConsumeAlcohol', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('CantidadYFrecuenciaCohabitantesAlcohol', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('UsoDeDrogasAlgunaVez', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('UsoDeDrogasActual', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Personas', ['HabitosAdolescentes'])

        # Adding model 'NoviazgoAdolescente'
        db.create_table(u'Personas_noviazgoadolescente', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('TieneNovios', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('CuantosTiene', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('TuvoNovios', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('CuantosTenia', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['NoviazgoAdolescente'])

        # Adding model 'DatosAgresor'
        db.create_table(u'Personas_datosagresor', (
            (u'datospersonalesbasicos_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosPersonalesBasicos'], unique=True, primary_key=True)),
            ('NivelAcademico', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Laborales', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosLaborales'], unique=True, null=True, blank=True)),
            ('TieneExclusiondelHogar', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosAgresor'])

        # Adding model 'DatosDePacienteAdulto'
        db.create_table(u'Personas_datosdepacienteadulto', (
            (u'datospersonalesbasicos_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosPersonalesBasicos'], unique=True, primary_key=True)),
            ('NivelAcademico', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Laborales', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosLaborales'], unique=True, null=True, blank=True)),
            ('EstadoCivil', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('TipoDeVivienda', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.TipoVivienda'])),
        ))
        db.send_create_signal(u'Personas', ['DatosDePacienteAdulto'])

        # Adding model 'DatosDelHijo'
        db.create_table(u'Personas_datosdelhijo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Madre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Personas.DatosDePacienteAdulto'], null=True, blank=True)),
            ('Nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('Apellido', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Cedula', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('Sexo', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('ViveCon', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('RelacionPadre', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosDelHijo'])

        # Adding model 'DatosDePacienteAdolescente'
        db.create_table(u'Personas_datosdepacienteadolescente', (
            (u'datospersonalesbasicos_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosPersonalesBasicos'], unique=True, primary_key=True)),
            ('AQuienPertaneceTelefono', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('TieneHijos', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('CantidadDeHijos', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0, null=True, blank=True)),
            ('ConQuienVive', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('NombreMama', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('NombrePapa', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('Tutor', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('Escolaridad', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.EscolaridadAdolescente'], unique=True, null=True)),
            ('Laborales', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosLaboralesAdolescentes'], unique=True, null=True)),
            ('Vivienda', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosDeViviendaAdolescente'], unique=True, null=True)),
            ('IngresoFamiliar', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosDeIngresoFamiliar'], unique=True, null=True)),
            ('Habitos', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.HabitosAdolescentes'], unique=True, null=True)),
            ('NoviazgoAdolescente', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.NoviazgoAdolescente'], unique=True, null=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosDePacienteAdolescente'])

        # Adding model 'DatosFamiliaresAdolescentes'
        db.create_table(u'Personas_datosfamiliaresadolescentes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Adolescente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Personas.DatosDePacienteAdolescente'])),
            ('Nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('Apellido', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Edad', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('NivelEscolar', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('Parentezco', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosFamiliaresAdolescentes'])

        # Adding model 'RelacionConNino'
        db.create_table(u'Personas_relacionconnino', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'Personas', ['RelacionConNino'])

        # Adding model 'InfanteFamiliar'
        db.create_table(u'Personas_infantefamiliar', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Nombre', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('Apellido', self.gf('django.db.models.fields.CharField')(max_length=35, null=True, blank=True)),
            ('Edad', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Ocupacion', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('Telefono', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['InfanteFamiliar'])

        # Adding model 'InfanteEscolaridad'
        db.create_table(u'Personas_infanteescolaridad', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Institucion', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('Grado', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('Direccion', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.Direccion'], unique=True, null=True)),
            ('Maestro', self.gf('django.db.models.fields.CharField')(max_length=35, null=True, blank=True)),
            ('Desempeno', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('Repitencia', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Disercion', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Observacion', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['InfanteEscolaridad'])

        # Adding model 'EncargadaDeNino'
        db.create_table(u'Personas_encargadadenino', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('Apellido', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Direccion', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.Direccion'], unique=True, null=True)),
            ('Telefono', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('RelacionConNino', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.RelacionConNino'])),
            ('NivelDeInstruccionAcademica', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('EstadoCivil', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('EsUsuariaDeLaFundacion', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Personas', ['EncargadaDeNino'])

        # Adding model 'DatosFamiliaresInfantiles'
        db.create_table(u'Personas_datosfamiliaresinfantiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Madre', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', null=True, to=orm['Personas.InfanteFamiliar'])),
            ('Padre', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['Personas.InfanteFamiliar'])),
            ('CantidadMatrimonios', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('CantidadHijos', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Edades', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=40, null=True, blank=True)),
            ('Observacion', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosFamiliaresInfantiles'])

        # Adding model 'DatosInfantiles'
        db.create_table(u'Personas_datosinfantiles', (
            (u'datospersonalesbasicos_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosPersonalesBasicos'], unique=True, primary_key=True)),
            ('Sobrenombre', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('CedulaMama', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('Encargada', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.EncargadaDeNino'], unique=True, null=True)),
            ('ConQuienVive', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('IdiomaEnCasa', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('TipoRelacion', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Familiares', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.DatosFamiliaresInfantiles'], unique=True, null=True)),
            ('Escolaridad', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Personas.InfanteEscolaridad'], unique=True, null=True)),
        ))
        db.send_create_signal(u'Personas', ['DatosInfantiles'])


    def backwards(self, orm):
        # Deleting model 'Pais'
        db.delete_table(u'Personas_pais')

        # Deleting model 'Departamento'
        db.delete_table(u'Personas_departamento')

        # Deleting model 'Ciudad'
        db.delete_table(u'Personas_ciudad')

        # Deleting model 'Barrio'
        db.delete_table(u'Personas_barrio')

        # Deleting model 'TipoVivienda'
        db.delete_table(u'Personas_tipovivienda')

        # Deleting model 'Direccion'
        db.delete_table(u'Personas_direccion')

        # Deleting model 'DatosPersonalesBasicos'
        db.delete_table(u'Personas_datospersonalesbasicos')

        # Deleting model 'DatosLaborales'
        db.delete_table(u'Personas_datoslaborales')

        # Deleting model 'DatosLaboralesAdolescentes'
        db.delete_table(u'Personas_datoslaboralesadolescentes')

        # Deleting model 'CasaUtilidades'
        db.delete_table(u'Personas_casautilidades')

        # Deleting model 'EscolaridadAdolescente'
        db.delete_table(u'Personas_escolaridadadolescente')

        # Deleting model 'DatosDeViviendaAdolescente'
        db.delete_table(u'Personas_datosdeviviendaadolescente')

        # Removing M2M table for field Utilidades on 'DatosDeViviendaAdolescente'
        db.delete_table(db.shorten_name(u'Personas_datosdeviviendaadolescente_Utilidades'))

        # Deleting model 'DatosDeIngresoFamiliar'
        db.delete_table(u'Personas_datosdeingresofamiliar')

        # Deleting model 'ContribuidorAlIngresoFamiliar'
        db.delete_table(u'Personas_contribuidoralingresofamiliar')

        # Deleting model 'HabitosAdolescentes'
        db.delete_table(u'Personas_habitosadolescentes')

        # Deleting model 'NoviazgoAdolescente'
        db.delete_table(u'Personas_noviazgoadolescente')

        # Deleting model 'DatosAgresor'
        db.delete_table(u'Personas_datosagresor')

        # Deleting model 'DatosDePacienteAdulto'
        db.delete_table(u'Personas_datosdepacienteadulto')

        # Deleting model 'DatosDelHijo'
        db.delete_table(u'Personas_datosdelhijo')

        # Deleting model 'DatosDePacienteAdolescente'
        db.delete_table(u'Personas_datosdepacienteadolescente')

        # Deleting model 'DatosFamiliaresAdolescentes'
        db.delete_table(u'Personas_datosfamiliaresadolescentes')

        # Deleting model 'RelacionConNino'
        db.delete_table(u'Personas_relacionconnino')

        # Deleting model 'InfanteFamiliar'
        db.delete_table(u'Personas_infantefamiliar')

        # Deleting model 'InfanteEscolaridad'
        db.delete_table(u'Personas_infanteescolaridad')

        # Deleting model 'EncargadaDeNino'
        db.delete_table(u'Personas_encargadadenino')

        # Deleting model 'DatosFamiliaresInfantiles'
        db.delete_table(u'Personas_datosfamiliaresinfantiles')

        # Deleting model 'DatosInfantiles'
        db.delete_table(u'Personas_datosinfantiles')


    models = {
        u'Personas.barrio': {
            'Meta': {'object_name': 'Barrio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.casautilidades': {
            'Meta': {'object_name': 'CasaUtilidades'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'Personas.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'Personas.contribuidoralingresofamiliar': {
            'Horario': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'Ingreso': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'IngresoFamiliarDatos': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.DatosDeIngresoFamiliar']"}),
            'LugarDeTrabajo': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'ContribuidorAlIngresoFamiliar'},
            'Parentezco': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datosagresor': {
            'Laborales': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosLaborales']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosAgresor'},
            'NivelAcademico': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'TieneExclusiondelHogar': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'datospersonalesbasicos_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosPersonalesBasicos']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Personas.datosdeingresofamiliar': {
            'Meta': {'object_name': 'DatosDeIngresoFamiliar'},
            'Observacion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datosdelhijo': {
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Cedula': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'Madre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.DatosDePacienteAdulto']", 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosDelHijo'},
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'RelacionPadre': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'Sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'ViveCon': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datosdepacienteadolescente': {
            'AQuienPertaneceTelefono': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'CantidadDeHijos': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'ConQuienVive': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'Escolaridad': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.EscolaridadAdolescente']", 'unique': 'True', 'null': 'True'}),
            'Habitos': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.HabitosAdolescentes']", 'unique': 'True', 'null': 'True'}),
            'IngresoFamiliar': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosDeIngresoFamiliar']", 'unique': 'True', 'null': 'True'}),
            'Laborales': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosLaboralesAdolescentes']", 'unique': 'True', 'null': 'True'}),
            'Meta': {'object_name': 'DatosDePacienteAdolescente', '_ormbases': [u'Personas.DatosPersonalesBasicos']},
            'NombreMama': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'NombrePapa': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'NoviazgoAdolescente': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.NoviazgoAdolescente']", 'unique': 'True', 'null': 'True'}),
            'TieneHijos': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Tutor': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'Vivienda': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosDeViviendaAdolescente']", 'unique': 'True', 'null': 'True'}),
            u'datospersonalesbasicos_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosPersonalesBasicos']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Personas.datosdepacienteadulto': {
            'EstadoCivil': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Laborales': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosLaborales']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosDePacienteAdulto'},
            'NivelAcademico': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'TipoDeVivienda': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.TipoVivienda']"}),
            u'datospersonalesbasicos_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosPersonalesBasicos']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Personas.datosdeviviendaadolescente': {
            'CantidadHabitaciones': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'CantidadHabitantes': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Construccion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosDeViviendaAdolescente'},
            'TipoVivienda': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.TipoVivienda']"}),
            'Utilidades': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Personas.CasaUtilidades']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datosfamiliaresadolescentes': {
            'Adolescente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.DatosDePacienteAdolescente']"}),
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Edad': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosFamiliaresAdolescentes'},
            'NivelEscolar': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'Parentezco': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datosfamiliaresinfantiles': {
            'CantidadHijos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'CantidadMatrimonios': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Edades': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Madre': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.InfanteFamiliar']"}),
            'Meta': {'object_name': 'DatosFamiliaresInfantiles'},
            'Observacion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Padre': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.InfanteFamiliar']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datosinfantiles': {
            'CedulaMama': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'ConQuienVive': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'Encargada': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.EncargadaDeNino']", 'unique': 'True', 'null': 'True'}),
            'Escolaridad': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.InfanteEscolaridad']", 'unique': 'True', 'null': 'True'}),
            'Familiares': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosFamiliaresInfantiles']", 'unique': 'True', 'null': 'True'}),
            'IdiomaEnCasa': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosInfantiles', '_ormbases': [u'Personas.DatosPersonalesBasicos']},
            'Sobrenombre': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'TipoRelacion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'datospersonalesbasicos_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosPersonalesBasicos']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Personas.datoslaborales': {
            'Cargo': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Ingreso': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'LugarDeTrabajo': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosLaborales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datoslaboralesadolescentes': {
            'AQuienBeneficia': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'EdadCuandoEmpezo': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosLaboralesAdolescentes', '_ormbases': [u'Personas.DatosLaborales']},
            'PorqueTrabaja': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'datoslaborales_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosLaborales']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Personas.datospersonalesbasicos': {
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Cedula': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'Correo': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'Direccion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.Direccion']", 'null': 'True', 'blank': 'True'}),
            'FechaCreada': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'FechaNacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'LugarDeNacimiento': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'Meta': {'object_name': 'DatosPersonalesBasicos'},
            'Nacionalidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Pais']"}),
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'Sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.departamento': {
            'Meta': {'object_name': 'Departamento'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.direccion': {
            'Barrio': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Barrio']"}),
            'Ciudad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'Departamento': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Departamento']"}),
            'Direccion': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Direccion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.encargadadenino': {
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Direccion': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.Direccion']", 'unique': 'True', 'null': 'True'}),
            'EsUsuariaDeLaFundacion': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'EstadoCivil': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'EncargadaDeNino'},
            'NivelDeInstruccionAcademica': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'RelacionConNino': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.RelacionConNino']"}),
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.escolaridadadolescente': {
            'Ciudad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'HabilidadEscolar': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'InstitucionEscolar': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'EscolaridadAdolescente'},
            'NivelEscolar': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'Transporte': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.habitosadolescentes': {
            'Alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'AlguienEnLaCasaConsumeAlcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'AlguienEnLaCasaConsumeTabaco': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'CantidadYFrecuenciaAlcohol': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'CantidadYFrecuenciaCohabitantesAlcohol': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'CantidadYFrecuenciaTabaco': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'HabitosAdolescentes'},
            'QuienConsumeAlcohol': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Tabaco': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'UsoDeDrogasActual': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'UsoDeDrogasAlgunaVez': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.infanteescolaridad': {
            'Desempeno': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'Direccion': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.Direccion']", 'unique': 'True', 'null': 'True'}),
            'Disercion': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Grado': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'Institucion': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'Maestro': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'InfanteEscolaridad'},
            'Observacion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Repitencia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.infantefamiliar': {
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'Edad': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'InfanteFamiliar'},
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'Ocupacion': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.noviazgoadolescente': {
            'CuantosTenia': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'CuantosTiene': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'NoviazgoAdolescente'},
            'TieneNovios': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'TuvoNovios': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.pais': {
            'Meta': {'object_name': 'Pais'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.relacionconnino': {
            'Meta': {'object_name': 'RelacionConNino'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'Personas.tipovivienda': {
            'Meta': {'object_name': 'TipoVivienda'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        }
    }

    complete_apps = ['Personas']