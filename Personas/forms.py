# coding=utf-8
from collections import defaultdict, OrderedDict
from django import forms
from django.forms.models import modelform_factory
from django.core.urlresolvers import reverse_lazy

from models import DatosDePacienteAdulto, DatosAgresor, DatosDePacienteAdolescente, DatosLaborales
from models import DatosLaboralesAdolescentes, DatosDeViviendaAdolescente, DatosDeIngresoFamiliar, HabitosAdolescentes
from models import NoviazgoAdolescente, EscolaridadAdolescente, ContribuidorAlIngresoFamiliar
from models import EncargadaDeNino, DatosInfantiles, InfanteEscolaridad
from models import DatosFamiliaresAdolescentes, DatosFamiliaresInfantiles, InfanteFamiliar

from models import Pais, Ciudad, Departamento, Barrio, TipoVivienda, Direccion
from fields import AgregarOpcionField, crear_url
from widgets import OpcionesListadoWidget

from django.forms.models import modelformset_factory, BaseModelFormSet
from django.utils.safestring import mark_safe

from util.multiform import MultiModelForm
###################################################################Probando Imprimir
class CrearInformeFormulario(forms.Form):
    FechaInicio = forms.DateField(label = "Fecha de Inicio", widget=forms.TextInput(attrs={"class":"datepicker"}))
    FechaFinal = forms.DateField(label = "Fecha Final", widget=forms.TextInput(attrs={"class":"datepicker"}))
   
    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

    def clean(self):
        cleaned_data = super(CrearInformeFormulario, self).clean()
        if 'FechaInicio' not in cleaned_data or 'FechaFinal' not in cleaned_data:
            raise forms.ValidationError('Se requieren las fechas')
        if cleaned_data['FechaInicio'] >= cleaned_data['FechaFinal']:
            raise forms.ValidationError("Final tiene que ser después de inicio.")
        return cleaned_data
######################################################################
def crear_formulario(modelo, campos, titulo, **kwargs):
    formulario = modelform_factory(modelo, fields = campos, **kwargs)
    formulario.titulo = titulo
    return formulario

def form_header(form):
    cabecera_principio = u"<tr><th>"
    cabecera_fin = u"</th></tr>"
    cabecera_cuerpo = u'</th><th>'.join([campo.label for campo in form.visible_fields()])
    return u"%s%s%s" % (cabecera_principio, cabecera_cuerpo, cabecera_fin)

def form_row(form):
    fila_principio = u"<tr><td>"
    fila_cuerpo = [unicode(campo) for campo in form.visible_fields()]
    fila_cuerpo[-1] += "".join([unicode(campo) for campo in form.hidden_fields()])
    fila_fin = u"</td></tr>"
    return u"%s%s%s" % (fila_principio, "</td><td>".join(fila_cuerpo), fila_fin)

def horizontal_as_table(self):
    cabecera = form_header(self[0])
    filas = [unicode(self.management_form)] + [form_row(f) for f in self]
    tabla = [u"<thead>", cabecera, u"</thead>", u"<tbody>"] + filas + [u"</tbody>"]
    return mark_safe(u'\n'.join(tabla))

class HorizRadioRenderer(forms.RadioSelect.renderer):
    """ this overrides widget method to put radio buttons horizontally
        instead of vertically.
    """
    def render(self):
            """Outputs radios"""
            return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))

class FormularioDireccion(forms.ModelForm):
    Departamento = AgregarOpcionField(queryset = Departamento.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Departamento"), tipo = Departamento),required=False)
    Ciudad = AgregarOpcionField(queryset = Ciudad.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Ciudad"), tipo = Ciudad),required=False)
    Barrio = AgregarOpcionField(queryset = Barrio.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Barrio"), tipo = Barrio),required=False)
    
    titulo = "Dirección"
    
    class Meta:
        model = Direccion
    
    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

class FormularioDatosAdultos(forms.ModelForm):
    Nacionalidad = AgregarOpcionField(queryset = Pais.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Pais"), tipo = Pais), required = False)
    LugarDeNacimiento = AgregarOpcionField(queryset = Ciudad.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Ciudad"), tipo =Ciudad), label = "Lugar de Nacimiento", required=False)
    TipoDeVivienda = AgregarOpcionField(queryset = TipoVivienda.objects.all(), widget = OpcionesListadoWidget(url = crear_url("TipoVivienda"), tipo = TipoVivienda), label = "Tipo de Vivienda", required = False)
    FechaNacimiento = forms.DateField(label = "Fecha de Nacimiento", widget=forms.TextInput(attrs={"class":"datepicker"}), required=False)

    class Meta:
        model = DatosDePacienteAdulto
        exclude = ("Hijos", "Direccion", "FechaCreada", "Laborales")

    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

FormularioLaboralAdulto = crear_formulario(DatosLaborales, ("LugarDeTrabajo", "Cargo", "Ingreso"), "Datos Laborales")

class FormularioDatosAgresorBase(forms.ModelForm):
    Nacionalidad = AgregarOpcionField(queryset = Pais.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Pais"), tipo = Pais), required = False)
    LugarDeNacimiento = AgregarOpcionField(label = "Lugar de Nacimiento", queryset = Ciudad.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Ciudad"), tipo = Ciudad), required = False)
    FechaNacimiento = forms.DateField(label = "Fecha de Nacimiento", widget=forms.TextInput(attrs={"class":"datepicker"}), required = False)
    class Meta:
        model = DatosAgresor
        exclude = ("Direccion","FechaCreada","Laborales",)

class FormularioDatosAgresor(MultiModelForm):
    titulo = "Datos del Agresor"
    base_forms = OrderedDict((('Agresor', FormularioDatosAgresorBase), ('Direccion', FormularioDireccion), ('Laborales', FormularioLaboralAdulto),))
    requerido = False

    def save(self, commit=True):
        """Save both forms and attach the address to the other info."""
        instances = super(FormularioDatosAgresor, self).save(commit=False)
        if commit:
            instances['Direccion'].save()
            instances['Laborales'].save()
        instances['Agresor'].Direccion = instances['Direccion']
        instances['Agresor'].Laborales = instances['Laborales']
        if commit:
            for instance in instances.values():
                instance.save()
        return instances["Agresor"]

class FormularioDatosAdolescentes(forms.ModelForm):
    Nacionalidad = AgregarOpcionField(queryset = Pais.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Pais"), tipo = Pais),required=False)
    LugarDeNacimiento = AgregarOpcionField(queryset = Ciudad.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Ciudad"), tipo = Ciudad), label = "Lugar de Nacimiento", required = False)
    Trabaja = forms.BooleanField(required = False, label = "¿Trabaja?")
    FechaNacimiento = forms.DateField(label = "Fecha de Nacimiento", widget=forms.TextInput(attrs={"class":"datepicker"}),required=False)
    class Meta:
        model = DatosDePacienteAdolescente
        exclude = ("Direccion", "Laborales","Vivienda", "IngresoFamiliar","Habitos", "NoviazgoAdolescente", "Escolaridad","FechaCreada",)
    
    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

class FormularioEscolaridadAdolescente(forms.ModelForm):
    Ciudad = AgregarOpcionField(queryset = Ciudad.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Ciudad"), tipo = Ciudad), label = "Ciudad", required = False)
    titulo = "Escolaridad"
    class Meta:
        model = EscolaridadAdolescente

class FormularioViviendaAdolescente(forms.ModelForm):
    TipoVivienda = AgregarOpcionField(queryset = TipoVivienda.objects.all(), widget = OpcionesListadoWidget(url = crear_url("TipoVivienda"), tipo = TipoVivienda), label = "Tipo de Vivienda", required = False)
    
    titulo = "Datos de Vivienda"
    class Meta:
        model = DatosDeViviendaAdolescente
        widgets = {"Utilidades": forms.widgets.CheckboxSelectMultiple}
    
    def __init__(self, *args, **kwargs):
        super(FormularioViviendaAdolescente, self).__init__(*args, **kwargs)
        ## Problema con Django
        self.fields["Utilidades"].help_text = ""

class HorizontalFormset(BaseModelFormSet):
    def as_table(self):
        return horizontal_as_table(self)

class FormularioContribuidor(forms.ModelForm):
    class Meta:
        model = ContribuidorAlIngresoFamiliar
        exclude = ("IngresoFamiliarDatos",)

FormularioIngresoAdolescente = crear_formulario(DatosDeIngresoFamiliar, ("Observacion", ), "Observacion")
FormsetIngresoFamiliar = modelformset_factory(ContribuidorAlIngresoFamiliar, form = FormularioContribuidor, formset = HorizontalFormset, extra = 1)

class FormularioFamiliares(forms.ModelForm):
    class Meta:
        model = DatosFamiliaresAdolescentes
        exclude = ("Adolescente",)
        
    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

FormsetMiembrosFamilia = modelformset_factory(DatosFamiliaresAdolescentes, form = FormularioFamiliares, formset=HorizontalFormset, extra = 1)
FormsetMiembrosFamilia.titulo = "Miembros de la familia"

FormularioLaboralAdolescente = crear_formulario(DatosLaboralesAdolescentes, ("LugarDeTrabajo", "Cargo", "Ingreso", "EdadCuandoEmpezo", "PorqueTrabaja", "AQuienBeneficia"), "Datos Laborales")
FormularioHabitosAdolescentes = crear_formulario(HabitosAdolescentes, ("Tabaco", "CantidadYFrecuenciaTabaco", "AlguienEnLaCasaConsumeTabaco",
                                                                                 "Alcohol", "CantidadYFrecuenciaAlcohol", "AlguienEnLaCasaConsumeAlcohol",
                                                                                 "QuienConsumeAlcohol", "CantidadYFrecuenciaCohabitantesAlcohol",
                                                                                 "UsoDeDrogasAlgunaVez", "UsoDeDrogasActual"), "Hábitos")
FormularioNoviazgoAdolescente = crear_formulario(NoviazgoAdolescente, ("TieneNovios", "CuantosTiene", "TuvoNovios", "CuantosTenia"), "Noviazgo")

class FormularioInfantil(forms.ModelForm):

    Nacionalidad = AgregarOpcionField(queryset = Pais.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Pais"), tipo = Pais), required = False)
    LugarDeNacimiento = AgregarOpcionField(queryset = Ciudad.objects.all(), widget = OpcionesListadoWidget(url = crear_url("Ciudad"), tipo = Ciudad), label = "Lugar de Nacimiento", required = False)
    AsisteEscuela = forms.BooleanField(label = "¿Asiste escuela?", required = False)
    FechaNacimiento = forms.DateField(label = "Fecha de Nacimiento", widget=forms.TextInput(attrs={"class":"datepicker"}), required = False)
    
    class Meta:
        model = DatosInfantiles
        exclude = ("Direccion", "Encargada", "Familiares", "Escolaridad", "FechaCreada")

    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
        
class FormularioInfantilCombined(MultiModelForm):
    titulo = "Datos Básicos"
    base_forms = OrderedDict([('basicos', FormularioInfantil),
                              ('basicos_dir', FormularioDireccion)])
    
    def save(self, commit=True):
        """Save both forms and attach the address to the other info."""
        instances = super(FormularioInfantilCombined, self).save(commit=False)
        if commit:
            instances['basicos_dir'].save()
            instances['basicos'].Direccion = instances['basicos_dir']
            instances['basicos'].save()
        else:
            instances['basicos'].Direccion = instances['basicos_dir']
        return instances

class FormularioInfanteEscolaridad(forms.ModelForm):
    titulo = "Escolaridad"
    class Meta:
        model = InfanteEscolaridad
        exclude = ("Direccion",)

class FormularioInfanteEscolaridadCombined(MultiModelForm):
    titulo = "Escolaridad"
    
    base_forms = OrderedDict([('escolaridad', FormularioInfanteEscolaridad),
                              ('escolaridad_dir', FormularioDireccion)])
    
    def save(self, commit=True):
        """Save both forms and attach the address to the other info."""
        instances = super(FormularioInfanteEscolaridadCombined, self).save(commit=False)
        if commit:
            instances['escolaridad_dir'].save()
            instances['escolaridad'].Direccion = instances['escolaridad_dir']
            instances['escolaridad'].save()
        else:
            instances['escolaridad'].Direccion = instances['escolaridad_dir']
        return instances

class FormularioInfanteEncargado(forms.ModelForm):
    titulo = "Encargada/o de la/el infante"
    class Meta:
        model = EncargadaDeNino
        widgets = {"RelacionConNino": forms.RadioSelect(renderer = HorizRadioRenderer)}
        exclude = ("Direccion",)
    
    def __init__(self, *args, **kwargs):
        super(FormularioInfanteEncargado, self).__init__(*args, **kwargs)
        self.fields["RelacionConNino"].empty_label = None


class FormularioEncargadaCombined(MultiModelForm):
    titulo = "Encargada"
    
    base_forms = OrderedDict([('encargada', FormularioInfanteEncargado),
                              ('encargada_dir', FormularioDireccion)])

    def save(self, commit=True):
        """Save both forms and attach the address to the other info."""
        instances = super(FormularioEncargadaCombined, self).save(commit=False)
        if commit:
            instances['encargada_dir'].save()
            instances['encargada'].Direccion = instances['encargada_dir']
            instances['encargada'].save()
        else:
            instances['encargada'].Direccion = instances['encargada_dir']
        return instances

class FormularioInfanteFamilia(forms.ModelForm):
    class Meta:
        model = DatosFamiliaresInfantiles
        exclude = ("Madre", "Padre",)

class FormularioPadreInfante(forms.ModelForm):
    Nombre= forms.CharField(required=False,label = "Nombre del Padre")
    Apellido= forms.CharField(required=False,label = "Apellido del Padre")
    Edad= forms.IntegerField(required=False,label = "Edad del Padre")
    class Meta:
        model = InfanteFamiliar
class FormularioMadreInfante(forms.ModelForm):
    Nombre= forms.CharField(required=False,label = "Nombre de la Madre")
    Apellido= forms.CharField(required=False,label = "Apellido de la Madre")
    Edad= forms.IntegerField(required=False,label = "Edad de la Madre")
    class Meta:
        model = InfanteFamiliar
class FormularioFamiliaInfanteCombined(MultiModelForm):
    titulo = "Familia"
    
    base_forms = OrderedDict([('madre', FormularioMadreInfante),
                              ('padre', FormularioPadreInfante),
                              ('familia', FormularioInfanteFamilia)])

    def save(self, commit=True):
        instances = super(FormularioFamiliaInfanteCombined, self).save(commit=False)
        if commit:
            instances['madre'].save()
            instances['padre'].save()
            instances['familia'].Madre = instances['madre']
            instances['familia'].Padre = instances['padre']
            instances['familia'].save()
        else:
            instances['familia'].Madre = instances['madre']
            instances['familia'].Padre = instances['padre']
        return instances

    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
