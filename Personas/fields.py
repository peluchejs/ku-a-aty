# coding=utf-8
from django import forms
from django.core.urlresolvers import reverse_lazy
from widgets import OpcionesListadoWidget

def crear_url(tipo):
    return reverse_lazy("listado_datos", kwargs={"tipo": tipo});

class AgregarOpcionField(forms.ModelChoiceField):
    widget = OpcionesListadoWidget
    
    def __init__(self, *args, **kwargs):
        if "nombre" in kwargs:
            self.nombreDelCampo = kwargs["nombre"]
            del kwargs["nombre"]
        else:
            self.nombreDelCampo = "nombre"
        super(AgregarOpcionField, self).__init__(*args, **kwargs)
    
    def clean(self, value):
        if value is not None and value != "":
            value = value.title()
            obj = self.queryset.get_or_create(**{self.nombreDelCampo: value})
            return super(AgregarOpcionField, self).clean(unicode(obj[0].pk))
        return super(AgregarOpcionField, self).clean(value)