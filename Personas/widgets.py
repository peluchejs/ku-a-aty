# coding=utf-8
from django import forms

class OpcionesListadoWidget(forms.TextInput):
    def __init__(self, url = None, tipo = None, *args, **kwargs):
        if url is not None:
            attrs = kwargs.get("attrs", {})
            attrs["data-url"] = url
            if "class" in attrs:
                attrs["class"] += " listado"
            else:
                attrs["class"] = "listado"
            kwargs["attrs"] = attrs
        if tipo:
            self.tipo = tipo
        super(OpcionesListadoWidget, self).__init__(*args, **kwargs)
    
    def _buscar_nombre(self, value):
        if value is None:
            return u""
        try:
            iv = int(value)
            if self.tipo and self.tipo.objects.filter(pk = iv).exists():
                value = unicode(self.tipo.objects.get(pk = iv))
        except ValueError:
            return value
        return value
    
    def render(self, name, value, attrs=None):
        value = self._buscar_nombre(value)
        return super(OpcionesListadoWidget, self).render(name, value, attrs)

    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/agregarElementos.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

class RelacionOpcionesListadoWidget(OpcionesListadoWidget):
    def __init__(self, related = None, *args, **kwargs):
        if related is not None:
            for r in related:
                pass
        if "url" in kwargs:
            attrs = kwargs.get("attrs", {})
            attrs["data-url"] = kwargs["url"]
            del kwargs["url"]
            if "class" in attrs:
                attrs["class"] += " listado"
            else:
                attrs["class"] = "listado"
            kwargs["attrs"] = attrs
        super(RelacionOpcionesListadoWidget, self).__init__(*args, **kwargs)
