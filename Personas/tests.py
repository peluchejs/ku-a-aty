from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse

from Usuarios.models import KunaAtyUsuario
from fields import AgregarOpcionField
from widgets import OpcionesListadoWidget
from models import Pais
from models import DatosPersonalesBasicos, DatosDePacienteAdulto, DatosAgresor, DatosDePacienteAdolescente, DatosInfantiles

from views import DatosListado, BuscarPersona, AdultoFormulario, Agregar

class CampoTest(TestCase):
    def test_crear(self):
        campo = AgregarOpcionField(queryset = Pais.objects.all())
        self.assertIsInstance(campo.widget, OpcionesListadoWidget)
        self.assertEqual(campo.nombreDelCampo, "nombre")
    
    def test_guardar(self):
        pais_count = Pais.objects.all().count()
        self.assertEqual(Pais.objects.all().filter(nombre = "Noesunpais").count(), 0)
        campo = AgregarOpcionField(queryset = Pais.objects.all())
        valor = campo.clean("NoEsUnPais")
        self.assertIsInstance(valor, Pais)
        self.assertEqual(Pais.objects.all().count() - pais_count, 1)
        self.assertEqual(Pais.objects.all().filter(nombre = "Noesunpais").count(), 1)


class DatosPersonalesBasicosTest(TestCase):
    def test_tipos(self):
        datos = DatosPersonalesBasicos()
        datos.save()
        self.assertIsNone(datos.buscar_tipo)
        datos = DatosDePacienteAdulto()
        datos.save()
        self.assertIsInstance(datos.buscar_tipo, DatosDePacienteAdulto)
        datos = DatosAgresor()
        datos.save()
        self.assertIsInstance(datos.buscar_tipo, DatosAgresor)
        datos = DatosDePacienteAdolescente()
        datos.save()
        self.assertIsInstance(datos.buscar_tipo, DatosDePacienteAdolescente)
        datos = DatosInfantiles()
        datos.save()
        self.assertIsInstance(datos.buscar_tipo, DatosInfantiles)

class ViewsTests(TestCase):
    def setUp(self):
        self.user = KunaAtyUsuario(username = "admin", is_staff = True)
        self.user.set_password("testpassword")
        self.user.save()
        self.client = Client()
    
    def testDatosListado(self):
        self.assertTrue(self.client.login(username = "admin", password = "testpassword"))
        for tipo in ["Pais", "Departamento", "Ciudad", "Barrio", "TipoVivienda"]:
            response = self.client.get(reverse(DatosListado, kwargs = {"tipo": tipo}))
            self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse(DatosListado, kwargs = {"tipo": "NoEsUnTipo"}))
        self.assertEqual(response.status_code, 200)
        self.client.logout()
    
    def testBuscarPersona(self):
        self.assertTrue(self.client.login(username = "admin", password = "testpassword"))
        response = self.client.get(reverse(BuscarPersona))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse(BuscarPersona), data = {"consulta": "maria"})
        self.assertEqual(response.status_code, 200)
        self.client.logout()
    
    def testAgregar(self):
        response = self.client.get(reverse(Agregar))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(self.client.login(username = "admin", password = "testpassword"))
        response = self.client.get(reverse(Agregar))
        self.assertEqual(response.status_code, 200)
        self.client.logout()
    
    def testAdultoFormulario(self):
        self.assertTrue(self.client.login(username = "admin", password = "testpassword"))
        response = self.client.get(reverse(AdultoFormulario))
        self.assertEqual(response.status_code, 200)
        
        self.client.logout()