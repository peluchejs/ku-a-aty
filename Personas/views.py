# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.views.generic import ListView, DetailView, UpdateView
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext

from django.db.models import Q
from django.utils import simplejson
from django.forms.models import inlineformset_factory

from django.contrib.formtools.wizard.views import SessionWizardView

from models import DatosDelHijo, DatosDePacienteAdulto, DatosInfantiles, DatosDePacienteAdolescente, DatosPersonalesBasicos
from models import Pais, Departamento, Ciudad, Barrio, TipoVivienda
from models import DatosFamiliaresAdolescentes

from forms import FormularioDatosAdultos, FormularioDatosAdolescentes, FormularioDireccion
from forms import FormularioEscolaridadAdolescente, FormularioLaboralAdolescente, FormularioViviendaAdolescente, FormularioIngresoAdolescente, FormularioHabitosAdolescentes, FormularioNoviazgoAdolescente, FormularioLaboralAdulto

from forms import FormularioDatosAdultos, FormularioDatosAdolescentes, FormularioDireccion, FormularioLaboralAdulto
from forms import FormularioEscolaridadAdolescente, FormularioLaboralAdolescente, FormularioViviendaAdolescente, FormularioIngresoAdolescente, FormularioHabitosAdolescentes, FormularioNoviazgoAdolescente

from forms import FormularioInfantilCombined, FormsetMiembrosFamilia, FormularioFamiliaInfanteCombined
from forms import FormularioEncargadaCombined, FormularioInfanteEscolaridadCombined

from forms import horizontal_as_table
from util.autoview import AutoVista, StringField, OverloadedAVField, ExtraDataField
from util.clases import DictionaryAttr
#############################################
# import os

# from django.shortcuts import render
# #from django.contrib.auth.decorators import login_required
# #from django.http import HttpResponse
# #from django.shortcuts import render
# from forms import CrearInformeFormulario

# from django.db.models import Count

# from django.views.generic import ListView, DetailView, UpdateView

# from Especialista.models import FichaBase
# from Usuarios.models import KunaAtyUsuario, Mensaje
# from KunaAty.settings import relativoALaRaiz

# import reportlab.platypus
# from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, Image
# from reportlab.lib.styles import getSampleStyleSheet
# from reportlab.lib import colors
# from reportlab.lib.colors import HexColor
# from reportlab.rl_config import defaultPageSize
# from reportlab.lib.units import inch

# PAGE_HEIGHT=defaultPageSize[1]
# PAGE_WIDTH=defaultPageSize[0]

# def datos_entre_fechas(fecha_inicio, fecha_fin):
	# fichas = FichaBase.objects.filter(FechaActualizada__gte = fecha_inicio, FechaActualizada__lte = fecha_fin)
	# return fichas

# def sociales_adultos(fichas):
	# return fichas.filter(fichasocial__isnull = False)

# def sociales_adolescentes(fichas):
	# return fichas.filter(fichasocialadolescente__isnull = False)

# def sociales_infantes(fichas):
	# return fichas.filter(fichainfantil__isnull = False)



# def dibujarPdf(respuesta, av):


	# def primeraPagina(canvas, doc):
		# canvas.saveState()
		# canvas.drawImage(relativoALaRaiz("..\\static\\img\\image.jpg"),120, 765, 289, 59)
		# canvas.setFont('Times-Bold',14)
		# canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-127, "Informe Estadístico")
		# canvas.setFont('Times-Bold',12)
		# canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-140, "Desde %s - Hasta %s")
		# canvas.setFont('Times-Roman',9)
		# canvas.drawString(inch, 0.75 * inch,"Página 1")
		# canvas.restoreState()
	
	# def siguientesPaginas(canvas, doc):
		# canvas.saveState()
		# canvas.drawImage(relativoALaRaiz("..\\static\\img\\image.jpg"),120, 765, 289, 59)
		# canvas.setFont('Times-Roman', 9)
		# canvas.drawString(inch, 0.75 * inch,"Página %d" % (doc.page,))
		# canvas.restoreState()
	
	# def tabla(cabecera, filas, destacarFilas = []):
		# filas.insert(0, [cabecera, ""])
		# t = Table(filas, style=[('GRID',(0,0),(-1,-1),0.5,colors.grey),
								# ('SPAN', (0,0),(1,0))
								# ], colWidths=200, rowHeights=20)
		# return t
		
	# doc = SimpleDocTemplate(respuesta)
	# contenido = [Spacer(1,1*inch)]
	# contenido.append(tabla("Resumen de atenciones realizadas", [[ av]]))#,
	# doc.build(contenido, onFirstPage = primeraPagina, onLaterPages = siguientesPaginas)

	
 
#######################################################################
def buscar_datos(tipo):
	qs = {"Pais": Pais.objects.all(),
		  "Departamento": Departamento.objects.all(),
		  "Ciudad": Ciudad.objects.all(),
		  "Barrio": Barrio.objects.all(),
		  "TipoVivienda": TipoVivienda.objects.all()}.get(tipo, [])
	return map(lambda x:x.nombre, qs)

@login_required
def DatosListado(solicitud, tipo):
	data = simplejson.dumps(buscar_datos(tipo))
	return HttpResponse(data, mimetype="text/json")

class BuscarPersonaResultados(ListView):
	extra = {}
	
	def get_context_data(self, **kwargs):
		context = super(self.__class__, self).get_context_data(**kwargs)
		for key, value in self.extra.items():
			if callable(value):
				context[key] = value()
			else:
				context[key] = value
		return context

def combinar(*args):
	for x in args:
		for y in x:
			yield y



@login_required
def BuscarPersona(solicitud):
	if 'nombre' and 'apellido' and 'cedula' not in solicitud.GET:
		return ListView.as_view(queryset = DatosPersonalesBasicos.objects.none(), template_name = "paciente_buscar.html")(solicitud)
	orden = solicitud.GET.get('orden', 'tipo')
	orden = orden if orden in ['Nombre', 'Apellido'] else 'tipo'
	nombre = solicitud.GET["nombre"]
	apellido = solicitud.GET["apellido"]
	cedula = solicitud.GET["cedula"]
	consulta = Q(Nombre__icontains = nombre)& Q(Apellido__icontains = apellido)& Q(Cedula__icontains = cedula)
	qs =  DatosPersonalesBasicos.objects.filter(datosagresor = None).filter(consulta)
	if orden == 'tipo':
		qs = list(combinar(qs.exclude(datosinfantiles = None), qs.exclude(datosdepacienteadolescente = None), qs.exclude(datosdepacienteadulto = None)))
	else:
		qs = qs.order_by(orden)
	return BuscarPersonaResultados.as_view(extra={"nombre": solicitud.GET["nombre"],"apellido": solicitud.GET["apellido"],"cedula": solicitud.GET["cedula"], 'orden': orden}, queryset = qs, template_name = "paciente_buscar.html", paginate_by = 30)(solicitud)

@login_required
def Agregar(solicitud):
	return render(solicitud, "elegir_tipo.html")

@login_required
def AdultoFormulario(solicitud, pk = None):
	pacienteInstancia = None
	direccionInstancia = None
	laboralInstancia= None
	if pk is not None:
		pacienteInstancia = get_object_or_404(DatosDePacienteAdulto, pk=int(pk))
		direccionInstancia = pacienteInstancia.Direccion
		laboralInstancia = pacienteInstancia.Laborales
	paciente_formulario = FormularioDatosAdultos(solicitud.POST or None, instance = pacienteInstancia, prefix = "paciente")
	direccion_formulario = FormularioDireccion(solicitud.POST or None, instance = direccionInstancia, prefix = "direccion")
	laboral_formulario=FormularioLaboralAdulto(solicitud.POST or None, instance = laboralInstancia, prefix = "datosLaborales")
	hijos_formset = inlineformset_factory(DatosDePacienteAdulto, DatosDelHijo, extra = 1, fk_name="Madre")
	hijos_formset.as_table = horizontal_as_table
	hijos_formset.titulo = "Hijos"
	hijos_formset.is_formset = True
	if paciente_formulario.is_valid() and direccion_formulario.is_valid() and laboral_formulario.is_valid():
		paciente = paciente_formulario.save(commit = False)
		direccion = direccion_formulario.save()
		paciente.Direccion = direccion
		datosLaborales=laboral_formulario.save()
		paciente.Laborales=datosLaborales
		paciente.save()
		paciente_formulario.save_m2m()
		hijos_formset = hijos_formset(solicitud.POST, instance = paciente, prefix = "hijos")
		if hijos_formset.is_valid():
			hijos_formset.save()
			return HttpResponseRedirect(paciente_formulario.instance.get_absolute_url())
	else:
		hijos_formset = hijos_formset(prefix = "hijos", instance = pacienteInstancia)
	return render(solicitud, "agregar_adulto.html", {"form": paciente_formulario, "extras": [direccion_formulario, laboral_formulario, hijos_formset]})

class DireccionVista(AutoVista):
	class Meta:
		fields = {
			"Ciudad": StringField,
			"Direccion": StringField,
			"Barrio": StringField,
			"Departamento": StringField
		  }

class EsolaridadVista(AutoVista):
	class Meta:
		fields = {
			"Ciudad": StringField
		}

class EncargadaVista(AutoVista):
	class Meta:
		fields = {
			"RelacionConNino": StringField
		}

class PersonaAutoVista(AutoVista):
	class Meta:
		exclude = ("datospersonalesbasicos_ptr", "FechaCreada",)
		fields = {
			"Direccion": OverloadedAVField(DireccionVista),
			"Escolaridad": OverloadedAVField(EsolaridadVista),
			"Encargada": OverloadedAVField(EncargadaVista),
			"Nacionalidad": StringField,
			"LugarDeNacimiento": StringField,
			"TipoDeVivienda": StringField,
			"FechaNacimiento": ExtraDataField(Edad = lambda instance: instance.Edad)
		}

import ho.pisa as pisa
import cStringIO as StringIO
import cgi
from django.template import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse
def generar_pdf(html):	
	# Función para generar el archivo PDF y devolverlo mediante HttpResponse
	result = StringIO.StringIO()
	pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
	if not pdf.err:
		return HttpResponse(result.getvalue(), mimetype='application/pdf')
	return HttpResponse('Error al generar el PDF: %s' % cgi.escape(html))
	
@login_required
def imprimir_adulto(request, paciente): #En realidad este va a imprimir los Datos Básicos, ya sea Adulto, Adolescente o Infante
	pk=int(paciente)
	if DatosDePacienteAdulto.objects.filter(pk = pk).exists():
		av=PersonaAutoVista(DatosDePacienteAdulto.objects.get(pk = pk))
	if DatosDePacienteAdolescente.objects.filter(pk = pk).exists():
		av=PersonaAutoVista(DatosDePacienteAdolescente.objects.get(pk = pk))
	if DatosInfantiles.objects.filter(pk = pk).exists():
		av=PersonaAutoVista(DatosInfantiles.objects.get(pk = pk))
	html = render_to_string('imprimir_DatosBasicos.html', {'pagesize':'A4', 'autovista': av}, context_instance=RequestContext(request)) ###ver request por solicitud
	return generar_pdf(html)
	
@login_required
def ver_adulto(solicitud, pk):
	av = PersonaAutoVista(DatosDePacienteAdulto.objects.get(pk = pk))	 
	paciente = get_object_or_404(DatosDePacienteAdulto, pk = pk)
	fichaSociales_list = []
	fichaSociales_list = paciente.fichasSociales.order_by("-FechaActualizada")
	fichaGinecologicas_list = []
	fichaGinecologicas_list = paciente.fichasMedicas.order_by("-FechaActualizada")
	fichaJuridicas_list = []
	fichaJuridicas_list = paciente.fichasJuridicas.order_by("-FechaActualizada")
	fichaPsicologicas_list = []
	fichaPsicologicas_list = paciente.fichasPsicologicas.order_by("-FechaActualizada")
	return render(solicitud, "ver_paciente.html", {"object": av.instance, 'autovista': av, 'fichas': fichaSociales_list,'fichasginecologicas': fichaGinecologicas_list,
	'fichasjurid': fichaJuridicas_list,'fichasPsico': fichaPsicologicas_list})

@login_required
def ver_adolescente(solicitud, pk):
	av = PersonaAutoVista(DatosDePacienteAdolescente.objects.get(pk = pk))
	paciente = get_object_or_404(DatosDePacienteAdolescente, pk = pk)
	fichaSociales_list = []
	fichaSociales_list = paciente.fichasSociales.order_by("-FechaActualizada")
	fichaPsicologicas_list = []
	fichaPsicologicas_list = paciente.fichasPsicologicas.order_by("-FechaActualizada")
	return render(solicitud, "ver_paciente.html", {"object": av.instance, "autovista": av, 'fichas': fichaSociales_list,'fichasPsico': fichaPsicologicas_list})

@login_required
def ver_infante(solicitud, pk):
	av = PersonaAutoVista(DatosInfantiles.objects.get(pk = pk))
	paciente = get_object_or_404(DatosInfantiles, pk = pk)
	fichasPsicosociales_list = paciente.fichabase_set.all().order_by("-FechaActualizada")
	return render(solicitud, "ver_paciente.html", {"object": av.instance, "autovista": av,'fichasPsicosociales': fichasPsicosociales_list})

class AdolescenteWizard(SessionWizardView):
	template_name = "agregar_adolescente.html"
	
	def done(self, form_list, **kwargs):
		paciente = form_list[0].save()
		paciente.Direccion = form_list[1].save()
		for f in form_list[2:]:
			o = f.save(commit = False)
			if type(o) == list:
				for ele in o:
					ele.Adolescente = paciente
					ele.save()
			else:
				o.save()
				o.datosdepacienteadolescente = paciente
				o.save()
				f.save_m2m()
		paciente.save()
		return HttpResponseRedirect(paciente.get_absolute_url())

FORMULARIOS = [("basico", FormularioDatosAdolescentes),
			   ("direccion", FormularioDireccion),
			   ("familia", FormsetMiembrosFamilia),
			   ("escolaridad", FormularioEscolaridadAdolescente),
			   ("laboral", FormularioLaboralAdolescente),
			   ("|", FormularioViviendaAdolescente),
			   ("ingreso", FormularioIngresoAdolescente),
			   ("habitos", FormularioHabitosAdolescentes),
			   ("noviazgo", FormularioNoviazgoAdolescente)]

def adolescente_trabaja(wizard):
	cleaned_data = wizard.get_cleaned_data_for_step('basico') or {'Trabaja': False}
	return cleaned_data['Trabaja']

VistaFormulario = login_required(AdolescenteWizard.as_view(FORMULARIOS, condition_dict={'laboral': adolescente_trabaja}, instance_dict = {"familia": DatosFamiliaresAdolescentes.objects.none()}))

@login_required
def ModificarAdolescente(solicitud, pk):
	paciente = get_object_or_404(DatosDePacienteAdolescente, pk = int(pk))
	inicial = {u'basico': paciente,
			   u'direccion': paciente.Direccion,
			   u'familia': paciente.datosfamiliaresadolescentes_set.all(),
			   u'escolaridad': paciente.Escolaridad,
			   u'laboral': paciente.Laborales,
			   u'vivienda': paciente.Vivienda,
			   u'ingreso': paciente.IngresoFamiliar,
			   u'habitos': paciente.Habitos,
			   u'noviazgo': paciente.NoviazgoAdolescente}
	wizard = AdolescenteWizard.as_view(FORMULARIOS, condition_dict={'laboral': adolescente_trabaja}, instance_dict = inicial)
	return wizard(solicitud)

@login_required
def ModificarAdulto(solicitud, pk):
	paciente = get_object_or_404(DatosDePacienteAdulto, pk = pk)
	modificar_datos=FormularioDatosAdultos(solicitud.POST or None, instance = paciente)
	if modificar_datos.is_valid():
		modificar_datos.save()
		return HttpResponseRedirect(modificar_datos.instance.get_absolute_url())
	return render(solicitud, "modificar_adulto.html",{"form": modificar_datos})


FORMULARIOS_INFANTILES = [("basico", FormularioInfantilCombined),
						  ("encargada", FormularioEncargadaCombined),
						  ("escolaridad", FormularioInfanteEscolaridadCombined),
						  ('familia', FormularioFamiliaInfanteCombined)]

TEMPLATES_INFANTILES = {}

def infante_escuela(wizard):
	cleaned_data = wizard.get_cleaned_data_for_step('basico') or {'basicos': {'AsisteEscuela': False}}
	return cleaned_data['basicos'] ['AsisteEscuela']

class InfanteWizard(SessionWizardView):
	template_name = "agregar_infante.html"
	instance_override = {}
	
	def __init__(self, *args, **kwargs):
		self.instance_override = kwargs.pop('instance_override', {})	
		super(InfanteWizard, self).__init__(*args, **kwargs)
	
	def get_template_names(self):
		return [TEMPLATES_INFANTILES.get(self.steps.current, InfanteWizard.template_name)]
	
	def get_form_kwargs(self, step):
		kwargs = super(InfanteWizard, self).get_form_kwargs(step)
		if step in self.instance_override:
			kwargs.update({'instance': self.instance_override[step]})
		return kwargs
	
	def done(self, form_list, **kwargs):
		paciente = form_list[0].save()['basicos']
		encargada = form_list[1].save()['encargada']
		asiste_escuela = infante_escuela(self)
		if asiste_escuela:
			escolaridad = form_list[2].save()['escolaridad']
			paciente.Escolaridad = escolaridad
		familia = form_list[3 if asiste_escuela else 2].save()['familia']
		paciente.Encargada = encargada
		paciente.Familiares = familia
		paciente.save()
		return HttpResponseRedirect(paciente.get_absolute_url())

VistaFormularioInfante = login_required(InfanteWizard.as_view(FORMULARIOS_INFANTILES, condition_dict={'escolaridad': infante_escuela}))

def attr_predeterminado(obj, attr, pre = None):
	if obj is None:
		return pre
	if hasattr(obj, attr):
		return getattr(obj, attr)
	return pre

@login_required
def ModificarInfante(solicitud, pk):
	infante = get_object_or_404(DatosInfantiles, pk=int(pk))
	inicial = {u'basico': DictionaryAttr(basicos = infante, basicos_dir = infante.Direccion),
			   u'encargada': DictionaryAttr(encargada = attr_predeterminado(infante, 'Encargada'), encargada_dir = attr_predeterminado(infante.Encargada, 'Direccion')),
			   u'escolaridad': DictionaryAttr(escolaridad = attr_predeterminado(infante, 'Escolaridad'), escolaridad_dir = attr_predeterminado(infante.Escolaridad, 'Direccion')),
			   u'familia': DictionaryAttr(madre = attr_predeterminado(infante.Familiares, 'Madre'),
										  padre = attr_predeterminado(infante.Familiares, 'Padre'),
										  familia = attr_predeterminado(infante, 'Familiares'))}
	wizard = InfanteWizard.as_view(FORMULARIOS_INFANTILES, condition_dict = {u'escolaridad': infante_escuela}, instance_override = inicial)
	return wizard(solicitud)
