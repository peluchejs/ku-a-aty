# coding=utf-8
from django.db import models
from django.core.urlresolvers import reverse
from datetime import date

class FichaUrl:
	def __init__(self, url, nombre, tipo):
		self.url = url
		self.nombre = nombre
		self.tipo = tipo

class Pais(models.Model):
	nombre = models.CharField(max_length=40)
	
	def __unicode__(self):
		return self.nombre

class Departamento(models.Model):
	nombre = models.CharField(max_length = 40)
	
	def __unicode__(self):
		return self.nombre

class Ciudad(models.Model):
	nombre = models.CharField(max_length = 45)
	
	def __unicode__(self):
		return self.nombre

class Barrio(models.Model):
	nombre = models.CharField(max_length = 40)
	
	def __unicode__(self):
		return self.nombre

class TipoVivienda(models.Model):
	nombre = models.CharField(max_length = 25)	
	
	def __unicode__(self):
		return self.nombre

class NivAcademico:
	Opciones = ((1, "Ninguna"), (2,"Primaria"), (3, "Secundaria"), (4, "Universitaria"), (5, "Diplomado"), (6, "Maestria")) 
	
class Sexo:
	Opciones = (("F", "Femenino"), ("M", "Masculino"))

class EstadoCivil:
	Opciones = ((0, "Soltera"),
				(1, "Casada"),
				(2, "Concubinada"),
				(3, "Separada"),
				(4, "Viuda"),
				(5, "Divorciada"))

class Direccion(models.Model):
	Direccion = models.CharField(max_length = 40, verbose_name = "Dirección", null=True,blank=True)
	Barrio = models.ForeignKey(Barrio, related_name = "+", null = True,blank=True)
	Ciudad = models.ForeignKey(Ciudad, related_name="+", null = True,blank=True)
	Departamento = models.ForeignKey(Departamento, related_name="+", null = True,blank=True)

class DatosPersonalesBasicos(models.Model):
	Nombre = models.CharField(max_length=40)
	Apellido = models.CharField(max_length=50)
	Cedula = models.CharField(max_length=15, verbose_name = "Cédula", null = True, blank = True)
	Sexo = models.CharField(max_length = 1, choices = Sexo.Opciones)
	Nacionalidad = models.ForeignKey(Pais, related_name="+", null = True, blank = True)
	FechaNacimiento = models.DateField(verbose_name = "Fecha de Nacimiento", null = True, blank = True)
	LugarDeNacimiento = models.ForeignKey(Ciudad, related_name="+", verbose_name = "Lugar de Nacimiento", null = True, blank = True)
	Direccion = models.ForeignKey(Direccion, verbose_name = "Dirección", null = True, blank = True)
	Telefono = models.CharField(max_length = 20, verbose_name = "Teléfono", null = True, blank = True)
	Correo=models.EmailField(null = True, blank = True)
	FechaCreada = models.DateField(default = date.today)
	
	@property
	def tipo_ficha(self):
		tipo_clase = self.buscar_tipo
		return {DatosDePacienteAdulto: "Adulto",
				DatosDePacienteAdolescente: "Adolescente",
				DatosInfantiles: "Infante"}.get(type(tipo_clase), "Nada")
	
	@property
	def buscar_tipo(self):
		if not hasattr(self, "_tipo_guardado"):
			self._tipo_guardado = self._buscar_tipo_interior()
		return self._tipo_guardado

	def _buscar_tipo_interior(self):
		if type(self) != DatosPersonalesBasicos and hasattr(self, 'datospersonalesbasicos_ptr'):
			self = self.datospersonalesbasicos_ptr
		try:
			if self.datosagresor is not None:
				return self.datosagresor
		except DatosAgresor.DoesNotExist:
			pass
		try:
			if self.datosdepacienteadulto is not None:
				return self.datosdepacienteadulto
		except DatosDePacienteAdulto.DoesNotExist:
			pass
		try:
			if self.datosdepacienteadolescente is not None:
				return self.datosdepacienteadolescente
		except DatosDePacienteAdolescente.DoesNotExist:
			pass
		try:
			if self.datosinfantiles is not None:
				return self.datosinfantiles
		except DatosInfantiles.DoesNotExist:
			pass
		return None
	
	@property
	def Edad(self):
		if self.FechaNacimiento is None:
			return "Desconocida"
		today = date.today()
		born = self.FechaNacimiento
		try: 
			birthday = born.replace(year=today.year)
		except ValueError: # raised when birth date is February 29 and the current year is not a leap year
			birthday = born.replace(year=today.year, day=born.day-1)
		if birthday > today:
			return today.year - born.year - 1
		else:
			return today.year - born.year
	
	def get_absolute_url(self):
		if self.buscar_tipo:
			return self.buscar_tipo.get_absolute_url()
		else:
			return "/"
	
	def get_modificar_url(self):
		if self.buscar_tipo:
			return self.buscar_tipo.get_modificar_url()
		else:
			return "/"

	def get_ficha_urls(self):
		if self.buscar_tipo:
			return self.buscar_tipo.get_ficha_urls()
		else:
			return []
	
		  
	def __unicode__(self):
		return "%(nombre)s %(apellido)s" % {"nombre": self.Nombre, "apellido": self.Apellido}


class DatosLaborales(models.Model):
	LugarDeTrabajo=models.CharField(max_length=40, null = True, blank = True, verbose_name = "Lugar de Trabajo")
	Cargo=models.CharField(max_length=40, null = True, blank = True )
	Ingreso=models.BigIntegerField(null = True, blank = True)

class DatosLaboralesAdolescentes(DatosLaborales):
	EdadCuandoEmpezo = models.PositiveSmallIntegerField(null = True, blank = True, verbose_name = "¿Cuántos años tenías cuando empezaste?")
	PorqueTrabaja = models.TextField(null = True, blank = True, verbose_name = "¿Por qué trabajas?")
	AQuienBeneficia = models.TextField(null = True, blank = True, verbose_name = "¿A quién beneficia tu trabajo?")

class ConstruccionOpciones:
	OPCIONES = ((0, "Material"), (1, "Madera"), (2, "Otro"))

class CasaUtilidades(models.Model):
	nombre = models.CharField(max_length = 25)
	
	def __unicode__(self):
		return self.nombre

class EscolarHabilidad:
	Opciones = ((0, "Excelente"), (1, "Muy buena/o"), (2, "Buena/o"), (3, "Regular"), (4, "Mala/o"))
	Predeterminado = 0

class EscolaridadAdolescente(models.Model):
	InstitucionEscolar = models.CharField(max_length = 35, verbose_name = "Institución Escolar", null = True, blank = True)
	Ciudad = models.ForeignKey(Ciudad, related_name = "+", null = True, blank = True)
	Transporte = models.BooleanField(verbose_name = "¿Utilizas transporte para llegar a la institución?")
	NivelEscolar = models.CharField(max_length = 15, verbose_name = "Nivel Escolar", null = True, blank = True)
	HabilidadEscolar = models.IntegerField(choices = EscolarHabilidad.Opciones, default =  EscolarHabilidad.Predeterminado, verbose_name = "¿Cómo te calificás como alumna/o?", null = True, blank = True)
	
class DatosDeViviendaAdolescente(models.Model):
	TipoVivienda = models.ForeignKey(TipoVivienda, related_name = "+", verbose_name = "Tipo de Vivienda", null = True, blank = True)
	Construccion = models.IntegerField(choices = ConstruccionOpciones.OPCIONES, verbose_name = "Construcción", null = True, blank = True)
	Utilidades = models.ManyToManyField(CasaUtilidades, related_name = "+", null = True, blank = True)
	CantidadHabitantes = models.PositiveSmallIntegerField(verbose_name = "Número de Habitantes", null = True, blank = True)
	CantidadHabitaciones = models.PositiveSmallIntegerField(verbose_name = "Número de Habitaciones", null = True, blank = True)

class DatosDeIngresoFamiliar(models.Model):
	Observacion = models.TextField(null = True, blank = True, verbose_name = "Observación")

class ContribuidorAlIngresoFamiliar(models.Model):
	Parentezco = models.CharField(max_length = 40, null = True, blank = True)
	LugarDeTrabajo = models.CharField(max_length = 50, null = True, blank = True)
	Ingreso = models.BigIntegerField(null = True, blank = True)
	Horario = models.CharField(max_length = 60, null = True, blank = True)
	IngresoFamiliarDatos = models.ForeignKey(DatosDeIngresoFamiliar)

class HabitosAdolescentes(models.Model):
	Tabaco = models.BooleanField(verbose_name = "Uso de tabaco")
	CantidadYFrecuenciaTabaco = models.TextField(null = True, blank = True, verbose_name = "Cantidad y frecuencia del uso de tabaco")
	AlguienEnLaCasaConsumeTabaco = models.BooleanField(verbose_name = "¿Alguién en la casa consume tabaco?")
	Alcohol = models.BooleanField(verbose_name = "Uso de alcohol")
	CantidadYFrecuenciaAlcohol = models.TextField(null = True, blank = True, verbose_name = "Cantidad y frecuencia del uso de alcohol")
	AlguienEnLaCasaConsumeAlcohol = models.BooleanField(verbose_name = "¿Alguién en la casa consume alcohol?")
	QuienConsumeAlcohol = models.TextField(null = True, blank = True, verbose_name = "¿Quién consume alcohol?")
	CantidadYFrecuenciaCohabitantesAlcohol = models.TextField(null = True, blank = True, verbose_name = "Cantidad y frecuencia del uso de alcohol")
	UsoDeDrogasAlgunaVez = models.BooleanField(verbose_name = "¿Consumiste drogas alguna vez?")
	UsoDeDrogasActual = models.BooleanField(verbose_name = "¿Consumes drogas actualmente?")

class NoviazgoAdolescente(models.Model):
	TieneNovios = models.BooleanField(verbose_name = "¿Tienes novia/os?")
	CuantosTiene = models.PositiveSmallIntegerField(null = True, blank = True, verbose_name = "¿Cuanta/os tienes?")
	TuvoNovios = models.BooleanField(verbose_name = "¿Tuviste novia/os?")
	CuantosTenia = models.PositiveSmallIntegerField(null = True, blank = True, verbose_name = "¿Cuanta/os tenías?")

class DatosAdultos(DatosPersonalesBasicos):
	NivelAcademico = models.IntegerField(choices = NivAcademico.Opciones, verbose_name = "Nivel Académico", null = True, blank = True)
	Laborales = models.OneToOneField(DatosLaborales, null = True, blank = True)
	
	class Meta:
		abstract = True

class ExlusionHogar:
	Opciones = (("S", "Si"), ("N", "No"), ("A", "Anteriormente"))

class DatosAgresor(DatosAdultos):
	TieneExclusiondelHogar= models.CharField( max_length = 1, null = True, blank = True, verbose_name = "Tiene Exclusión del Hogar", choices = ExlusionHogar.Opciones)
	
class DatosDePacienteAdulto(DatosAdultos):
	EstadoCivil=models.IntegerField(choices = EstadoCivil.Opciones, verbose_name = "Estado Civil", null = True, blank = True)
	TipoDeVivienda = models.ForeignKey(TipoVivienda ,related_name="+", verbose_name = "Tipo de Vivienda", null = True, blank = True)
	
	def get_absolute_url(self):
		return reverse("ver_adulto", kwargs = {"pk": self.pk})
	
	def get_modificar_url(self):
		return reverse("modificar_adulto", kwargs={"pk": self.pk})
	
	def get_ficha_urls(self):
		return [FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'social'}), "Fichas Sociales","social"),
				FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'medica'}), "Fichas Ginecológicas","medica"),
				FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'juridica'}), "Fichas Jurídicas", "juridica"),
				FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'psicologica'}), "Fichas Psicológicas","psicologica")]
	
	@property
	def fichasSociales(self):
		return self.fichabase_set.exclude(fichasocial = None)
		
	@property
	def fichasMedicas(self):
		return self.fichabase_set.exclude(fichamedica = None)
		
	@property
	def fichasJuridicas(self):
		return self.fichabase_set.exclude(fichajuridica = None)
		
	@property
	def fichasPsicologicas(self):
		return self.fichabase_set.exclude(fichapsicologica = None)

class RelacionPadre:
	Opciones = (("S", "Si"), ("N", "No"), ("F", "Con Frecuencia"))
	

	EstadoCivil=models.IntegerField(choices = EstadoCivil.Opciones, verbose_name = "Estado Civil",null=True,blank=True)
	TipoDeVivienda = models.ForeignKey(TipoVivienda, related_name="+", verbose_name = "Tipo de Vivienda",null=True,blank=True)
	
	
	def get_absolute_url(self):
		return reverse("ver_adulto", kwargs = {"pk": self.pk})
	
	def get_ficha_urls(self):
		return [FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'social'}), "Fichas Sociales","social"),
				FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'medica'}), "Fichas Ginecológicas","medica"),
				FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'juridica'}), "Fichas Jurídicas", "juridica"),
				FichaUrl(reverse("ficha_adulto_listado", kwargs = {'pk': self.pk, 'tipo': 'psicologica'}), "Fichas Psicológicas","psicologica")]
	
	@property
	def fichasSociales(self):
		return self.fichabase_set.exclude(fichasocial = None)
		
	@property
	def fichasMedicas(self):
		return self.fichabase_set.exclude(fichamedica = None)
		
	@property
	def fichasJuridicas(self):
		return self.fichabase_set.exclude(fichajuridica = None)
		
	@property
	def fichasPsicologicas(self):
		return self.fichabase_set.exclude(fichapsicologica = None)


	
class DatosDelHijo(models.Model):
	Madre = models.ForeignKey(DatosDePacienteAdulto, null = True, blank = True)
	Nombre = models.CharField(max_length=40)
	Apellido = models.CharField(max_length=50)
	Cedula = models.CharField(max_length=15, verbose_name = "Cédula", null = True, blank = True)
	Sexo = models.CharField(max_length = 1, choices = Sexo.Opciones)
	ViveCon = models.CharField(max_length = 20, verbose_name = "Vive con", null = True, blank = True)
	RelacionPadre = models.CharField( max_length = 1, null = True, blank = True, verbose_name = "Relación con el Padre", choices = RelacionPadre.Opciones)

class DatosDePacienteAdolescente(DatosPersonalesBasicos):
	AQuienPertaneceTelefono = models.CharField(max_length = 25, verbose_name = "¿A quién pertenece el teléfono?", null = True, blank = True)
	TieneHijos = models.BooleanField(verbose_name = "¿Tiene hijos?")
	CantidadDeHijos = models.PositiveSmallIntegerField(default = 0, verbose_name = "Cantidad de hijos", null = True, blank = True)
	
	ConQuienVive = models.CharField(max_length = 20, null = True, blank = True, verbose_name = "Con quién vives?")
	NombreMama = models.CharField(max_length = 30, null = True, blank = True, verbose_name = "Nombre de la mamá")
	NombrePapa = models.CharField(max_length = 30, null = True, blank = True, verbose_name = "Nombre del papá")
	Tutor = models.CharField(max_length = 30, null = True, blank = True, verbose_name = "Tutor/a o Responsable")

	Escolaridad = models.OneToOneField(EscolaridadAdolescente, null = True)
	Laborales = models.OneToOneField(DatosLaboralesAdolescentes, null = True)
	Vivienda = models.OneToOneField(DatosDeViviendaAdolescente, null = True)
	IngresoFamiliar = models.OneToOneField(DatosDeIngresoFamiliar, null = True, verbose_name = "Ingreso Familiar")
	Habitos = models.OneToOneField(HabitosAdolescentes, null = True, verbose_name = "Hábitos")
	NoviazgoAdolescente = models.OneToOneField(NoviazgoAdolescente, null = True, verbose_name = "Noviazgo Adolescente")
	
	def get_absolute_url(self):
		return reverse("ver_adolescente", kwargs = {"pk": self.pk})
	
	def get_modificar_url(self):
		return reverse("modificar_adolescente", kwargs={"pk": self.pk})
	
	def get_ficha_urls(self):
		return [FichaUrl(reverse("ficha_adolescente_listado", kwargs = {'pk': self.pk, 'tipo': 'social'}), "Fichas Sociales", "social"),
				FichaUrl(reverse("ficha_adolescente_listado", kwargs = {'pk': self.pk, 'tipo': 'psicologica'}), "Fichas Psicológicas","psicologica")]
		
	@property
	def fichasSociales(self):
		return self.fichabase_set.exclude(fichasocialadolescente = None)
		
	@property
	def fichasPsicologicas(self):
		return self.fichabase_set.exclude(fichapsicologicaadolescente = None)

class DatosFamiliaresAdolescentes(models.Model):
	Adolescente = models.ForeignKey(DatosDePacienteAdolescente)
	Nombre = models.CharField(max_length=40)
	Apellido = models.CharField(max_length=50)
	Edad = models.PositiveSmallIntegerField(null = True, blank = True)
	NivelEscolar = models.CharField(max_length = 40, verbose_name = "Nivel Escolar", null = True, blank = True)
	Parentezco = models.CharField(max_length = 40, null = True, blank = True)

class RelacionConNino(models.Model):
	nombre = models.CharField(max_length = 20)
	
	def __unicode__(self):
		return self.nombre

class IdiomaOpciones:
	OPCIONES = ((0, "Castellano"),
				(1, "Guaraní"),
				(2, "Ambos"),
				(3, "Otro"))

class InstruccionOpciones:
	OPCIONES = (
				(0, "Primaria"),
				(1, "Secundaria"),
				(2, "Terciaria"),
				(3, "Universitaria")
				)

class TipoHijoOpciones:
	OPCIONES = (
				(0, "Adoptado/a"),
				(1, "Biológico/a")
				)
class InfanteFamiliar(models.Model):
	Nombre = models.CharField(max_length = 25,null = True, blank = True)
	Apellido = models.CharField(max_length = 35,null = True, blank = True)
	Edad = models.PositiveIntegerField(null = True, blank = True)
	Ocupacion = models.CharField(max_length = 25, verbose_name = "Ocupación", null = True, blank = True)
	Telefono = models.CharField(max_length = 15, verbose_name = "Teléfono", null = True, blank = True)

class InfanteEscolarHabilidad:
	OPCIONES = ((0, "Malo"), (1, "Regular"), (2, "Bueno"))

class InfanteEscolaridad(models.Model):
	Institucion = models.CharField(max_length = 25, verbose_name = "Institución", null = True, blank = True)
	Grado = models.CharField(max_length = 20, null = True, blank = True)
	Direccion = models.OneToOneField(Direccion, null = True, verbose_name="Dirección")
	Maestro = models.CharField(max_length = 35, verbose_name = "Nombre de la/el maestra/o", null = True, blank = True)
	Desempeno = models.IntegerField(choices = InfanteEscolarHabilidad.OPCIONES, default = 0, verbose_name = "Desempeño escolar", null = True, blank = True)
	Repitencia = models.BooleanField()
	Disercion = models.BooleanField(verbose_name = "Diserción")
	Observacion = models.TextField(verbose_name = "Observación", null = True, blank = True)

class EncargadaDeNino(models.Model):
	Nombre = models.CharField(max_length=40)
	Apellido = models.CharField(max_length=50)
	Direccion = models.OneToOneField(Direccion, null = True, verbose_name="Dirección")
	Telefono = models.CharField(max_length = 20, verbose_name="Teléfono", null = True, blank = True)
	RelacionConNino = models.ForeignKey(RelacionConNino, related_name = "+", verbose_name="Relación con Niño", null = True, blank = True)
	NivelDeInstruccionAcademica = models.IntegerField(choices = InstruccionOpciones.OPCIONES, verbose_name="Nivel Académico", null = True, blank = True)
	EstadoCivil=models.IntegerField(choices = EstadoCivil.Opciones, verbose_name="Estado Civil", null = True, blank = True)
	EsUsuariaDeLaFundacion = models.BooleanField(verbose_name="Es Usuaria de la Fundación")

class DatosFamiliaresInfantiles(models.Model):
	Madre = models.ForeignKey(InfanteFamiliar, related_name = "+", null = True)
	Padre = models.ForeignKey(InfanteFamiliar, related_name = "+", null = True,	 blank = True)
	CantidadMatrimonios = models.IntegerField(verbose_name = "Número de matrimonios o parejas estables", null = True, blank = True)
	CantidadHijos = models.IntegerField(verbose_name = "Cantidad de Hijos", null = True, blank = True)
	Edades = models.CommaSeparatedIntegerField(max_length = 40, null = True, blank = True)
	Observacion = models.TextField(verbose_name = "Observación", null = True, blank = True)

class DatosInfantiles(DatosPersonalesBasicos):
	Sobrenombre = models.CharField(null = True, blank = True, max_length = 15)
	CedulaMama = models.CharField(max_length=15, verbose_name = "Cédula de Mamá", null = True, blank = True)
	Encargada = models.OneToOneField(EncargadaDeNino, null = True)
	ConQuienVive = models.CharField(max_length = 60, verbose_name = "¿Con quién vive?", null = True, blank = True)
	IdiomaEnCasa = models.IntegerField(choices = IdiomaOpciones.OPCIONES, default = 0, verbose_name = "Idioma hablada en la casa", null = True, blank = True)
	TipoRelacion = models.IntegerField(choices = TipoHijoOpciones.OPCIONES, verbose_name = "Hija/o es...", null = True, blank = True)
	Familiares = models.OneToOneField(DatosFamiliaresInfantiles, null = True)
	Escolaridad = models.OneToOneField(InfanteEscolaridad, null = True)
	
	def get_ficha_urls(self):
		return [FichaUrl(reverse("ficha_infante_listado", kwargs = {'pk': self.pk}), "Fichas Psicosociales", "psicosocial")]
	
	def get_absolute_url(self):
		return reverse("ver_infante", kwargs = {"pk": self.pk})
	
	def get_modificar_url(self):
		return reverse("modificar_infante", kwargs={"pk": self.pk})
