# coding=utf-8
from django.db import models
from django.core.urlresolvers import reverse
from Personas.models import DatosAdultos, DatosPersonalesBasicos, Direccion, DatosAgresor, DatosLaborales, DatosLaboralesAdolescentes 
from Usuarios.models import KunaAtyUsuario
import datetime

class RelacionConAgresor:
    Opciones = ((0, "Esposo"),
                (1, "Concubino"),
                (2, "Novio"),
                (3, "Ex esposo"),
                (4, "Ex concubino"),
                (5, "Ex novio"),
                (6, "Padre"),
                (7, "Hijo/a"),
                (8, "Otro"))

class Oinstitucion:
    Opciones = ((1, "Juzgado"), (2, "Comisaría"), (3, "Hospital"), (4, "Centro de Salud"))
    
class Llegada:
    Opciones = ((0, "Radio"),
                (1, "Televisión"),
                (2, "Internet"),
                (3, "Periódico"),
                (4, "Otra usuaria"),
                (5, "Otra institución"), ## como agregar una subopción de otra institución?
                (6, "Amigos/as"),
                (7, "Familiares"))


################################### Otras clases ############################################
class MotivoDeConsulta(models.Model):
    nombre = models.CharField(max_length = 35)
    
    def __unicode__(self):
        return self.nombre

class TiposDeViolencia(models.Model):
    nombre = models.CharField(max_length = 25)
    
    def __unicode__(self):
        return self.nombre


class ViolenciaFrecuencia(models.Model):
    nombre = models.CharField(max_length = 25)
    predeterminado = models.BooleanField(default = False)
    
    def __unicode__(self):
        return self.nombre

################################### Ficha Base ##############################################
class FichaBase(models.Model):
    Paciente = models.ForeignKey(DatosPersonalesBasicos, null = True, blank = True)
    FechaActualizada = models.DateField(default = datetime.date.today, verbose_name = "Fecha Actualizada")
    ProfesionalTratante=models.ForeignKey(KunaAtyUsuario, verbose_name = "Profesional Tratante")
    Finalizado=models.BooleanField(verbose_name = "Es un caso Finalizado?")
    @property
    def ficha_type(self):
        return {"ad_social": "social",
                "ad_psicologica": "psicologica"}.get(self._ficha_tipo_interno, self._ficha_tipo_interno)
    
    @property
    def _ficha_tipo_interno(self):
        if not hasattr(self, "_tipo_guardado"):
            self._tipo_guardado = self._ficha_type_interior()
        return self._tipo_guardado

    def _ficha_type_interior(self):
        try:
            if self.fichasocial is not None:
                return "social"
        except FichaSocial.DoesNotExist:
            pass
        try:
            if self.fichamedica:
                return "medica"
        except FichaMedica.DoesNotExist:
            pass
        try:
            if self.fichajuridica:
                return "juridica"
        except FichaJuridica.DoesNotExist:
            pass
        try:
            if self.fichapsicologica:
                return "psicologica"
        except FichaPsicologica.DoesNotExist:
            pass
        try:
            if self.fichapsicologicaadolescente:
                return "ad_psicologica"
        except FichaPsicologicaAdolescente.DoesNotExist:
            pass
        try:
            if self.fichasocialadolescente:
                return "ad_social"
        except FichaSocialAdolescente.DoesNotExist:
            pass
        try:
            if self.fichainfantil:
                return "infantil"
        except FichaInfantil.DoesNotExist:
            pass
        return ""
    
    @classmethod
    def get_model(cls, tipo):
        return {"social": FichaSocial,
                "medica": FichaMedica,
                "juridica": FichaJuridica,
                "psicologica": FichaPsicologica,
                "ad_social": FichaSocialAdolescente,
                "ad_psicologica": FichaPsicologicaAdolescente,
                "infantil": FichaInfantil
                }.get(tipo, None)
    
    @property
    def model(self):
        return FichaBase.get_model(self._ficha_tipo_interno)

    @property
    def sub_model(self):
        if self._ficha_tipo_interno == "social":
            return self.fichasocial
        if self._ficha_tipo_interno == "medica":
            return self.fichamedica
        if self._ficha_tipo_interno == "juridica":
            return self.fichajuridica
        if self._ficha_tipo_interno == "psicologica":
            return self.fichapsicologica
        if self._ficha_tipo_interno == "ad_psicologica":
            return self.fichapsicologicaadolescente
        if self._ficha_tipo_interno == "ad_social":
            return self.fichasocialadolescente
        if self._ficha_tipo_interno == "infantil":
            return self.fichainfantil
        return None

    def get_absolute_url(self):
        return self.sub_model.get_absolute_url()
    
    def get_modificar_enlace(self):
        return reverse("modificar_ficha", kwargs = {"pk": self.Paciente.pk, "tipo": self.ficha_type, "ficha_pk": self.pk})

class Observacion(models.Model):
    texto = models.TextField()
    fecha = models.DateField(default = datetime.date.today)
    ficha = models.ForeignKey(FichaBase)
################################### Fichas Infantiles #######################################
class FuenteDeDerivacion(models.Model):
    Institucion = models.CharField(max_length = 30, verbose_name = "Institución")
    Direccion = models.OneToOneField(Direccion, related_name = "+", verbose_name = "Dirección")
    Telefono = models.CharField(max_length = 15, verbose_name = "Teléfono")

class IntervencionAnterior(models.Model):
    Institucion = models.CharField(max_length = 30, verbose_name = "Institución")
    MotivoDeConsulta = models.CharField(max_length = 40, verbose_name = "Motivo de Consulta")
    Resultado = models.CharField(max_length = 50, verbose_name = "Resultado de la Intervención")

class IndicadoresFisicosInfantiles(models.Model):
    nombre = models.CharField(max_length = 30)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class IndicadoresEmocionalesInfantiles(models.Model):
    nombre = models.CharField(max_length = 30)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class IndicadoresCognitivosInfantiles(models.Model):
    nombre = models.CharField(max_length = 30)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class IndicadoresComportamentalesInfantiles(models.Model):
    nombre = models.CharField(max_length = 30)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class FichaInfantil(FichaBase):
    FuenteDerivacion = models.OneToOneField(FuenteDeDerivacion, verbose_name = "Fuente de Derivación", null = True, blank = True)
    Intervenciones = models.OneToOneField(IntervencionAnterior, verbose_name = "Intervenciones", null = True, blank = True)
    MotivosDeConsulta = models.ManyToManyField(MotivoDeConsulta, verbose_name = "Motivos de Consulta", related_name = "fsi+", null = True, blank = True)
    TipoDeViolencia = models.ManyToManyField(TiposDeViolencia, verbose_name = "Tipo de Violencia", related_name = "fsi+", null = True, blank = True)
    IndicadoresFisicos = models.ManyToManyField(IndicadoresFisicosInfantiles, verbose_name = "Indicadores Físicos", related_name = "+", blank = True)
    IndicadoresEmocionales = models.ManyToManyField(IndicadoresEmocionalesInfantiles, verbose_name = "Indicadores Emocionales", related_name = "+", blank = True)
    IndicadoresCognitivos = models.ManyToManyField(IndicadoresCognitivosInfantiles, verbose_name = "Indicadores Cognitivos", related_name = "+", blank = True)
    IndicadoresComportamentales = models.ManyToManyField(IndicadoresComportamentalesInfantiles, verbose_name = "Indicadores Comportamentales", related_name = "+", blank = True)
    HistoriaFamiliar = models.TextField(verbose_name = "Historia Familiar", null  = True, blank = True)
    Seguimientos = models.TextField(null = True, blank = True)
    
    def get_absolute_url(self):
        return reverse('ver_ficha_infante', kwargs = {"pk": self.Paciente.pk, "tipo": "psicosocial", "ficha_pk": self.pk})
    
    def get_modificar_enlace(self):
        return reverse('modificar_ficha_infante', kwargs = {"pk": self.Paciente.pk, "ficha_pk": self.pk})
    
################################### Fichas Adolescentes #####################################
class FichaSocialAdolescente(FichaBase):
    Agresor = models.OneToOneField(DatosAgresor, null = True, blank = True, related_name = "PacienteAdolescente")
    ViveConAgresor=models.BooleanField(verbose_name = "¿Vive con agresor?")
    RelacionConAgresor=models.IntegerField(choices = RelacionConAgresor.Opciones, verbose_name = "Relación con el Agresor")
    MotivosDeConsulta = models.ManyToManyField(MotivoDeConsulta, related_name = "fsa+", verbose_name = "Motivos de Consulta")
    TipoDeViolencia = models.ManyToManyField(TiposDeViolencia, related_name = "fsa+", verbose_name = "Tipo de Violencia")
    FrecuenciaDeViolencia = models.ForeignKey(ViolenciaFrecuencia, related_name = "fsa+", verbose_name = "Frecuencia de Violencia")
    DiagnosticoMedico = models.BooleanField(verbose_name = "¿Realizó diagnóstico médico?")
    DenunciaComisaria= models.BooleanField(verbose_name = "¿Realizó denuncia en la comisaría?")
    DenunciaJuridico= models.BooleanField(verbose_name = "¿Realizó denuncia jurídica?")
    DenunciaPolicia= models.BooleanField(verbose_name = "¿Realizó denuncia con la policía?")
    DenunciaFiscalia= models.BooleanField(verbose_name = "¿Realizó denuncia en la fiscalía?")
    ComoSabiaDeKunaAty = models.IntegerField(choices = Llegada.Opciones, verbose_name = "¿Cómo sabía de Kuña Aty?")

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in FichaSocialAdolescente._meta.fields]
    
    def get_absolute_url(self):
        return reverse('ver_ficha_adolescente', kwargs = {"pk": self.Paciente_id, "tipo": "social", "ficha_pk": self.pk})
    
    def get_modificar_enlace(self):
        return reverse('modificar_ficha_adolescente', kwargs = {"pk": self.Paciente.pk, "tipo": "social", "ficha_pk": self.pk})
        
class IndicadoresViolenciaFisicos(models.Model):
    nombre = models.CharField(max_length = 30)
    
    def __unicode__(self):
        return self.nombre

class IndicadoresViolenciaEmocionales(models.Model):
    nombre = models.CharField(max_length = 30)
    
    def __unicode__(self):
        return self.nombre

class IndicadoresViolenciaComportamentales(models.Model):
    nombre = models.CharField(max_length = 30)
    
    def __unicode__(self):
        return self.nombre

class IndicadoresViolenciaCognitivos(models.Model):
    nombre = models.CharField(max_length = 30)
    
    def __unicode__(self):
        return self.nombre

class FichaPsicologicaAdolescente(FichaBase):
    MotivosDeConsulta = models.ManyToManyField(MotivoDeConsulta, related_name = "fpa+", verbose_name = "Motivos de consulta", null = True, blank = True)
    Fisico = models.ManyToManyField(IndicadoresViolenciaFisicos, related_name = "fpa+", verbose_name = "Indicadores Físicos", null = True, blank = True)
    Emocional = models.ManyToManyField(IndicadoresViolenciaEmocionales, related_name = "fpa+", verbose_name = "Indicadores Emocionales", null = True, blank = True)
    Comportamentales = models.ManyToManyField(IndicadoresViolenciaComportamentales, related_name = "fpa+", verbose_name = "Indicadores Comportamentales", null = True, blank = True)
    Cognitivos = models.ManyToManyField(IndicadoresViolenciaCognitivos, related_name = "fpa+", verbose_name = "Indicadores Cognitivos", null = True, blank = True)
    DescripcionDelCaso = models.TextField(verbose_name = "Descripción del caso", null = True, blank = True)
    
    def get_absolute_url(self):
        return reverse('ver_ficha_adolescente', kwargs = {"pk": self.Paciente_id, "tipo": "psicologica", "ficha_pk": self.pk})
    
    def get_modificar_enlace(self):
        return reverse('modificar_ficha_adolescente', kwargs = {"pk": self.Paciente.pk, "tipo": "psicologica", "ficha_pk": self.pk})

################################### Fichas Adultos ##########################################
class FichaSocial(FichaBase):
    Agresor = models.OneToOneField(DatosAgresor, null = True, blank = True, related_name = "PacienteAdulto")
    ViveConAgresor=models.BooleanField(verbose_name = "¿Vive con el agresor?")
    RelacionConAgresor=models.IntegerField(choices = RelacionConAgresor.Opciones, verbose_name = "Relación con el agresor", null = True, blank = True)
    MotivosDeConsulta = models.ManyToManyField(MotivoDeConsulta, related_name = "fs+", null = True, blank = True, verbose_name = "Motivos de Consulta")
    TipoDeViolencia = models.ManyToManyField(TiposDeViolencia, related_name = "fs+", null = True, blank = True, verbose_name = "Tipos de Violencia")
    FrecuenciaDeViolencia = models.ForeignKey(ViolenciaFrecuencia, related_name = "fs+", null = True, blank = True, verbose_name = "Frecuencia de Violencia")
    DiagnosticoMedico = models.BooleanField(verbose_name = "Realizó diagnóstico médico")
    DenunciaComisaria= models.BooleanField(verbose_name = "Realizó denuncia en la comisaria")
    DenunciaJuridico= models.BooleanField(verbose_name = "Realizó denuncia en Juzgado de Paz")
    DenunciaPolicia= models.BooleanField(verbose_name = "Realizó denuncia al policia")
    DenunciaFiscalia= models.BooleanField(verbose_name = "Realizó denuncia en la fiscalia")
    ComoSabiaDeKunaAty = models.IntegerField(choices = Llegada.Opciones, verbose_name = "¿Cómo sabía de Kuña Aty?", null = True, blank = True)
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in FichaSocial._meta.fields]
    
    def get_absolute_url(self):
        return reverse("ver_ficha", kwargs = {"pk": self.Paciente.pk, "tipo": self.ficha_type, "ficha_pk": self.pk})

class AparienciaPersonal:
    OPCIONES = (('D', "Descuidada"), ('C', "Cuidada"))

class LesionesFisicas:
    OPCIONES = (('N', "No presente"), ('L', "Leves"), ('M', "Moderadas"), ('G', "Graves"))
    PREDETERMINADO = 'N'

class IndicadoresCognitivos(models.Model):
    nombre = models.CharField(max_length = 25)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class IndicadoresFisicos(models.Model):
    nombre = models.CharField(max_length = 25)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class IndicadoresEmocionales(models.Model):
    nombre = models.CharField(max_length = 40)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class IndicadoresConductuales(models.Model):
    nombre = models.CharField(max_length = 25)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class ProblemasSociales(models.Model):
    nombre = models.CharField(max_length = 50)
    predeterminado = models.BooleanField()
    
    def __unicode__(self):
        return self.nombre

class FichaPsicologica(FichaBase):
    TipoDeViolencia = models.ManyToManyField(TiposDeViolencia, related_name = "+", blank = True)
    IndicadoresCognitivos = models.ManyToManyField(IndicadoresCognitivos, related_name = "+", blank = True)
    IndicadoresFisicos = models.ManyToManyField(IndicadoresFisicos, related_name = "+", blank = True)
    AparienciaPersonal = models.CharField(max_length = 1, choices = AparienciaPersonal.OPCIONES, null = True, blank = True, verbose_name = "Apariencia Personal")
    IndicadoresEmocionales = models.ManyToManyField(IndicadoresEmocionales, related_name = "+", blank = True)
    IndicadoresConductuales = models.ManyToManyField(IndicadoresConductuales, related_name = "+", blank = True)
    ProblemasSociales = models.ManyToManyField(ProblemasSociales, related_name = "+", blank = True)
    LesionesFisicas = models.CharField(max_length = 1, choices = LesionesFisicas.OPCIONES, default = LesionesFisicas.PREDETERMINADO, verbose_name = "Lesiones Físicas")
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in FichaPsicologica._meta.fields]
    
    def get_absolute_url(self):
        return reverse("ver_ficha", kwargs = {"pk": self.Paciente.pk, "tipo": self.ficha_type, "ficha_pk": self.pk})

class Anticonceptivos:
    Opciones= (
        (0,"Ninguno"), (1, "Condon"), (2, "DIU"),(3, "Inyectable"), (4, "Anticonceptivos Orales"), (5, "Metodo Natural"), (6, "Espermicida") )

class FichaMedica(FichaBase):
    Nro_Embarazos = models.PositiveIntegerField(null = True, blank = True)
    Nro_Nacido_vivo = models.PositiveIntegerField(null = True, blank = True)
    Nro_Cesareas= models.PositiveIntegerField(null = True, blank = True)
    Nro_PartosNormales= models.PositiveIntegerField(null = True, blank = True)
    Nro_Abortos = models.PositiveIntegerField(null = True, blank = True)
    Menarca = models.PositiveIntegerField(null = True, blank = True)
    Menopausia= models.PositiveIntegerField(null = True, blank = True)
    Metodo_anticonceptivo = models.IntegerField(choices = Anticonceptivos.Opciones, null = True, blank = True)
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in FichaMedica._meta.fields]
    
    def get_absolute_url(self):
        return reverse("ver_ficha", kwargs = {"pk": self.Paciente.pk, "tipo": self.ficha_type, "ficha_pk": self.pk})
 
class CasosJuridicos(models.Model):
    nombre = models.CharField(max_length = 45)
    
    def __unicode__(self):
        return self.nombre

class AA:
    Opciones= [(1, "Prenatal"), (2, "Ofrecimiento"), (3, "Aumento"), (4, "Disminucion"), (5, "Cesacion")]
        
class FIL:
    Opciones = [(1, "Impugnacion"),(2, "Desconocimiento")]
    
class TipoFuero:
    Opciones=[(1, "Civil"), (2, "Niñez")]
        
class TipoJuicio:
    Opciones = [(1, "SD"), (2, "AI"), (3, "Providencia")] 
    
class Es_Esto_Un:
    Opciones = [(1, "Acuerdo"), (2, "Juicio")]
    
class FichaJuridica(FichaBase):
    Motivo_Consulta= models.ManyToManyField(CasosJuridicos, related_name = "+", blank = True, null = True)
    Asistencia_Alimenticia = models.IntegerField(choices= AA.Opciones, null = True, blank = True)
    Filiacion = models.IntegerField(choices= FIL.Opciones, null = True, blank = True)
    Es_Esto_Un = models.IntegerField(choices= Es_Esto_Un.Opciones, null = True, blank = True)
    Tipo_Juicio= models.IntegerField(choices= TipoJuicio.Opciones, null = True, blank = True)
    Fecha_Inicio = models.DateField(null = True, blank = True)
    Fecha_Resolucion= models.DateField(null = True, blank = True)
    Nro_Resolucion= models.PositiveIntegerField(null = True, blank = True)
    Juzgado= models.CharField(max_length=30, null = True, blank = True)
    Fueros= models.IntegerField(choices= TipoFuero.Opciones, null = True, blank = True)
    Secretaria= models.PositiveIntegerField(null = True, blank = True)
    Caratula_Expediente = models.CharField(max_length = 100, null = True, blank = True, verbose_name = "Carátula del expediente")
    
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in FichaJuridica._meta.fields]
    
    def get_absolute_url(self):
        return reverse("ver_ficha", kwargs = {"pk": self.Paciente.pk, "tipo": self.ficha_type, "ficha_pk": self.pk})
