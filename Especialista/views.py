# coding=utf-8
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden, Http404
from django.views.generic import ListView, DetailView, UpdateView
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse

from Especialista.models import FichaBase, Observacion, FichaInfantil
from forms import buscarFicha, buscarFichaAdolescente, ObservacionesFormulario, FormularioFichaPsicosocialInfante
from django.forms.models import inlineformset_factory
from Personas.models import DatosDePacienteAdulto, DatosInfantiles, DatosDePacienteAdolescente

from util.autoview import AutoVista, LinkField, StringField
##############################################################
import os

from django.shortcuts import render
#from django.contrib.auth.decorators import login_required
#from django.http import HttpResponse
#from django.shortcuts import render
from django.db.models import Count

from django.views.generic import ListView, DetailView, UpdateView

from Especialista.models import FichaBase
from Usuarios.models import KunaAtyUsuario, Mensaje
from KunaAty.settings import relativoALaRaiz

import reportlab.platypus
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, Image
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch

PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]

def datos_entre_fechas(fecha_inicio, fecha_fin):
    fichas = FichaBase.objects.filter(FechaActualizada__gte = fecha_inicio, FechaActualizada__lte = fecha_fin)
    return fichas

def sociales_adultos(fichas):
    return fichas.filter(fichasocial__isnull = False)

def sociales_adolescentes(fichas):
    return fichas.filter(fichasocialadolescente__isnull = False)

def sociales_infantes(fichas):
    return fichas.filter(fichainfantil__isnull = False)

def dibujarPdf(respuesta, av):


    def primeraPagina(canvas, doc):
        canvas.saveState()
        canvas.drawImage(relativoALaRaiz("..\\static\\img\\image.jpg"),120, 765, 289, 59)
        canvas.setFont('Times-Bold',14)
        canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-127, "Informe Estadístico")
        canvas.setFont('Times-Bold',12)
        canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-140, "Desde %s - Hasta %s")
        canvas.setFont('Times-Roman',9)
        canvas.drawString(inch, 0.75 * inch,"Página 1")
        canvas.restoreState()
    
    def siguientesPaginas(canvas, doc):
        canvas.saveState()
        canvas.drawImage(relativoALaRaiz("..\\static\\img\\image.jpg"),120, 765, 289, 59)
        canvas.setFont('Times-Roman', 9)
        canvas.drawString(inch, 0.75 * inch,"Página %d" % (doc.page,))
        canvas.restoreState()
    
    def tabla(cabecera, filas, destacarFilas = []):
        filas.insert(0, [cabecera, ""])
        t = Table(filas, style=[('GRID',(0,0),(-1,-1),0.5,colors.grey),
                                ('SPAN', (0,0),(1,0))
                                ], colWidths=200, rowHeights=20)
        return t
        
    doc = SimpleDocTemplate(respuesta)
    contenido = [Spacer(1,1*inch)]
    contenido.append(tabla("Resumen de atenciones realizadas", [[ av]]))#,
    doc.build(contenido, onFirstPage = primeraPagina, onLaterPages = siguientesPaginas)

# @login_required
# @user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
# def ListadoFichaSociales(solicitud, pk):
    # paciente = get_object_or_404(DatosDePacienteAdulto, pk = pk)
    # fichaSociales_list = []
    # fichaSociales_list = paciente.fichasSociales
    # return render(solicitud, "ver_paciente.html", {'fichas': fichaSociales_list})
######################################################################################

import ho.pisa as pisa
import cStringIO as StringIO
import cgi
from django.template import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse
def generar_pdf(html):  
    # Función para generar el archivo PDF y devolverlo mediante HttpResponse
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('Error al generar el PDF: %s' % cgi.escape(html))

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def ImprimirFicha(request, pk, tipo, ficha_pk):
    ficha = FichaBase.objects.filter(pk = ficha_pk, Paciente_id = pk)
    if not ficha.exists():
        return HttpResponseForbidden("La ficha no corresponde con la persona")
    actual_ficha = ficha.get().sub_model
    av = FichaVista(actual_ficha)
    #respuesta = HttpResponse(content_type='application/pdf')
    #dibujarPdf(respuesta, av)
    #return respuesta
    #return render(solicitud, 'imprimir_ficha.html',{ 'autovista': av})
    html = render_to_string('imprimir_ficha.html', {'pagesize':'A4', 'autovista': av,"object": actual_ficha}, context_instance=RequestContext(request))
    return generar_pdf(html)
###########################################################################################    
@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def FichaListado(solicitud, pk, tipo):
    paciente = get_object_or_404(DatosDePacienteAdulto, pk = pk)
    ficha_list = []
    fichaFinalizado_list = []
    if tipo == 'social':
        ficha_list = paciente.fichasSociales.filter(Finalizado = False).order_by("-FechaActualizada")
        fichaFinalizado_list = paciente.fichasSociales.filter(Finalizado = True).order_by("-FechaActualizada")
    elif tipo == 'medica':
        ficha_list = paciente.fichasMedicas.filter(Finalizado = False).order_by("-FechaActualizada")
        fichaFinalizado_list = paciente.fichasMedicas.filter(Finalizado = True).order_by("-FechaActualizada")
    elif tipo == 'juridica':
        ficha_list = paciente.fichasJuridicas.filter(Finalizado = False).order_by("-FechaActualizada")
        fichaFinalizado_list = paciente.fichasJuridicas.filter(Finalizado = True).order_by("-FechaActualizada")
    elif tipo == 'psicologica':
        ficha_list = paciente.fichasPsicologicas.filter(Finalizado = False).order_by("-FechaActualizada")
        fichaFinalizado_list = paciente.fichasPsicologicas.filter(Finalizado = True).order_by("-FechaActualizada")
    else:
        return Http404()
    return render(solicitud, "ficha_listado.html",{'paciente': paciente, 'fichas': ficha_list, 'tipo': tipo, 'fichasFinalizado': fichaFinalizado_list})

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def FichaListadoAdolescente(solicitud, pk, tipo):
    paciente = get_object_or_404(DatosDePacienteAdolescente, pk = pk)
    ficha_list = []
    fichaFinalizado_list = []
    if tipo == 'social':
        ficha_list = paciente.fichasSociales.filter(Finalizado = False).order_by("-FechaActualizada")
        fichaFinalizado_list = paciente.fichasSociales.filter(Finalizado = True).order_by("-FechaActualizada")
    elif tipo == 'psicologica':
        ficha_list = paciente.fichasPsicologicas.filter(Finalizado = False).order_by("-FechaActualizada")
        fichaFinalizado_list = paciente.fichasPsicologicas.filter(Finalizado = True).order_by("-FechaActualizada")
    else:
        return Http404()
    return render(solicitud, "ficha_adolescente_listado.html", {'paciente': paciente, 'fichas': ficha_list, 'tipo': tipo, 'fichasFinalizado': fichaFinalizado_list})

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def FichaListadoInfante(solicitud, pk):
    paciente = get_object_or_404(DatosInfantiles, pk = pk)
    #ficha_list = paciente.fichabase_set.all()
    ficha_list = paciente.fichabase_set.filter(Finalizado = False).order_by("-FechaActualizada")
    fichaFinalizado_list = []
    fichaFinalizado_list =paciente.fichabase_set.filter(Finalizado = True).order_by("-FechaActualizada")
    return render(solicitud, "ficha_infante_listado.html", {'paciente': paciente, 'fichas': ficha_list, 'fichasFinalizado': fichaFinalizado_list})

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def CrearFicha(solicitud, pk, tipo, ficha_pk = None):

    ficha = None
    if ficha_pk is not None:
        ficha = get_object_or_404(FichaBase.get_model(tipo), pk = ficha_pk)
    if not DatosDePacienteAdulto.objects.filter(pk = pk).exists():
        raise Http404()
    fichaFormulario = buscarFicha(tipo)(solicitud.POST or None, instance = ficha, prefix = "ficha")
    extras = [x[1][0](solicitud.POST or None, prefix = 'extra'+str(x[0]), instance = x[1][1], empty_permitted = not x[1][0].requerido) for x in enumerate(fichaFormulario.extras)]
    obser_formset = inlineformset_factory(FichaBase, Observacion, extra = 1, form = ObservacionesFormulario)
    if fichaFormulario.is_valid() and all([x.is_valid() for x in extras]):
        obj = fichaFormulario.save(commit = False)
        obj.Paciente_id = pk
        obj.ProfesionalTratante = solicitud.user
        for f in extras:
            fo = f.save()
            fo.PacienteAdulto = obj
        obser_formset = obser_formset(solicitud.POST, instance = obj, prefix = "obs")
        if obser_formset.is_valid():
            obj.save()
            fichaFormulario.save_m2m()
            obser_formset.save()
            return HttpResponseRedirect(obj.get_absolute_url())
    else:
        obser_formset = obser_formset(prefix = "obs")
    return render(solicitud, "ficha.html", {"ficha": fichaFormulario,
                                            "extras": extras,
                                            "obs": obser_formset,
                                            "paciente": DatosDePacienteAdulto.objects.get(pk=pk),
                                            "ficha_tipo": tipo})

    

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def CrearFichaInfante(solicitud, pk, ficha_pk = None):
    ficha = None
    if ficha_pk is not None:
        ficha = get_object_or_404(FichaInfantil, pk = ficha_pk)
    fichaFormulario = FormularioFichaPsicosocialInfante(solicitud.POST or None, instance = ficha, prefix = "ficha")
    obser_formset = inlineformset_factory(FichaBase, Observacion, extra = 1, form = ObservacionesFormulario)
    if fichaFormulario.is_valid() and DatosInfantiles.objects.filter(pk = pk).exists():
        obj = fichaFormulario.save(commit = False)
        obj.Paciente_id = pk
        obj.ProfesionalTratante=solicitud.user ###
        obser_formset = obser_formset(solicitud.POST, instance = obj, prefix = "obs")
        if obser_formset.is_valid():
            obj.save()
            fichaFormulario.save_m2m()
            obser_formset.save()
            return HttpResponseRedirect(obj.get_absolute_url())
    else:
        obser_formset = obser_formset(prefix = "obs")
    return render(solicitud, "ficha.html", {"ficha": fichaFormulario,
                                            "obs": obser_formset,
                                            "paciente": DatosInfantiles.objects.get(pk=pk),
                                            "ficha_tipo": "Ficha Infantil"})

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def CrearFichaAdolescente(solicitud, pk, tipo, ficha_pk = None):
    ficha = None
    if ficha_pk is not None:
        ficha = get_object_or_404(FichaBase.get_model("ad_" + tipo), pk = ficha_pk)
    fichaFormulario = buscarFichaAdolescente(tipo)(solicitud.POST or None, prefix = "ficha", instance = ficha)
    extras = [x[0](solicitud.POST or None, prefix = 'extra'+str(x[1])) for x in zip(fichaFormulario.extras, xrange(100))]
    obser_formset = inlineformset_factory(FichaBase, Observacion, extra = 1, form = ObservacionesFormulario)
    if fichaFormulario.is_valid() and DatosDePacienteAdolescente.objects.filter(pk = pk).exists() and all([x.is_valid() for x in extras if x.requerido]):
        obj = fichaFormulario.save(commit = False)
        obj.Paciente_id = pk
        obj.ProfesionalTratante =solicitud.user###
        for f in extras:
            if f.requerido or f.is_valid():
                fo = f.save()
                fo.PacienteAdolescente = obj
        obser_formset = obser_formset(solicitud.POST, instance = obj, prefix = "obs")
        if obser_formset.is_valid():
            obj.save()
            fichaFormulario.save_m2m()
            obser_formset.save()
            return HttpResponseRedirect(obj.get_absolute_url())
    else:
        obser_formset = obser_formset(prefix = "obs")
    return render(solicitud, "ficha.html", {"ficha": fichaFormulario,
                                            "extras": extras,
                                            "obs": obser_formset,
                                            "paciente": DatosDePacienteAdolescente.objects.get(pk=pk),
                                            "ficha_tipo": tipo})

class FichaVista(AutoVista):
    class Meta:
        fields = {
            "Paciente": LinkField,
            "FrecuenciaDeViolencia": StringField,
            "ProfesionalTratante": StringField
        }

@login_required
@user_passes_test(lambda u:u.tipo > 0 or u.is_staff)
def VerFicha(solicitud, pk, tipo, ficha_pk):
    ficha = FichaBase.objects.filter(pk = ficha_pk, Paciente_id = pk)
    if not ficha.exists():
        return HttpResponseForbidden("La ficha no corresponde con la persona")
    actual_ficha = ficha.get().sub_model
    av = FichaVista(actual_ficha)
    return render(solicitud, "ver_ficha.html", {"autovista": av, "object": actual_ficha})

