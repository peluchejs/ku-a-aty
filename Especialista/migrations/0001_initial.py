# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MotivoDeConsulta'
        db.create_table(u'Especialista_motivodeconsulta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=35)),
        ))
        db.send_create_signal(u'Especialista', ['MotivoDeConsulta'])

        # Adding model 'TiposDeViolencia'
        db.create_table(u'Especialista_tiposdeviolencia', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal(u'Especialista', ['TiposDeViolencia'])

        # Adding model 'ViolenciaFrecuencia'
        db.create_table(u'Especialista_violenciafrecuencia', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['ViolenciaFrecuencia'])

        # Adding model 'FichaBase'
        db.create_table(u'Especialista_fichabase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Paciente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Personas.DatosPersonalesBasicos'], null=True, blank=True)),
            ('FechaActualizada', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('ProfesionalTratante', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Usuarios.KunaAtyUsuario'])),
            ('Finalizado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['FichaBase'])

        # Adding model 'Observacion'
        db.create_table(u'Especialista_observacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('texto', self.gf('django.db.models.fields.TextField')()),
            ('fecha', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('ficha', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Especialista.FichaBase'])),
        ))
        db.send_create_signal(u'Especialista', ['Observacion'])

        # Adding model 'FuenteDeDerivacion'
        db.create_table(u'Especialista_fuentedederivacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Institucion', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('Direccion', self.gf('django.db.models.fields.related.OneToOneField')(related_name='+', unique=True, to=orm['Personas.Direccion'])),
            ('Telefono', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'Especialista', ['FuenteDeDerivacion'])

        # Adding model 'IntervencionAnterior'
        db.create_table(u'Especialista_intervencionanterior', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Institucion', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('MotivoDeConsulta', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('Resultado', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'Especialista', ['IntervencionAnterior'])

        # Adding model 'IndicadoresFisicosInfantiles'
        db.create_table(u'Especialista_indicadoresfisicosinfantiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresFisicosInfantiles'])

        # Adding model 'IndicadoresEmocionalesInfantiles'
        db.create_table(u'Especialista_indicadoresemocionalesinfantiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresEmocionalesInfantiles'])

        # Adding model 'IndicadoresCognitivosInfantiles'
        db.create_table(u'Especialista_indicadorescognitivosinfantiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresCognitivosInfantiles'])

        # Adding model 'IndicadoresComportamentalesInfantiles'
        db.create_table(u'Especialista_indicadorescomportamentalesinfantiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresComportamentalesInfantiles'])

        # Adding model 'FichaInfantil'
        db.create_table(u'Especialista_fichainfantil', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('FuenteDerivacion', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FuenteDeDerivacion'], unique=True, null=True, blank=True)),
            ('Intervenciones', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.IntervencionAnterior'], unique=True, null=True, blank=True)),
            ('HistoriaFamiliar', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('Seguimientos', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Especialista', ['FichaInfantil'])

        # Adding M2M table for field MotivosDeConsulta on 'FichaInfantil'
        m2m_table_name = db.shorten_name(u'Especialista_fichainfantil_MotivosDeConsulta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichainfantil', models.ForeignKey(orm[u'Especialista.fichainfantil'], null=False)),
            ('motivodeconsulta', models.ForeignKey(orm[u'Especialista.motivodeconsulta'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichainfantil_id', 'motivodeconsulta_id'])

        # Adding M2M table for field TipoDeViolencia on 'FichaInfantil'
        m2m_table_name = db.shorten_name(u'Especialista_fichainfantil_TipoDeViolencia')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichainfantil', models.ForeignKey(orm[u'Especialista.fichainfantil'], null=False)),
            ('tiposdeviolencia', models.ForeignKey(orm[u'Especialista.tiposdeviolencia'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichainfantil_id', 'tiposdeviolencia_id'])

        # Adding M2M table for field IndicadoresFisicos on 'FichaInfantil'
        m2m_table_name = db.shorten_name(u'Especialista_fichainfantil_IndicadoresFisicos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichainfantil', models.ForeignKey(orm[u'Especialista.fichainfantil'], null=False)),
            ('indicadoresfisicosinfantiles', models.ForeignKey(orm[u'Especialista.indicadoresfisicosinfantiles'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichainfantil_id', 'indicadoresfisicosinfantiles_id'])

        # Adding M2M table for field IndicadoresEmocionales on 'FichaInfantil'
        m2m_table_name = db.shorten_name(u'Especialista_fichainfantil_IndicadoresEmocionales')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichainfantil', models.ForeignKey(orm[u'Especialista.fichainfantil'], null=False)),
            ('indicadoresemocionalesinfantiles', models.ForeignKey(orm[u'Especialista.indicadoresemocionalesinfantiles'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichainfantil_id', 'indicadoresemocionalesinfantiles_id'])

        # Adding M2M table for field IndicadoresCognitivos on 'FichaInfantil'
        m2m_table_name = db.shorten_name(u'Especialista_fichainfantil_IndicadoresCognitivos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichainfantil', models.ForeignKey(orm[u'Especialista.fichainfantil'], null=False)),
            ('indicadorescognitivosinfantiles', models.ForeignKey(orm[u'Especialista.indicadorescognitivosinfantiles'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichainfantil_id', 'indicadorescognitivosinfantiles_id'])

        # Adding M2M table for field IndicadoresComportamentales on 'FichaInfantil'
        m2m_table_name = db.shorten_name(u'Especialista_fichainfantil_IndicadoresComportamentales')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichainfantil', models.ForeignKey(orm[u'Especialista.fichainfantil'], null=False)),
            ('indicadorescomportamentalesinfantiles', models.ForeignKey(orm[u'Especialista.indicadorescomportamentalesinfantiles'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichainfantil_id', 'indicadorescomportamentalesinfantiles_id'])

        # Adding model 'FichaSocialAdolescente'
        db.create_table(u'Especialista_fichasocialadolescente', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('Agresor', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='PacienteAdolescente', unique=True, null=True, to=orm['Personas.DatosAgresor'])),
            ('ViveConAgresor', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('RelacionConAgresor', self.gf('django.db.models.fields.IntegerField')()),
            ('FrecuenciaDeViolencia', self.gf('django.db.models.fields.related.ForeignKey')(related_name='fsa+', to=orm['Especialista.ViolenciaFrecuencia'])),
            ('DiagnosticoMedico', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaComisaria', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaJuridico', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaPolicia', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaFiscalia', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ComoSabiaDeKunaAty', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'Especialista', ['FichaSocialAdolescente'])

        # Adding M2M table for field MotivosDeConsulta on 'FichaSocialAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichasocialadolescente_MotivosDeConsulta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichasocialadolescente', models.ForeignKey(orm[u'Especialista.fichasocialadolescente'], null=False)),
            ('motivodeconsulta', models.ForeignKey(orm[u'Especialista.motivodeconsulta'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichasocialadolescente_id', 'motivodeconsulta_id'])

        # Adding M2M table for field TipoDeViolencia on 'FichaSocialAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichasocialadolescente_TipoDeViolencia')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichasocialadolescente', models.ForeignKey(orm[u'Especialista.fichasocialadolescente'], null=False)),
            ('tiposdeviolencia', models.ForeignKey(orm[u'Especialista.tiposdeviolencia'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichasocialadolescente_id', 'tiposdeviolencia_id'])

        # Adding model 'IndicadoresViolenciaFisicos'
        db.create_table(u'Especialista_indicadoresviolenciafisicos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresViolenciaFisicos'])

        # Adding model 'IndicadoresViolenciaEmocionales'
        db.create_table(u'Especialista_indicadoresviolenciaemocionales', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresViolenciaEmocionales'])

        # Adding model 'IndicadoresViolenciaComportamentales'
        db.create_table(u'Especialista_indicadoresviolenciacomportamentales', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresViolenciaComportamentales'])

        # Adding model 'IndicadoresViolenciaCognitivos'
        db.create_table(u'Especialista_indicadoresviolenciacognitivos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresViolenciaCognitivos'])

        # Adding model 'FichaPsicologicaAdolescente'
        db.create_table(u'Especialista_fichapsicologicaadolescente', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('DescripcionDelCaso', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Especialista', ['FichaPsicologicaAdolescente'])

        # Adding M2M table for field MotivosDeConsulta on 'FichaPsicologicaAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologicaadolescente_MotivosDeConsulta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologicaadolescente', models.ForeignKey(orm[u'Especialista.fichapsicologicaadolescente'], null=False)),
            ('motivodeconsulta', models.ForeignKey(orm[u'Especialista.motivodeconsulta'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologicaadolescente_id', 'motivodeconsulta_id'])

        # Adding M2M table for field Fisico on 'FichaPsicologicaAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologicaadolescente_Fisico')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologicaadolescente', models.ForeignKey(orm[u'Especialista.fichapsicologicaadolescente'], null=False)),
            ('indicadoresviolenciafisicos', models.ForeignKey(orm[u'Especialista.indicadoresviolenciafisicos'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologicaadolescente_id', 'indicadoresviolenciafisicos_id'])

        # Adding M2M table for field Emocional on 'FichaPsicologicaAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologicaadolescente_Emocional')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologicaadolescente', models.ForeignKey(orm[u'Especialista.fichapsicologicaadolescente'], null=False)),
            ('indicadoresviolenciaemocionales', models.ForeignKey(orm[u'Especialista.indicadoresviolenciaemocionales'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologicaadolescente_id', 'indicadoresviolenciaemocionales_id'])

        # Adding M2M table for field Comportamentales on 'FichaPsicologicaAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologicaadolescente_Comportamentales')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologicaadolescente', models.ForeignKey(orm[u'Especialista.fichapsicologicaadolescente'], null=False)),
            ('indicadoresviolenciacomportamentales', models.ForeignKey(orm[u'Especialista.indicadoresviolenciacomportamentales'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologicaadolescente_id', 'indicadoresviolenciacomportamentales_id'])

        # Adding M2M table for field Cognitivos on 'FichaPsicologicaAdolescente'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologicaadolescente_Cognitivos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologicaadolescente', models.ForeignKey(orm[u'Especialista.fichapsicologicaadolescente'], null=False)),
            ('indicadoresviolenciacognitivos', models.ForeignKey(orm[u'Especialista.indicadoresviolenciacognitivos'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologicaadolescente_id', 'indicadoresviolenciacognitivos_id'])

        # Adding model 'FichaSocial'
        db.create_table(u'Especialista_fichasocial', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('Agresor', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='PacienteAdulto', unique=True, null=True, to=orm['Personas.DatosAgresor'])),
            ('ViveConAgresor', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('RelacionConAgresor', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('FrecuenciaDeViolencia', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='fs+', null=True, to=orm['Especialista.ViolenciaFrecuencia'])),
            ('DiagnosticoMedico', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaComisaria', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaJuridico', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaPolicia', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('DenunciaFiscalia', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ComoSabiaDeKunaAty', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Especialista', ['FichaSocial'])

        # Adding M2M table for field MotivosDeConsulta on 'FichaSocial'
        m2m_table_name = db.shorten_name(u'Especialista_fichasocial_MotivosDeConsulta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichasocial', models.ForeignKey(orm[u'Especialista.fichasocial'], null=False)),
            ('motivodeconsulta', models.ForeignKey(orm[u'Especialista.motivodeconsulta'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichasocial_id', 'motivodeconsulta_id'])

        # Adding M2M table for field TipoDeViolencia on 'FichaSocial'
        m2m_table_name = db.shorten_name(u'Especialista_fichasocial_TipoDeViolencia')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichasocial', models.ForeignKey(orm[u'Especialista.fichasocial'], null=False)),
            ('tiposdeviolencia', models.ForeignKey(orm[u'Especialista.tiposdeviolencia'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichasocial_id', 'tiposdeviolencia_id'])

        # Adding model 'IndicadoresCognitivos'
        db.create_table(u'Especialista_indicadorescognitivos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresCognitivos'])

        # Adding model 'IndicadoresFisicos'
        db.create_table(u'Especialista_indicadoresfisicos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresFisicos'])

        # Adding model 'IndicadoresEmocionales'
        db.create_table(u'Especialista_indicadoresemocionales', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresEmocionales'])

        # Adding model 'IndicadoresConductuales'
        db.create_table(u'Especialista_indicadoresconductuales', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['IndicadoresConductuales'])

        # Adding model 'ProblemasSociales'
        db.create_table(u'Especialista_problemassociales', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('predeterminado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Especialista', ['ProblemasSociales'])

        # Adding model 'FichaPsicologica'
        db.create_table(u'Especialista_fichapsicologica', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('AparienciaPersonal', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('LesionesFisicas', self.gf('django.db.models.fields.CharField')(default='N', max_length=1)),
        ))
        db.send_create_signal(u'Especialista', ['FichaPsicologica'])

        # Adding M2M table for field TipoDeViolencia on 'FichaPsicologica'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologica_TipoDeViolencia')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologica', models.ForeignKey(orm[u'Especialista.fichapsicologica'], null=False)),
            ('tiposdeviolencia', models.ForeignKey(orm[u'Especialista.tiposdeviolencia'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologica_id', 'tiposdeviolencia_id'])

        # Adding M2M table for field IndicadoresCognitivos on 'FichaPsicologica'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologica_IndicadoresCognitivos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologica', models.ForeignKey(orm[u'Especialista.fichapsicologica'], null=False)),
            ('indicadorescognitivos', models.ForeignKey(orm[u'Especialista.indicadorescognitivos'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologica_id', 'indicadorescognitivos_id'])

        # Adding M2M table for field IndicadoresFisicos on 'FichaPsicologica'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologica_IndicadoresFisicos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologica', models.ForeignKey(orm[u'Especialista.fichapsicologica'], null=False)),
            ('indicadoresfisicos', models.ForeignKey(orm[u'Especialista.indicadoresfisicos'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologica_id', 'indicadoresfisicos_id'])

        # Adding M2M table for field IndicadoresEmocionales on 'FichaPsicologica'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologica_IndicadoresEmocionales')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologica', models.ForeignKey(orm[u'Especialista.fichapsicologica'], null=False)),
            ('indicadoresemocionales', models.ForeignKey(orm[u'Especialista.indicadoresemocionales'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologica_id', 'indicadoresemocionales_id'])

        # Adding M2M table for field IndicadoresConductuales on 'FichaPsicologica'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologica_IndicadoresConductuales')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologica', models.ForeignKey(orm[u'Especialista.fichapsicologica'], null=False)),
            ('indicadoresconductuales', models.ForeignKey(orm[u'Especialista.indicadoresconductuales'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologica_id', 'indicadoresconductuales_id'])

        # Adding M2M table for field ProblemasSociales on 'FichaPsicologica'
        m2m_table_name = db.shorten_name(u'Especialista_fichapsicologica_ProblemasSociales')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichapsicologica', models.ForeignKey(orm[u'Especialista.fichapsicologica'], null=False)),
            ('problemassociales', models.ForeignKey(orm[u'Especialista.problemassociales'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichapsicologica_id', 'problemassociales_id'])

        # Adding model 'FichaMedica'
        db.create_table(u'Especialista_fichamedica', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('Nro_Embarazos', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Nro_Nacido_vivo', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Nro_Cesareas', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Nro_PartosNormales', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Nro_Abortos', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Menarca', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Menopausia', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Metodo_anticonceptivo', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Especialista', ['FichaMedica'])

        # Adding model 'CasosJuridicos'
        db.create_table(u'Especialista_casosjuridicos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=45)),
        ))
        db.send_create_signal(u'Especialista', ['CasosJuridicos'])

        # Adding model 'FichaJuridica'
        db.create_table(u'Especialista_fichajuridica', (
            (u'fichabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Especialista.FichaBase'], unique=True, primary_key=True)),
            ('Asistencia_Alimenticia', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Filiacion', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Es_Esto_Un', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Tipo_Juicio', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Fecha_Inicio', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('Fecha_Resolucion', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('Nro_Resolucion', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Juzgado', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('Fueros', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('Secretaria', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('Caratula_Expediente', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'Especialista', ['FichaJuridica'])

        # Adding M2M table for field Motivo_Consulta on 'FichaJuridica'
        m2m_table_name = db.shorten_name(u'Especialista_fichajuridica_Motivo_Consulta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fichajuridica', models.ForeignKey(orm[u'Especialista.fichajuridica'], null=False)),
            ('casosjuridicos', models.ForeignKey(orm[u'Especialista.casosjuridicos'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fichajuridica_id', 'casosjuridicos_id'])


    def backwards(self, orm):
        # Deleting model 'MotivoDeConsulta'
        db.delete_table(u'Especialista_motivodeconsulta')

        # Deleting model 'TiposDeViolencia'
        db.delete_table(u'Especialista_tiposdeviolencia')

        # Deleting model 'ViolenciaFrecuencia'
        db.delete_table(u'Especialista_violenciafrecuencia')

        # Deleting model 'FichaBase'
        db.delete_table(u'Especialista_fichabase')

        # Deleting model 'Observacion'
        db.delete_table(u'Especialista_observacion')

        # Deleting model 'FuenteDeDerivacion'
        db.delete_table(u'Especialista_fuentedederivacion')

        # Deleting model 'IntervencionAnterior'
        db.delete_table(u'Especialista_intervencionanterior')

        # Deleting model 'IndicadoresFisicosInfantiles'
        db.delete_table(u'Especialista_indicadoresfisicosinfantiles')

        # Deleting model 'IndicadoresEmocionalesInfantiles'
        db.delete_table(u'Especialista_indicadoresemocionalesinfantiles')

        # Deleting model 'IndicadoresCognitivosInfantiles'
        db.delete_table(u'Especialista_indicadorescognitivosinfantiles')

        # Deleting model 'IndicadoresComportamentalesInfantiles'
        db.delete_table(u'Especialista_indicadorescomportamentalesinfantiles')

        # Deleting model 'FichaInfantil'
        db.delete_table(u'Especialista_fichainfantil')

        # Removing M2M table for field MotivosDeConsulta on 'FichaInfantil'
        db.delete_table(db.shorten_name(u'Especialista_fichainfantil_MotivosDeConsulta'))

        # Removing M2M table for field TipoDeViolencia on 'FichaInfantil'
        db.delete_table(db.shorten_name(u'Especialista_fichainfantil_TipoDeViolencia'))

        # Removing M2M table for field IndicadoresFisicos on 'FichaInfantil'
        db.delete_table(db.shorten_name(u'Especialista_fichainfantil_IndicadoresFisicos'))

        # Removing M2M table for field IndicadoresEmocionales on 'FichaInfantil'
        db.delete_table(db.shorten_name(u'Especialista_fichainfantil_IndicadoresEmocionales'))

        # Removing M2M table for field IndicadoresCognitivos on 'FichaInfantil'
        db.delete_table(db.shorten_name(u'Especialista_fichainfantil_IndicadoresCognitivos'))

        # Removing M2M table for field IndicadoresComportamentales on 'FichaInfantil'
        db.delete_table(db.shorten_name(u'Especialista_fichainfantil_IndicadoresComportamentales'))

        # Deleting model 'FichaSocialAdolescente'
        db.delete_table(u'Especialista_fichasocialadolescente')

        # Removing M2M table for field MotivosDeConsulta on 'FichaSocialAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichasocialadolescente_MotivosDeConsulta'))

        # Removing M2M table for field TipoDeViolencia on 'FichaSocialAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichasocialadolescente_TipoDeViolencia'))

        # Deleting model 'IndicadoresViolenciaFisicos'
        db.delete_table(u'Especialista_indicadoresviolenciafisicos')

        # Deleting model 'IndicadoresViolenciaEmocionales'
        db.delete_table(u'Especialista_indicadoresviolenciaemocionales')

        # Deleting model 'IndicadoresViolenciaComportamentales'
        db.delete_table(u'Especialista_indicadoresviolenciacomportamentales')

        # Deleting model 'IndicadoresViolenciaCognitivos'
        db.delete_table(u'Especialista_indicadoresviolenciacognitivos')

        # Deleting model 'FichaPsicologicaAdolescente'
        db.delete_table(u'Especialista_fichapsicologicaadolescente')

        # Removing M2M table for field MotivosDeConsulta on 'FichaPsicologicaAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologicaadolescente_MotivosDeConsulta'))

        # Removing M2M table for field Fisico on 'FichaPsicologicaAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologicaadolescente_Fisico'))

        # Removing M2M table for field Emocional on 'FichaPsicologicaAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologicaadolescente_Emocional'))

        # Removing M2M table for field Comportamentales on 'FichaPsicologicaAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologicaadolescente_Comportamentales'))

        # Removing M2M table for field Cognitivos on 'FichaPsicologicaAdolescente'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologicaadolescente_Cognitivos'))

        # Deleting model 'FichaSocial'
        db.delete_table(u'Especialista_fichasocial')

        # Removing M2M table for field MotivosDeConsulta on 'FichaSocial'
        db.delete_table(db.shorten_name(u'Especialista_fichasocial_MotivosDeConsulta'))

        # Removing M2M table for field TipoDeViolencia on 'FichaSocial'
        db.delete_table(db.shorten_name(u'Especialista_fichasocial_TipoDeViolencia'))

        # Deleting model 'IndicadoresCognitivos'
        db.delete_table(u'Especialista_indicadorescognitivos')

        # Deleting model 'IndicadoresFisicos'
        db.delete_table(u'Especialista_indicadoresfisicos')

        # Deleting model 'IndicadoresEmocionales'
        db.delete_table(u'Especialista_indicadoresemocionales')

        # Deleting model 'IndicadoresConductuales'
        db.delete_table(u'Especialista_indicadoresconductuales')

        # Deleting model 'ProblemasSociales'
        db.delete_table(u'Especialista_problemassociales')

        # Deleting model 'FichaPsicologica'
        db.delete_table(u'Especialista_fichapsicologica')

        # Removing M2M table for field TipoDeViolencia on 'FichaPsicologica'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologica_TipoDeViolencia'))

        # Removing M2M table for field IndicadoresCognitivos on 'FichaPsicologica'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologica_IndicadoresCognitivos'))

        # Removing M2M table for field IndicadoresFisicos on 'FichaPsicologica'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologica_IndicadoresFisicos'))

        # Removing M2M table for field IndicadoresEmocionales on 'FichaPsicologica'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologica_IndicadoresEmocionales'))

        # Removing M2M table for field IndicadoresConductuales on 'FichaPsicologica'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologica_IndicadoresConductuales'))

        # Removing M2M table for field ProblemasSociales on 'FichaPsicologica'
        db.delete_table(db.shorten_name(u'Especialista_fichapsicologica_ProblemasSociales'))

        # Deleting model 'FichaMedica'
        db.delete_table(u'Especialista_fichamedica')

        # Deleting model 'CasosJuridicos'
        db.delete_table(u'Especialista_casosjuridicos')

        # Deleting model 'FichaJuridica'
        db.delete_table(u'Especialista_fichajuridica')

        # Removing M2M table for field Motivo_Consulta on 'FichaJuridica'
        db.delete_table(db.shorten_name(u'Especialista_fichajuridica_Motivo_Consulta'))


    models = {
        u'Especialista.casosjuridicos': {
            'Meta': {'object_name': 'CasosJuridicos'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'Especialista.fichabase': {
            'FechaActualizada': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'Finalizado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'FichaBase'},
            'Paciente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.DatosPersonalesBasicos']", 'null': 'True', 'blank': 'True'}),
            'ProfesionalTratante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Usuarios.KunaAtyUsuario']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Especialista.fichainfantil': {
            'FuenteDerivacion': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FuenteDeDerivacion']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'HistoriaFamiliar': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'IndicadoresCognitivos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresCognitivosInfantiles']"}),
            'IndicadoresComportamentales': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresComportamentalesInfantiles']"}),
            'IndicadoresEmocionales': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresEmocionalesInfantiles']"}),
            'IndicadoresFisicos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresFisicosInfantiles']"}),
            'Intervenciones': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.IntervencionAnterior']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'FichaInfantil', '_ormbases': [u'Especialista.FichaBase']},
            'MotivosDeConsulta': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fsi+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.MotivoDeConsulta']"}),
            'Seguimientos': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'TipoDeViolencia': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fsi+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.TiposDeViolencia']"}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fichajuridica': {
            'Asistencia_Alimenticia': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Caratula_Expediente': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'Es_Esto_Un': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Fecha_Inicio': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'Fecha_Resolucion': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'Filiacion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Fueros': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Juzgado': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'FichaJuridica', '_ormbases': [u'Especialista.FichaBase']},
            'Motivo_Consulta': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.CasosJuridicos']"}),
            'Nro_Resolucion': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Secretaria': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Tipo_Juicio': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fichamedica': {
            'Menarca': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Menopausia': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'FichaMedica', '_ormbases': [u'Especialista.FichaBase']},
            'Metodo_anticonceptivo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Nro_Abortos': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Nro_Cesareas': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Nro_Embarazos': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Nro_Nacido_vivo': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Nro_PartosNormales': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fichapsicologica': {
            'AparienciaPersonal': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'IndicadoresCognitivos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresCognitivos']"}),
            'IndicadoresConductuales': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresConductuales']"}),
            'IndicadoresEmocionales': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresEmocionales']"}),
            'IndicadoresFisicos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.IndicadoresFisicos']"}),
            'LesionesFisicas': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'}),
            'Meta': {'object_name': 'FichaPsicologica', '_ormbases': [u'Especialista.FichaBase']},
            'ProblemasSociales': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.ProblemasSociales']"}),
            'TipoDeViolencia': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'+'", 'blank': 'True', 'to': u"orm['Especialista.TiposDeViolencia']"}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fichapsicologicaadolescente': {
            'Cognitivos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fpa+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.IndicadoresViolenciaCognitivos']"}),
            'Comportamentales': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fpa+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.IndicadoresViolenciaComportamentales']"}),
            'DescripcionDelCaso': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Emocional': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fpa+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.IndicadoresViolenciaEmocionales']"}),
            'Fisico': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fpa+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.IndicadoresViolenciaFisicos']"}),
            'Meta': {'object_name': 'FichaPsicologicaAdolescente', '_ormbases': [u'Especialista.FichaBase']},
            'MotivosDeConsulta': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fpa+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.MotivoDeConsulta']"}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fichasocial': {
            'Agresor': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'PacienteAdulto'", 'unique': 'True', 'null': 'True', 'to': u"orm['Personas.DatosAgresor']"}),
            'ComoSabiaDeKunaAty': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'DenunciaComisaria': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DenunciaFiscalia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DenunciaJuridico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DenunciaPolicia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DiagnosticoMedico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'FrecuenciaDeViolencia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'fs+'", 'null': 'True', 'to': u"orm['Especialista.ViolenciaFrecuencia']"}),
            'Meta': {'object_name': 'FichaSocial', '_ormbases': [u'Especialista.FichaBase']},
            'MotivosDeConsulta': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fs+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.MotivoDeConsulta']"}),
            'RelacionConAgresor': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'TipoDeViolencia': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'fs+'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['Especialista.TiposDeViolencia']"}),
            'ViveConAgresor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fichasocialadolescente': {
            'Agresor': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'PacienteAdolescente'", 'unique': 'True', 'null': 'True', 'to': u"orm['Personas.DatosAgresor']"}),
            'ComoSabiaDeKunaAty': ('django.db.models.fields.IntegerField', [], {}),
            'DenunciaComisaria': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DenunciaFiscalia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DenunciaJuridico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DenunciaPolicia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DiagnosticoMedico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'FrecuenciaDeViolencia': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fsa+'", 'to': u"orm['Especialista.ViolenciaFrecuencia']"}),
            'Meta': {'object_name': 'FichaSocialAdolescente', '_ormbases': [u'Especialista.FichaBase']},
            'MotivosDeConsulta': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'fsa+'", 'symmetrical': 'False', 'to': u"orm['Especialista.MotivoDeConsulta']"}),
            'RelacionConAgresor': ('django.db.models.fields.IntegerField', [], {}),
            'TipoDeViolencia': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'fsa+'", 'symmetrical': 'False', 'to': u"orm['Especialista.TiposDeViolencia']"}),
            'ViveConAgresor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'fichabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Especialista.FichaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Especialista.fuentedederivacion': {
            'Direccion': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'+'", 'unique': 'True', 'to': u"orm['Personas.Direccion']"}),
            'Institucion': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Meta': {'object_name': 'FuenteDeDerivacion'},
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Especialista.indicadorescognitivos': {
            'Meta': {'object_name': 'IndicadoresCognitivos'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadorescognitivosinfantiles': {
            'Meta': {'object_name': 'IndicadoresCognitivosInfantiles'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadorescomportamentalesinfantiles': {
            'Meta': {'object_name': 'IndicadoresComportamentalesInfantiles'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadoresconductuales': {
            'Meta': {'object_name': 'IndicadoresConductuales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadoresemocionales': {
            'Meta': {'object_name': 'IndicadoresEmocionales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadoresemocionalesinfantiles': {
            'Meta': {'object_name': 'IndicadoresEmocionalesInfantiles'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadoresfisicos': {
            'Meta': {'object_name': 'IndicadoresFisicos'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadoresfisicosinfantiles': {
            'Meta': {'object_name': 'IndicadoresFisicosInfantiles'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.indicadoresviolenciacognitivos': {
            'Meta': {'object_name': 'IndicadoresViolenciaCognitivos'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'Especialista.indicadoresviolenciacomportamentales': {
            'Meta': {'object_name': 'IndicadoresViolenciaComportamentales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'Especialista.indicadoresviolenciaemocionales': {
            'Meta': {'object_name': 'IndicadoresViolenciaEmocionales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'Especialista.indicadoresviolenciafisicos': {
            'Meta': {'object_name': 'IndicadoresViolenciaFisicos'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'Especialista.intervencionanterior': {
            'Institucion': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Meta': {'object_name': 'IntervencionAnterior'},
            'MotivoDeConsulta': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'Resultado': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Especialista.motivodeconsulta': {
            'Meta': {'object_name': 'MotivoDeConsulta'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '35'})
        },
        u'Especialista.observacion': {
            'Meta': {'object_name': 'Observacion'},
            'fecha': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'ficha': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Especialista.FichaBase']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {})
        },
        u'Especialista.problemassociales': {
            'Meta': {'object_name': 'ProblemasSociales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Especialista.tiposdeviolencia': {
            'Meta': {'object_name': 'TiposDeViolencia'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'Especialista.violenciafrecuencia': {
            'Meta': {'object_name': 'ViolenciaFrecuencia'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'predeterminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Personas.barrio': {
            'Meta': {'object_name': 'Barrio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'Personas.datosagresor': {
            'Laborales': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosLaborales']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosAgresor'},
            'NivelAcademico': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'TieneExclusiondelHogar': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'datospersonalesbasicos_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Personas.DatosPersonalesBasicos']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Personas.datoslaborales': {
            'Cargo': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Ingreso': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'LugarDeTrabajo': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'DatosLaborales'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.datospersonalesbasicos': {
            'Apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Cedula': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'Correo': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'Direccion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Personas.Direccion']", 'null': 'True', 'blank': 'True'}),
            'FechaCreada': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'FechaNacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'LugarDeNacimiento': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'Meta': {'object_name': 'DatosPersonalesBasicos'},
            'Nacionalidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Pais']"}),
            'Nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'Sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.departamento': {
            'Meta': {'object_name': 'Departamento'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Personas.direccion': {
            'Barrio': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Barrio']"}),
            'Ciudad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Ciudad']"}),
            'Departamento': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['Personas.Departamento']"}),
            'Direccion': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Direccion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Personas.pais': {
            'Meta': {'object_name': 'Pais'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'Usuarios.kunaatyusuario': {
            'Direccion': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'+'", 'unique': 'True', 'null': 'True', 'to': u"orm['Personas.Direccion']"}),
            'Especialidad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'Meta': {'object_name': 'KunaAtyUsuario'},
            'Telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'tipo': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Especialista']