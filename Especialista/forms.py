# coding=utf-8
from django import forms
from django.core.urlresolvers import reverse_lazy

from Personas.models import DatosDePacienteAdulto, DatosAgresor, DatosLaborales, DatosLaboralesAdolescentes
from models import FichaSocial, FichaMedica, FichaJuridica, FichaPsicologica, Observacion
from models import FichaSocialAdolescente, FichaPsicologicaAdolescente, FichaInfantil
from models import TiposDeViolencia, IndicadoresCognitivos, IndicadoresFisicos, IndicadoresEmocionales, IndicadoresConductuales, ProblemasSociales
from models import CasosJuridicos
from Personas.models import Pais, Ciudad, Departamento, Barrio, TipoVivienda
from Personas.forms import FormularioDatosAgresor
from util.multiform import MultiModelForm
from util.widgets import CheckboxSelectOther
from util.fields import AgregarOpcionCampo


if 'NumberInput' not in dir(forms):
	NumberInput = forms.TextInput
else:
	NumberInput = forms.NumberInput

def crear_url(tipo):
	return reverse_lazy("listado_datos", kwargs={"tipo": tipo});

class ObservacionesFormulario(forms.ModelForm):
	class Meta:
		model = Observacion
		exclude = ("fecha", "ficha",)

class AgresorInstanceAyudante(object):
	def __init__(self, agresor):
		self.Agresor = agresor
		self.Direccion = agresor.Direccion if agresor else None
		self.Laborales = agresor.Laborales if agresor else None
		
class FormularioFichaSocial(forms.ModelForm):

	TipoDeViolencia = AgregarOpcionCampo(queryset = TiposDeViolencia.objects.all(), label = "Tipo de Violencia", required = False)
	
	class Meta:
		model=FichaSocial
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
		}
		exclude = ("Paciente", "FechaActualizada", "Agresor","ProfesionalTratante")
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js","js/agregarElementos.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
	
	def __init__(self, *args, **kwargs):
		super(FormularioFichaSocial, self).__init__(*args, **kwargs)
		## Problema con Django
		self.fields["MotivosDeConsulta"].help_text = ""
		self.fields["TipoDeViolencia"].help_text = ""
	
	@property
	def extras(self):
		return [(FormularioDatosAgresor, AgresorInstanceAyudante(self.instance.Agresor) if self.instance else None)]

class FormularioFichaMedica(forms.ModelForm):
	class Meta:
		model=FichaMedica
		widgets={'Nro_Abortos': NumberInput(attrs={'min':0,'max':10}),
			'Menarca': NumberInput(attrs={'min':8,'max':20}),
			'Menopausia': NumberInput(attrs={'min':38,'max':55})
		}

		
		exclude = ("Paciente", "FechaActualizada",)
	
	class Media:

		model=FichaMedica
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
	
	class Media:

		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
		
	@property
	def extras(self):
		return []
		
class FormularioFichaPsicologica(forms.ModelForm):

	TipoDeViolencia = AgregarOpcionCampo(queryset = TiposDeViolencia.objects.all(), label = "Tipo de Violencia", required = False)
	IndicadoresCognitivos = AgregarOpcionCampo(queryset = IndicadoresCognitivos.objects.all(), label = "Indicadores Cognitivos", required = False)
	IndicadoresFisicos = AgregarOpcionCampo(queryset = IndicadoresFisicos.objects.all(), label = "Indicadores Fisicos", required = False)
	IndicadoresEmocionales = AgregarOpcionCampo(queryset = IndicadoresEmocionales.objects.all(), label = "Indicadores Emocionales", required = False)
	IndicadoresConductuales = AgregarOpcionCampo(queryset = IndicadoresConductuales.objects.all(), label = "Indicadores Conductuales", required = False)
	ProblemasSociales = AgregarOpcionCampo(queryset = ProblemasSociales.objects.all(), label = "Problemas Sociales", required = False)
	
	
	class Meta:
		model=FichaPsicologica
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
	
	class Media:

		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
		
	def __init__(self, *args, **kwargs):
		super(FormularioFichaPsicologica, self).__init__(*args, **kwargs)
	
	@property
	def extras(self):
		return []
		
class FormularioFichaPsicologicaAdolescente(forms.ModelForm):

	class Meta:
		model=FichaPsicologicaAdolescente
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
		
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
			"Cognitivos": forms.widgets.CheckboxSelectMultiple,
			"Fisico": forms.widgets.CheckboxSelectMultiple,
			"Emocional": forms.widgets.CheckboxSelectMultiple,
			"Comportamentales": forms.widgets.CheckboxSelectMultiple
		}
	
	class Media:

		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

	TipoDeViolencia = AgregarOpcionCampo(queryset = TiposDeViolencia.objects.all(), label = "Tipo de Violencia", required = False)
	class Meta:
		model=FichaSocial
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
		}
		exclude = ("Paciente", "FechaActualizada", "Agresor","ProfesionalTratante")
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js","js/agregarElementos.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
	
	def __init__(self, *args, **kwargs):
		super(FormularioFichaSocial, self).__init__(*args, **kwargs)
		## Problema con Django
		self.fields["MotivosDeConsulta"].help_text = ""
		self.fields["TipoDeViolencia"].help_text = ""
	
	@property
	def extras(self):
		return [(FormularioDatosAgresor, AgresorInstanceAyudante(self.instance.Agresor) if self.instance else None)]

class FormularioFichaMedica(forms.ModelForm):
	class Meta:
		widgets={'Nro_Abortos': NumberInput(attrs={'min':0,'max':10}),
			'Menarca': NumberInput(attrs={'min':8,'max':20}),
			'Menopausia': NumberInput(attrs={'min':38,'max':55})
		}
		model=FichaMedica
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
		
	@property
	def extras(self):
		return []

class FormularioFichaJuridica(forms.ModelForm):
	Fecha_Inicio = forms.DateField(label = "Fecha de Inicio", widget=forms.TextInput(attrs={"class":"datepicker"}), required=False)
	Fecha_Resolucion = forms.DateField(label = "Fecha de Resolucion", widget=forms.TextInput(attrs={"class":"datepicker"}), required=False)
	Motivo_Consulta = AgregarOpcionCampo(queryset = CasosJuridicos.objects.all(), label = "Motivo de Consulta", required = False)
	
	class Meta:
		model=FichaJuridica
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
		
	class Media:

		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
	
	@property
	def extras(self):
		return []
		
class FormularioFichaPsicologica(forms.ModelForm):
	TipoDeViolencia = AgregarOpcionCampo(queryset = TiposDeViolencia.objects.all(), label = "Tipo de Violencia", required = False)
	IndicadoresCognitivos = AgregarOpcionCampo(queryset = IndicadoresCognitivos.objects.all(), label = "Indicadores Cognitivos", required = False)
	IndicadoresFisicos = AgregarOpcionCampo(queryset = IndicadoresFisicos.objects.all(), label = "Indicadores Fisicos", required = False)
	IndicadoresEmocionales = AgregarOpcionCampo(queryset = IndicadoresEmocionales.objects.all(), label = "Indicadores Emocionales", required = False)
	IndicadoresConductuales = AgregarOpcionCampo(queryset = IndicadoresConductuales.objects.all(), label = "Indicadores Conductuales", required = False)
	ProblemasSociales = AgregarOpcionCampo(queryset = ProblemasSociales.objects.all(), label = "Problemas Sociales", required = False)
	
	class Meta:
		model=FichaPsicologica
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
		
	def __init__(self, *args, **kwargs):
		super(FormularioFichaPsicologica, self).__init__(*args, **kwargs)
	
	@property
	def extras(self):
		return []
		
class FormularioFichaPsicologicaAdolescente(forms.ModelForm):
	class Meta:
		model=FichaPsicologicaAdolescente
		exclude = ("Paciente", "FechaActualizada","ProfesionalTratante")
		
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
			"Cognitivos": forms.widgets.CheckboxSelectMultiple,
			"Fisico": forms.widgets.CheckboxSelectMultiple,
			"Emocional": forms.widgets.CheckboxSelectMultiple,
			"Comportamentales": forms.widgets.CheckboxSelectMultiple
		}
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}


	def __init__(self, *args, **kwargs):
		super(FormularioFichaPsicologicaAdolescente, self).__init__(*args, **kwargs)
		## Problema con Django
		self.fields["MotivosDeConsulta"].help_text = ""
		self.fields["Cognitivos"].help_text = ""
		self.fields["Fisico"].help_text = ""
		self.fields["Emocional"].help_text = ""
		self.fields["Comportamentales"].help_text = ""
		
	@property
	def extras(self):
		return []
		
class FormularioFichaAdolescente(forms.ModelForm):

	class Meta:
		model=FichaSocialAdolescente
		exclude = ("Paciente", "FechaActualizada", "Agresor",)
		
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
			"TipoDeViolencia": forms.widgets.CheckboxSelectMultiple
		}
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}

	class Meta:
		model=FichaSocialAdolescente
		exclude = ("Paciente", "FechaActualizada", "Agresor","ProfesionalTratante")
		
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
			"TipoDeViolencia": forms.widgets.CheckboxSelectMultiple
		}
	
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}


	def __init__(self, *args, **kwargs):
		super(FormularioFichaAdolescente, self).__init__(*args, **kwargs)
		## Problema con Django
		self.fields["MotivosDeConsulta"].help_text = ""
		self.fields["TipoDeViolencia"].help_text = ""

	@property
	def extras(self):
		return [FormularioDatosAgresor]

class FormularioFichaPsicosocialInfante(forms.ModelForm):

	class Meta:
		model = FichaInfantil
		exclude = ("Paciente", "FechaActualizada","FuenteDerivacion", "Intervenciones","ProfesionalTratante",)
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
			"TipoDeViolencia": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresFisicos": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresEmocionales": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresCognitivos": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresComportamentales": forms.widgets.CheckboxSelectMultiple
		}
		
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
	
	def __init__(self, *args, **kwargs):
		super(FormularioFichaPsicosocialInfante, self).__init__(*args, **kwargs)
		## Problema con Django
		for nombre in FormularioFichaPsicosocialInfante.Meta.widgets.keys():
			if FormularioFichaPsicosocialInfante.Meta.widgets[nombre] == forms.widgets.CheckboxSelectMultiple:
				self.fields[nombre].help_text = ""

	class Meta:
		model = FichaInfantil
		exclude = ("Paciente", "FechaActualizada","FuenteDerivacion", "Intervenciones","ProfesionalTratante",)
		widgets = {
			"MotivosDeConsulta": forms.widgets.CheckboxSelectMultiple,
			"TipoDeViolencia": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresFisicos": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresEmocionales": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresCognitivos": forms.widgets.CheckboxSelectMultiple,
			"IndicadoresComportamentales": forms.widgets.CheckboxSelectMultiple
		}
		
	class Media:
		js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js","js/fechas.js",)
		css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}
	
	def __init__(self, *args, **kwargs):
		super(FormularioFichaPsicosocialInfante, self).__init__(*args, **kwargs)
		## Problema con Django
		for nombre in FormularioFichaPsicosocialInfante.Meta.widgets.keys():
			if FormularioFichaPsicosocialInfante.Meta.widgets[nombre] == forms.widgets.CheckboxSelectMultiple:
				self.fields[nombre].help_text = ""


#### Crear formsets para cada tipo de paciente
def buscarFichaAdolescente(nombre):
	return {"social": FormularioFichaAdolescente,
			"psicologica": FormularioFichaPsicologicaAdolescente}.get(nombre.lower(), None)

def buscarFicha(nombre):
	return {"social": FormularioFichaSocial,
			"juridica": FormularioFichaJuridica,
			"medica": FormularioFichaMedica,
			"psicologica": FormularioFichaPsicologica,
			"psicologicaadolescente": FormularioFichaPsicologicaAdolescente,
			"adolescente": FormularioFichaAdolescente}.get(nombre.lower(), None)
			

