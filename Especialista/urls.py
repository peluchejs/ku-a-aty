# coding=utf-8
from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView, CreateView
from django.contrib.auth.decorators import login_required
from Personas.models import DatosDePacienteAdulto, DatosDePacienteAdolescente

urlpatterns = patterns('',
    url(r'^Paciente/Adulto/(?P<pk>\d+)/CrearFicha/(?P<tipo>social|psicologica|medica|juridica)/$', 'Especialista.views.CrearFicha', name='crearFicha'),
    url(r'^Paciente/Adulto/(?P<pk>\d+)/ModificarFicha/(?P<tipo>social|psicologica|medica|juridica)/(?P<ficha_pk>\d+)/$', 'Especialista.views.CrearFicha', name='modificar_ficha'),
    url(r'^Paciente/Adulto/(?P<pk>\d+)/VerFicha/(?P<tipo>social|psicologica|medica|juridica)/(?P<ficha_pk>\d+)/$', 'Especialista.views.VerFicha', name="ver_ficha"),
    url(r'^Paciente/Adulto/(?P<pk>\d+)/Ficha/(?P<tipo>social|psicologica|medica|juridica)/$', 'Especialista.views.FichaListado', name='ficha_adulto_listado'),
    url(r'^Paciente/Infante/(?P<pk>\d+)/Ficha/$', 'Especialista.views.FichaListadoInfante', name='ficha_infante_listado'),
    url(r'^Paciente/Infante/(?P<pk>\d+)/CrearFicha/$', 'Especialista.views.CrearFichaInfante', name='crear_ficha_infante'),
    url(r'^Paciente/Infante/(?P<pk>\d+)/VerFicha/(?P<tipo>psicosocial)/(?P<ficha_pk>\d+)/$', 'Especialista.views.VerFicha', name = 'ver_ficha_infante'),
    url(r'^Paciente/Infante/(?P<pk>\d+)/ModificarFicha/(?P<ficha_pk>\d+)/$', 'Especialista.views.CrearFichaInfante', name = 'modificar_ficha_infante'),
    url(r'^Paciente/Adolescente/(?P<pk>\d+)/Ficha/(?P<tipo>social|psicologica)/$', 'Especialista.views.FichaListadoAdolescente', name='ficha_adolescente_listado'),
    url(r'^Paciente/Adolescente/(?P<pk>\d+)/CrearFicha/(?P<tipo>social|psicologica)/$', 'Especialista.views.CrearFichaAdolescente', name='crear_ficha_adolescente'),
    url(r'^Paciente/Adolescente/(?P<pk>\d+)/VerFicha/(?P<tipo>social|psicologica)/(?P<ficha_pk>\d+)/$', 'Especialista.views.VerFicha', name='ver_ficha_adolescente'),
    url(r'^Paciente/Adolescente/(?P<pk>\d+)/ModificarFicha/(?P<tipo>social|psicologica)/(?P<ficha_pk>\d+)/$', 'Especialista.views.CrearFichaAdolescente', name = 'modificar_ficha_adolescente'),
    url(r'^Paciente/Adulto/(?P<pk>\d+)/Imprimir/(?P<tipo>social|psicologica|medica|juridica)/(?P<ficha_pk>\d+)/$', 'Especialista.views.ImprimirFicha', name="imprimir_ficha"),
    url(r'^Paciente/Infante/(?P<pk>\d+)/Imprimir/(?P<tipo>psicosocial)/(?P<ficha_pk>\d+)/$', 'Especialista.views.ImprimirFicha', name = 'imprimir_ficha'),
    )
