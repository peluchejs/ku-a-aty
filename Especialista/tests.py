# coding=utf-8
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from views import FichaListado, FichaListadoAdolescente, FichaListadoInfante

from Personas.models import DatosDePacienteAdulto, DatosDePacienteAdolescente, DatosInfantiles

class ViewsListadosTests(TestCase):
    fixtures = ['test_data.json']
    def setUp(self):
        self.client = Client()
    
    def testFichaListado(self):
        pk_adulto = DatosDePacienteAdulto.objects.all()[0].pk
        npk_adulto = 9999999
        self.assertTrue(self.client.login(username = "testuser", password = "testpassword"))
        for tipo in ('social', 'medica', 'juridica', 'psicologica'):
            response = self.client.get(reverse(FichaListado, kwargs = {"tipo": tipo, "pk": pk_adulto}))
            self.assertEqual(response.status_code, 200)
            response = self.client.get(reverse(FichaListado, kwargs = {"tipo": tipo, "pk": npk_adulto}))
            self.assertEqual(response.status_code, 404)
        self.client.logout()

    def testFichaListadoAdolescente(self):
        pk_adolescente = DatosDePacienteAdolescente.objects.create().pk
        npk_adolescente = 9999999
        self.assertTrue(self.client.login(username = "testuser", password = "testpassword"))
        for tipo in ('social', 'psicologica'):
            response = self.client.get(reverse(FichaListadoAdolescente, kwargs = {"tipo": tipo, "pk": pk_adolescente}))
            self.assertEqual(response.status_code, 200)
            response = self.client.get(reverse(FichaListadoAdolescente, kwargs = {"tipo": tipo, "pk": npk_adolescente}))
            self.assertEqual(response.status_code, 404)
        self.client.logout()
    
    def testFichaListadoInfante(self):
        pk_infante = DatosInfantiles.objects.create().pk
        npk_infante = 9999999
        self.assertTrue(self.client.login(username = "testuser", password = "testpassword"))
        response = self.client.get(reverse(FichaListadoInfante, kwargs = {"pk": pk_infante}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse(FichaListadoInfante, kwargs = {"pk": npk_infante}))
        self.assertEqual(response.status_code, 404)
        self.client.logout()