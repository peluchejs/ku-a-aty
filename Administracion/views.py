# coding=utf-8
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from forms import CrearUsuarioFormulario, CambiarContrasenhaFormulario
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.core.servers.basehttp import FileWrapper
from django.core.urlresolvers import reverse
from django.contrib import messages
from Usuarios.models import KunaAtyUsuario
from Fichero.models import Actividad
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os.path

@login_required
@user_passes_test(lambda u:u.is_staff)
def Inicio(solicitud):
	return render(solicitud, 'adminInicio.html')

@login_required
@user_passes_test(lambda u:u.is_staff)
def Backup(solicitud):
	from KunaAty import settings
	db_engine = settings.DATABASES['default']['ENGINE']

	if db_engine == 'django.db.backends.sqlite3':
		db_path = settings.DATABASES['default']['NAME']
		response = HttpResponse(FileWrapper(open(db_path)), content_type='application/x-sqlite3')
		response['Content-Disposition'] = 'attachment;filename=backup.db'
		response['Content-Length'] = os.path.getsize(db_path) 
		return response
	else:
		messages.success(solicitud, "El BACKUP se realizará automáticamente en el Servidor.")
		return HttpResponseRedirect(reverse(Inicio))
		#return HttpResponse("settings.DATABASES['default']['ENGINE'] is %s,<br />\
							# only for 'django.db.backends.sqlite3' online backup is posible." % (db_engine), status = 500)
		
	
@login_required
@user_passes_test(lambda u:u.is_staff)
def Registrar(solicitud):
	form=CrearUsuarioFormulario(solicitud.POST or None)
	if form.is_valid():
		form.save()
		messages.success(solicitud, "La nueva cuenta fue creada exitosamente.")
		return HttpResponseRedirect(reverse(Inicio))
	return render(solicitud, "CrearUsuario.html", {"form":form})

@login_required
@user_passes_test(lambda u:u.is_staff)
def BloquearCuenta(solicitud):	  
	if "usuario" in solicitud.GET and KunaAtyUsuario.objects.filter(pk = solicitud.GET["usuario"]).exists():
		usuario = KunaAtyUsuario.objects.get(pk = solicitud.GET["usuario"])
		usuario.is_active = False
		usuario.save()
		messages.success(solicitud, "La cuenta fue bloqueada.")
	return HttpResponseRedirect(reverse(UsuariosListado))

@login_required
@user_passes_test(lambda u:u.is_staff)
def HabilitarCuenta(solicitud):	   
	if "usuario" in solicitud.GET and KunaAtyUsuario.objects.filter(pk = solicitud.GET["usuario"]).exists():
		usuario = KunaAtyUsuario.objects.get(pk = solicitud.GET["usuario"])
		usuario.is_active = True
		usuario.save()
		messages.success(solicitud, "La cuenta fue habilitada.")
	return HttpResponseRedirect(reverse(UsuariosListado))

@login_required
@user_passes_test(lambda u:u.is_staff)
def CambiarContrasenha(solicitud, usuario_pk):
	if int(usuario_pk) == solicitud.user.pk:
		return HttpResponseRedirect(reverse('cambiar_contrasenha'))
	form = CambiarContrasenhaFormulario(solicitud.POST or None)
	usuario = get_object_or_404(KunaAtyUsuario, pk = usuario_pk)
	if form.is_valid():
		usuario.set_password(form.cleaned_data['password1'])
		usuario.save()
		messages.success(solicitud, "La contraseña fue cambiada.")
		return HttpResponseRedirect(reverse(UsuariosListado))
	return render(solicitud, "cambiarContrasenha.html", {"form": form})

@login_required
@user_passes_test(lambda u:u.is_staff)
def Fichero(solicitud):
	qs = Actividad.objects.all().order_by("-tiempo")
	paginas = Paginator(qs, 50)
	paginaactual = solicitud.GET.get("p")
	try:
		actividades = paginas.page(paginaactual)
	except PageNotAnInteger:
		actividades = paginas.page(1)
	except EmptyPage:
		actividades = paginas.page(paginas.num_pages)
	return render(solicitud, "fichero.html", {"fichero": actividades})
	
	qs = Actividad.objects.all().order_by("-tiempo")
	paginas = Paginator(qs, 50)
	paginaactual = solicitud.GET.get("p")
	try:
		actividades = paginas.page(paginaactual)
	except PageNotAnInteger:
		actividades = paginas.page(1)
	except EmptyPage:
		actividades = paginas.page(paginas.num_pages)
	return render(solicitud, "fichero.html", {"fichero": actividades})
	

@login_required
@user_passes_test(lambda u:u.is_staff)
def UsuariosListado(solicitud):
	return render(solicitud, "usuariosListado.html", {"usuariosActivos": KunaAtyUsuario.objects.filter(is_active = True),
													   "usuariosInactivos": KunaAtyUsuario.objects.filter(is_active = False)})
													  
