# coding=utf-8
from django.test import TestCase
from django.test.client import RequestFactory, Client
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import AnonymousUser
from forms import CambiarContrasenhaFormulario, CrearUsuarioFormulario
from views import Inicio, Backup, Registrar, BloquearCuenta, HabilitarCuenta, CambiarContrasenha, Fichero, UsuariosListado
from Usuarios.models import KunaAtyUsuario

class TestContrasenhaFormulario(TestCase):
    def testFormularioErrores(self):
        form = CambiarContrasenhaFormulario()
        self.assertEqual(False, form.is_valid())
        form = CambiarContrasenhaFormulario({"password1": "asdf", "password2": ""})
        self.assertEqual(False, form.is_valid())
        self.assertIn("password2", form.errors)
        self.assertNotIn("password1", form.errors)
        form = CambiarContrasenhaFormulario({"password1": "asdf", "password2": "8675309"})
        self.assertEqual(False, form.is_valid())
        self.assertIn("password2", form.errors)
        self.assertNotIn("password1", form.errors)
        
    def testFormularioExito(self):
        form = CambiarContrasenhaFormulario({"password1": "villarrica", "password2": "villarrica"})
        self.assertTrue(form.is_valid())
        self.assertEqual("villarrica", form.cleaned_data['password1'])

class TestCrearUsuarioFormulario(TestCase):
    fixtures = ['test_data.json']
    
    def testCrearUsuarioErrores(self):
        form = CrearUsuarioFormulario()
        self.assertFalse(form.is_valid())
        form = CrearUsuarioFormulario({"username": "testuser1", "password1": "asdf", "password2": "asdf"})
        self.assertFalse(form.is_valid())
        self.assertIn("username", form.errors)
        self.assertNotIn("password1", form.errors)
        self.assertNotIn("password2", form.errors)
        form = CrearUsuarioFormulario({"username": "testuser5", "password1": "", "password2": "asdf"})
        self.assertFalse(form.is_valid())
        self.assertIn("password1", form.errors)
        self.assertNotIn("username", form.errors)
        self.assertNotIn("password2", form.errors)
        form = CrearUsuarioFormulario({"username": "testuser5", "password1": "asdfasdf", "password2": "asdf"})
        self.assertFalse(form.is_valid())
        self.assertIn("password2", form.errors)
        form = CrearUsuarioFormulario({"username": "testuser5", "password1": "villarrica", "password2": "villarrica"})
        self.assertFalse(form.is_valid())
    
    def testFormularioExito(self):
        form = CrearUsuarioFormulario({"username": "testuser5",
                                       "password1": "villarrica",
                                       "password2": "villarrica",
                                       "tipo": "0",
                                       "Especialidad": "2",
                                       "email": "test@test.com"})
        self.assertTrue(form.is_valid())
        user = form.save(commit = False)
        self.assertEqual("testuser5", user.username)
        user.save()
        form = CrearUsuarioFormulario({"username": "testuser324",
                                       "password1": "villarrica",
                                       "password2": "villarrica",
                                       "tipo": "0",
                                       "Especialidad": "2",
                                       "email": "test@test.com"})
        self.assertTrue(form.is_valid())
        user = form.save()
        self.assertEqual("testuser324", user.username)

class ViewUrl(object):
    def __init__(self, viewFunc, rArgs = {}, vpArgs = [], vkwArgs = {}):
        self.view = viewFunc
        self.url = reverse(viewFunc, **rArgs)
        self.pArgs = vpArgs
        self.kwArgs = vkwArgs
        
    def __call__(self, request):
        return self.view(request, *self.pArgs, **self.kwArgs)

class TestViews(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.adminUser = KunaAtyUsuario.objects.create(username = "admin", is_staff = True)
        self.adminUser.set_password("password")
        self.adminUser.save()
        self.normalUser = KunaAtyUsuario.objects.create(username = "normal")
        self.normalUser.set_password("password")
        self.normalUser.save()
        self.viewList = [ViewUrl(Inicio),
                         ViewUrl(Backup),
                         ViewUrl(Registrar), 
                         ViewUrl(BloquearCuenta),
                         ViewUrl(HabilitarCuenta),
                         ViewUrl(CambiarContrasenha, rArgs = {"kwargs": {"usuario_pk": self.normalUser.pk}}, vpArgs = [], vkwArgs = {"usuario_pk": self.normalUser.pk}),
                         ViewUrl(Fichero), 
                         ViewUrl(UsuariosListado)]
    
    def testAccessDenied(self):
        for view in self.viewList:
            request = self.factory.get(view.url)
            request.user = self.normalUser
            response = view(request)
            self.assertIsInstance(response, HttpResponseRedirect)
            request = self.factory.get(view.url)
            request.user = AnonymousUser()
            response = view(request)
            self.assertIsInstance(response, HttpResponseRedirect)
            
    def testInicioView(self):
        request = self.factory.get(reverse(Inicio))
        request.user = self.adminUser
        response = Inicio(request)
        self.assertEqual(response.status_code, 200)
    
    def testBackup(self):
        from KunaAty import settings
        db_engine = settings.DATABASES['default']['ENGINE']
        settings.DATABASES['default']['ENGINE'] = "mysql"
        request = self.factory.get(reverse(Backup))
        request.user = self.adminUser
        response = Backup(request)
        self.assertEqual(response.status_code, 500)
        settings.DATABASES['default']['ENGINE'] = db_engine
    
    def testRegistrar(self):
        self.assertTrue(self.client.login(username = "admin", password = "password"))
        response = self.client.get(reverse(Registrar))
        self.assertEqual(response.status_code, 200)
        data = {'username': 'charles', 'password1': 'dickens', 'password2': '', 'tipo': 0, 'Especialidad': 2, 'email': 'test@test.org'}
        response = self.client.post(reverse(Registrar), data = data)
        self.assertEqual(response.status_code, 200)
        data['password2'] = 'dickens'
        response = self.client.post(reverse(Registrar), data = data)
        self.assertIsInstance(response, HttpResponseRedirect)
        user = KunaAtyUsuario.objects.get(username = 'charles')
        self.assertEqual(user.tipo, 0)
        self.assertEqual(user.Especialidad, 2)
        self.assertEqual(user.email, 'test@test.org')
        self.client.logout()
        
    def testBloquearCuentaHabilitarCuenta(self):
        self.assertTrue(self.client.login(username = "admin", password = "password"))
        response = self.client.get(reverse(BloquearCuenta), data = {"usuario": self.normalUser.pk})
        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse(UsuariosListado), response['Location'])
        self.client.logout()
        self.assertFalse(self.client.login(username = "normal", password = "password"))
        self.assertTrue(self.client.login(username = "admin", password = "password"))
        response = self.client.get(reverse(HabilitarCuenta), data = {"usuario": self.normalUser.pk})
        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse(UsuariosListado), response['Location'])
        self.client.logout()
        self.assertTrue(self.client.login(username = "normal", password = "password"))
    
    def testFichero(self):
        self.assertTrue(self.client.login(username = "admin", password = "password"))
        response = self.client.get(reverse(Fichero))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse(Fichero), data = {"p": 9999})
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse(Fichero), data = {"p": "test"})
        self.assertEqual(response.status_code, 200)
        self.client.logout()
    
    def testListado(self):
        self.assertTrue(self.client.login(username = "admin", password = "password"))
        response = self.client.get(reverse(UsuariosListado))
        self.assertEqual(response.status_code, 200)
        self.client.logout()
    
    def testCambiarContrasenha(self):
        self.assertTrue(self.client.login(username = "admin", password = "password"))
        response = self.client.get(reverse(CambiarContrasenha, kwargs = {"usuario_pk": self.adminUser.pk}))
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(CambiarContrasenha, kwargs = {"usuario_pk": self.normalUser.pk}))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse(CambiarContrasenha, kwargs = {"usuario_pk": self.normalUser.pk}), data = {"password1": "newpass", "password2": "newpass"})
        self.assertEqual(response.status_code, 302)
        self.client.logout()
        self.assertFalse(self.client.login(username = "normal", password = "password"))
        self.assertTrue(self.client.login(username = "normal", password = "newpass"))
        self.client.logout()