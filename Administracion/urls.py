# coding=utf-8
from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView, CreateView
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
     url(r'^$', 'Administracion.views.Inicio', name='Administracion'),
     url(r'^Backup$', 'Administracion.views.Backup', name="backup"),   
     url(r'^Registrar$', 'Administracion.views.Registrar', name="registrar"),
     url(r'^Bloquear$', 'Administracion.views.BloquearCuenta', name="bloquearCuenta"),
     url(r'^Habilitar$', 'Administracion.views.HabilitarCuenta', name="habilitarCuenta"),
     url(r'^CambiarContrasenha/(?P<usuario_pk>\d+)/$', 'Administracion.views.CambiarContrasenha', name='cambiarContrasenha'),
     url(r'^Fichero$', 'Administracion.views.Fichero', name="fichero"),
     url(r'^UsuariosListado', 'Administracion.views.UsuariosListado', name="usuariosListado"),
     url(r'^CrearUsuario/$', 'Administracion.views.Registrar', name="registrar"),

)
     
