﻿# coding=utf-8
from django import forms
from Usuarios.models import KunaAtyUsuario

class CambiarContrasenhaFormulario(forms.Form):
    password1 = forms.CharField(label = 'Contraseña', widget = forms.PasswordInput(attrs={"required": ""}))
    password2 = forms.CharField(label = 'Confirmación de contraseña', widget = forms.PasswordInput(attrs = {"required": ""}))
    
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas no son iguales")
        return password2

class CrearUsuarioFormulario(forms.ModelForm):
    username = forms.CharField(label='Nombre de Usuario', widget=forms.TextInput(attrs={"required": ""}))
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput(attrs={"required": ""}))
    password2 = forms.CharField(label='Confirmación de contraseña', widget=forms.PasswordInput(attrs={"required": ""}))
    class Meta:
        model = KunaAtyUsuario
        fields = ('username', 'tipo', 'email', "Especialidad")

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas no son iguales")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(CrearUsuarioFormulario, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if user.tipo == 2:
            user.is_staff = True
        if commit:
            user.save()
        return user
    class Media:
        js = ("js/jquery-2.0.3.min.js", "js/jquery-ui-1.10.3.min.js", "js/jquery.validate.min.js",)
        css = {"all": ("css/jquery-ui-1.10.3.custom.min.css",)}